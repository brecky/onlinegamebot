﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GrepolisBot2
{
    sealed class Settings
    {
        //Singleton
        private static readonly Settings m_Instance = new Settings();

        //General
        private string m_GenUserName = "name";
        private string m_GenPassword = "password";
        private string m_GenMainServer = "en.grepolis.com";
        private string m_GenServer = "en1.grepolis.com";
        private bool m_GenAutoStart = false;
        private bool m_GenToTryIcon = true;
        
        //Building/training queue
        private int m_QueueTimer = 20;
        
        //E-Mail
        private string m_MailAddress = "name@domain.com";
        private bool m_MailNotify = true;
        private string m_MailUsername = "name@gmail.com";
        private string m_MailPassword = "password";
        private string m_MailServer = "smtp.gmail.com";
        private int m_MailPort = 587;

        //Advanced
        private int m_AdvTimerRefresh = 2;
        private int m_AdvTimerReconnect = 1;
        private bool m_AdvDebugMode = true;
        private string m_AdvDebugFile = "debug.txt";//Not visible on GUI
        private string m_AdvProgramSettingsFile = "Config/settings.xml";//Not visible on GUI
        private string m_AdvTownsSettingsFile = "Config/towns.xml";//Not visible on GUI
        private string m_AdvUserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13";//Not vissible on GUI
        private string m_AdvNotesFile = "notes.txt";//Not visible on GUI
        private string m_AdvResponseDir = "Response/";//Not visible on GUI
        private string m_AdvDonatorSettingsFile = "Config/donator.xml";//Not visible on GUI

        //GUI
        private Color m_GUIBuildingLevelTargetColor = Color.GreenYellow;
        private bool m_GUIBuildingTooltipsEnabled = true;

        //Donator (Read only)
        private string m_DonUserName = "name";//See donator.xml
        private string m_DonPassword = "password";//See donator.xml
        private bool m_DonDonated = false;//Not saved, is checked at startup

        //Proxy
        private bool m_ProxyEnabled = false;
        private string m_ProxyServer = "http://1.2.3.4:3128";
        private string m_ProxyUserName = "";
        private string m_ProxyPassword = "";

//-->Constructor

        private Settings()
        {

        }

//-->Attributes

        //Singleton
        public static Settings Instance
        {
            get
            {
                return m_Instance;
            }
        }

        //General
        public string GenUserName
        {
            get { return m_GenUserName; }
            set { m_GenUserName = value; }
        }

        public string GenPassword
        {
            get { return m_GenPassword; }
            set { m_GenPassword = value; }
        }

        public string GenMainServer
        {
            get { return m_GenMainServer; }
            set { m_GenMainServer = value; }
        }

        public string GenServer
        {
            get { return m_GenServer; }
            set { m_GenServer = value; }
        }

        public bool GenAutoStart
        {
            get { return m_GenAutoStart; }
            set { m_GenAutoStart = value; }
        }

        public bool GenToTryIcon
        {
            get { return m_GenToTryIcon; }
            set { m_GenToTryIcon = value; }
        }

        //Building/training queue
        public int QueueTimer
        {
            get { return m_QueueTimer; }
            set { m_QueueTimer = value; }
        }

        //E-Mail
        public string MailAddress
        {
            get { return m_MailAddress; }
            set { m_MailAddress = value; }
        }

        public bool MailNotify
        {
            get { return m_MailNotify; }
            set { m_MailNotify = value; }
        }

        public string MailUsername
        {
            get { return m_MailUsername; }
            set { m_MailUsername = value; }
        }

        public string MailPassword
        {
            get { return m_MailPassword; }
            set { m_MailPassword = value; }
        }

        public string MailServer
        {
            get { return m_MailServer; }
            set { m_MailServer = value; }
        }

        public int MailPort
        {
            get { return m_MailPort; }
            set { m_MailPort = value; }
        }

        //Advanced
        public int AdvTimerRefresh
        {
            get { return m_AdvTimerRefresh; }
            set { m_AdvTimerRefresh = value; }
        }

        public int AdvTimerReconnect
        {
            get { return m_AdvTimerReconnect; }
            set { m_AdvTimerReconnect = value; }
        }

        public bool AdvDebugMode
        {
            get { return m_AdvDebugMode; }
            set { m_AdvDebugMode = value; }
        }

        public string AdvDebugFile
        {
            get { return m_AdvDebugFile; }
            set { m_AdvDebugFile = value; }
        }

        public string AdvProgramSettingsFile
        {
            get { return m_AdvProgramSettingsFile; }
            set { m_AdvProgramSettingsFile = value; }
        }

        public string AdvTownsSettingsFile
        {
            get { return m_AdvTownsSettingsFile; }
            set { m_AdvTownsSettingsFile = value; }
        }

        public string AdvUserAgent
        {
            get { return m_AdvUserAgent; }
            set { m_AdvUserAgent = value; }
        }

        public string AdvNotesFile
        {
            get { return m_AdvNotesFile; }
            set { m_AdvNotesFile = value; }
        }

        public string AdvResponseDir
        {
            get { return m_AdvResponseDir; }
            set { m_AdvResponseDir = value; }
        }

        public string AdvDonatorSettingsFile
        {
            get { return m_AdvDonatorSettingsFile; }
            set { m_AdvDonatorSettingsFile = value; }
        }

        //GUI
        public Color GUIBuildingLevelTargetColor
        {
            get { return m_GUIBuildingLevelTargetColor; }
            set { m_GUIBuildingLevelTargetColor = value; }
        }

        public bool GUIBuildingTooltipsEnabled
        {
            get { return m_GUIBuildingTooltipsEnabled; }
            set { m_GUIBuildingTooltipsEnabled = value; }
        }

        //Donator (Read only)
        public string DonUserName
        {
            get { return m_DonUserName; }
            set { m_DonUserName = value; }
        }

        public string DonPassword
        {
            get { return m_DonPassword; }
            set { m_DonPassword = value; }
        }

        public bool DonDonated
        {
            get { return m_DonDonated; }
            set { m_DonDonated = value; }
        }

        //Proxy
        public bool ProxyEnabled
        {
            get { return m_ProxyEnabled; }
            set { m_ProxyEnabled = value; }
        }

        public string ProxyServer
        {
            get { return m_ProxyServer; }
            set { m_ProxyServer = value; }
        }

        public string ProxyUserName
        {
            get { return m_ProxyUserName; }
            set { m_ProxyUserName = value; }
        }

        public string ProxyPassword
        {
            get { return m_ProxyPassword; }
            set { m_ProxyPassword = value; }
        }

//--Methods

    }
}
