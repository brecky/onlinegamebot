﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Reflection;

namespace GrepolisBot2
{
    class Controller
    {
        //General
        private bool m_LoggedIn = false;
        private bool m_LoggedInOnce = false;
        private bool m_IsBotRunning = false;
        private bool m_IsBotRunningOnce = false;
        private bool m_IsBotReadyToStart = false;
        private bool m_TimeoutOnQueueTimer = false;
        private bool m_RequestedToStopBot = false;
        private bool m_RequestingServerData = false;

        private string m_State = "";
        private int m_RetryCount = 0;
        private int m_RetryCountServerError = 0;
        private int m_CurrentTownIntern = 0;
        private int m_StateManagerMode = 0;
        private int m_StateManagerCount = 3;
        private bool m_AddedExtraBuildingInNonTargetQueueMode = false;

        private string m_H = "";//csrfToken
        private string m_Nlreq_id = "0";
        private string m_ServerTimer = "";
        private string m_ServerTimerOffset = "";
        private string m_DisconnectedString = "/start?action=login";
        //private string m_ConqueredString = "id=\"conquest\">";//todo check this for version 2.0

        private Timer m_RequestTimer = new Timer();

        //Custom classes
        private HttpHandler m_HttpHandler = new HttpHandler();
        private HttpHandler m_HttpHandlerCheckVersion = new HttpHandler();
        private Player m_Player = new Player();
        private jdTimer m_RefreshTimer = new jdTimer();
        private jdTimer m_QueueTimer = new jdTimer();
        private jdTimer m_ReconnectTimer = new jdTimer();
        private jdTimer m_ConnectedTimer = new jdTimer();

        //Events
        public delegate void SetStatusBarHandler(object sender, CustomArgs ca);
        public event SetStatusBarHandler statusBarUpdated;
        public delegate void UpdateFarmerGUIHandler(object sender, CustomArgs ca);
        public event UpdateFarmerGUIHandler farmerGUIUpdated;
        public delegate void SetGuiToTimeoutProcessedStateHandler(object sender, CustomArgs ca);
        public event SetGuiToTimeoutProcessedStateHandler timeoutProcessedStateChanged;
        public delegate void SetGuiToLoggedInStateHandler(object sender, CustomArgs ca);
        public event SetGuiToLoggedInStateHandler loggedInStateChanged;
        public delegate void UpdateGUIGodHandler(object sender, CustomArgs ca);
        public event UpdateGUIGodHandler godGUIUpdated;
        public delegate void updateRefreshTimerHandler(object sender, CustomArgs ca);
        public event updateRefreshTimerHandler refreshTimerUpdated;
        public delegate void updateQueueTimerHandler(object sender, CustomArgs ca);
        public event updateQueueTimerHandler queueTimerUpdated;
        public delegate void updateReconnectTimerHandler(object sender, CustomArgs ca);
        public event updateReconnectTimerHandler reconnectTimerUpdated;
        public delegate void updateConnectedTimerHandler(object sender, CustomArgs ca);
        public event updateConnectedTimerHandler connectedTimerUpdated;
        public delegate void updateBuildingQueueHandler(object sender, CustomArgs ca);
        public event updateBuildingQueueHandler buildingQueueUpdated;
        public delegate void versionInfoHandler(object sender, CustomArgs ca);
        public event versionInfoHandler versionInfoUpdated;
        public delegate void logHandler(object sender, CustomArgs ca);
        public event logHandler logUpdated;
        public delegate void serverRequestDelayRequestHandler(object sender, CustomArgs ca);
        public event serverRequestDelayRequestHandler serverRequestDelayRequested;
        
//-->Constructor

        public Controller()
        {
            initEventHandlers();
            initTimers();
            initHttpHandlers();
        }

//-->Attributes

        //General
        public bool LoggedIn
        {
            get { return m_LoggedIn; }
            set { m_LoggedIn = value; }
        }

        public bool LoggedInOnce
        {
            get { return m_LoggedInOnce; }
            set { m_LoggedInOnce = value; }
        }

        public bool IsBotReadyToStart
        {
            get { return m_IsBotReadyToStart; }
            set { m_IsBotReadyToStart = value; }
        }
        
        public bool IsBotRunning
        {
            get { return m_IsBotRunning; }
            set { m_IsBotRunning = value; }
        }

        public bool IsBotRunningOnce
        {
            get { return m_IsBotRunningOnce; }
            set { m_IsBotRunningOnce = value; }
        }

        public bool RequestedToStopBot
        {
            get { return m_RequestedToStopBot; }
            set { m_RequestedToStopBot = value; }
        }

        public bool RequestingServerData
        {
            get { return m_RequestingServerData; }
            set { m_RequestingServerData = value; }
        }

        public string H
        {
            get { return m_H; }
            set { m_H = value; }
        }

        public string Nlreq_id
        {
            get { return m_Nlreq_id; }
            set { m_Nlreq_id = value; }
        }

        public string ServerTimer
        {
            get { return m_ServerTimer; }
            set { m_ServerTimer = value; }
        }

        public string ServerTimerOffset
        {
            get { return m_ServerTimerOffset; }
            set { m_ServerTimerOffset = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public Timer RequestTimer
        {
            get { return m_RequestTimer; }
            set { m_RequestTimer = value; }
        }

        //Custom classes
        public Player Player
        {
            get { return m_Player; }
            set { m_Player = value; }
        }

        public jdTimer ReconnectTimer
        {
            get { return m_ReconnectTimer; }
            set { m_ReconnectTimer = value; }
        }

//-->Methods

        private void initEventHandlers()
        {
            m_HttpHandler.UploadValuesCompleted += new System.Net.UploadValuesCompletedEventHandler(m_HttpHandler_UploadValuesCompleted);
            m_HttpHandler.DownloadStringCompleted += new System.Net.DownloadStringCompletedEventHandler(m_HttpHandler_DownloadStringCompleted);
            m_HttpHandlerCheckVersion.DownloadStringCompleted += new DownloadStringCompletedEventHandler(m_HttpHandlerCheckVersion_DownloadStringCompleted);
            m_RefreshTimer.InternalTimer.Elapsed += new System.Timers.ElapsedEventHandler(RefreshTimer_InternalTimer_Elapsed);
            m_QueueTimer.InternalTimer.Elapsed += new System.Timers.ElapsedEventHandler(QueueTimer_InternalTimer_Elapsed);
            m_ReconnectTimer.InternalTimer.Elapsed += new System.Timers.ElapsedEventHandler(ReconnectTimer_InternalTimer_Elapsed);
            m_ConnectedTimer.InternalTimer.Elapsed +=new System.Timers.ElapsedEventHandler(ConnectedTimer_InternalTimer_Elapsed);
            m_RequestTimer.Tick += new EventHandler(m_RequestTimer_Tick);
        }

        public void initTimers()
        {
            Settings l_Settings = Settings.Instance;

            m_RefreshTimer.Duration = l_Settings.AdvTimerRefresh;
            m_QueueTimer.Duration = l_Settings.QueueTimer;
            m_ReconnectTimer.Duration = l_Settings.AdvTimerReconnect;
            m_ConnectedTimer.Duration = 0;
            m_RequestTimer.Interval = 100;
        }

        private void initHttpHandlers()
        {
            m_HttpHandlerCheckVersion.Headers.Add("Referer", AssemblyTitle + " " + AssemblyVersion);
        }

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public void checkVersion()
        {
            Settings l_Settings = Settings.Instance;
            IOHandler l_IOHandler = IOHandler.Instance;

            try
            {
                string l_Url = "http://bots.uthar.nl/files/grepolis2latestversion.txt";
                Uri l_Uri = new Uri(l_Url);
                m_HttpHandlerCheckVersion.DownloadStringAsync(l_Uri);
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in checkVersion(): " + e.Message);
            }
        }

        public void setCookies(string p_Cookies)
        {
            Settings l_Settings = Settings.Instance;
            IOHandler l_IOHandler = IOHandler.Instance;

            try
            {
                CookieCollection l_CookieCollection = new CookieCollection();
                string[] l_CookieArray = p_Cookies.Split(';');
                for (int i = 0; i < l_CookieArray.Length; i++)
                {
                    l_CookieArray[i] = l_CookieArray[i].Trim();
                    l_CookieCollection.Add(new Cookie(l_CookieArray[i].Split('=')[0], l_CookieArray[i].Split('=')[1]));
                }
                m_HttpHandler.CookieContainer.Add(new Uri("http://" + l_Settings.GenServer), l_CookieCollection);
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in setCookies(): " + e.Message);
            }
        }

        public string fixSpecialCharacters(string p_String)
        {
            Regex l_Regex = new Regex(@"\\[uU]([0-9A-F]{4})", RegexOptions.IgnoreCase);
            string l_String = p_String;

            //Fix special characters
            l_String = l_Regex.Replace(l_String, l_Match => ((char)int.Parse(l_Match.Groups[1].Value, NumberStyles.HexNumber)).ToString());
            l_String = l_String.Replace("\\", "");

            return l_String;
        }

        /*
         * Start bot using the start button on the GUI
         */
        public void startbot()
        {
            Settings l_Settings = Settings.Instance;

            if (m_LoggedIn && m_IsBotReadyToStart && !m_IsBotRunning)
            {
                m_IsBotRunning = true;//Set to true again when there was a reconnect needed
                m_RefreshTimer.start(randomizeTimer(l_Settings.AdvTimerRefresh));

                m_QueueTimer.start(randomizeTimer(l_Settings.QueueTimer));
                m_TimeoutOnQueueTimer = false;
            }
        }

        /*
         * Stop bot
         */
        public void stop()
        {
            if (!m_RequestingServerData)
            {
                m_RefreshTimer.stop();
                m_QueueTimer.stop();
                m_IsBotRunning = false;
            }
            else
            {
                MessageBox.Show("The bot will stop when it's done updating your towns.");
                m_RequestedToStopBot = true;
            }
        }

        public void resumeTimers()
        {
            Settings l_Settings = Settings.Instance;

            if (m_LoggedIn && m_IsBotReadyToStart)
            {
                m_IsBotRunning = true;//Set to true again when there was a reconnect needed
                m_RefreshTimer.start(randomizeTimer(l_Settings.AdvTimerRefresh));

                if (m_TimeoutOnQueueTimer)
                {
                    m_QueueTimer.start(randomizeTimer(l_Settings.QueueTimer));
                    m_TimeoutOnQueueTimer = false;
                }
                else
                    m_QueueTimer.resume();
            }
        }

        public void startConnectedTimer()
        {
            m_ConnectedTimer.start(0);
        }

        /*
         * Starts reconnect sequence
         */
        public void startReconnect()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            l_IOHandler.debug("Connection to server lost. Starting reconnect...");
            m_LoggedIn = false;
            m_IsBotReadyToStart = false;
            m_IsBotRunning = false;
            m_TimeoutOnQueueTimer = false;
            m_RequestedToStopBot = false;
            m_RequestingServerData = false;
            m_CurrentTownIntern = 0;
            m_RefreshTimer.stop();
            m_QueueTimer.stop();
            m_ConnectedTimer.stop();
            m_HttpHandler.clearCookies();
            setStatusBarEvent("Waiting for reconnect");

            Random l_Random = new Random();
            m_ReconnectTimer.start(l_Random.Next(l_Settings.AdvTimerReconnect, l_Settings.AdvTimerReconnect + 20));
        }

        private double randomizeTimer(int p_Interval)
        {
            double l_Quotient = 0;

            Random l_Random = new Random();
            l_Quotient = l_Random.Next(-10, 10);
            l_Quotient = 100 + l_Quotient;//Value between 90% and 110%;
            l_Quotient = (l_Quotient / 100.0);//Value between 0.9 and 1.1
            return p_Interval * l_Quotient;
        }

        private void randomizeStateManagerMode()
        {
            Random l_Random = new Random();
            m_StateManagerMode = l_Random.Next(m_StateManagerCount);
        }

//-->Events

        private void setStatusBarEvent(string p_Message)
        {
            CustomArgs l_CustomArgs = new CustomArgs(p_Message);
            statusBarUpdated(this, l_CustomArgs);
        }

        private void versionInfoEvent(string p_VersionInfo)
        {
            CustomArgs l_CustomArgs = new CustomArgs(p_VersionInfo);
            versionInfoUpdated(this, l_CustomArgs);
        }

        private void updateFarmerGUIEvent()
        {
            CustomArgs l_CustomArgs = new CustomArgs("");
            farmerGUIUpdated(this, l_CustomArgs);
        }

        private void setGuiToTimeoutProcessedStateEvent()
        {
            CustomArgs l_CustomArgs = new CustomArgs("");
            timeoutProcessedStateChanged(this, l_CustomArgs);

            m_CurrentTownIntern = 0;
            m_RequestingServerData = false;
        }

        private void setGuiToLoggedInStateEvent()
        {
            CustomArgs l_CustomArgs = new CustomArgs("");
            loggedInStateChanged(this, l_CustomArgs);

            m_CurrentTownIntern = 0;
        }

        private void updateGUIGodEvent()
        {
            CustomArgs l_CustomArgs = new CustomArgs("");
            godGUIUpdated(this, l_CustomArgs);
        }

        private void updateBuildingQueueEvent(string p_Index)
        {
            CustomArgs l_CustomArgs = new CustomArgs(p_Index);
            buildingQueueUpdated(this, l_CustomArgs);
        }

        private void logEvent(string p_Message)
        {
            CustomArgs l_CustomArgs = new CustomArgs(p_Message);
            logUpdated(this, l_CustomArgs);
        }

        private void updateRefreshTimerEvent()
        {
            m_RefreshTimer.InternalTimer.Stop();

            CustomArgs l_CustomArgs = new CustomArgs(m_RefreshTimer.getTimeLeft());
            refreshTimerUpdated(this, l_CustomArgs);

            if (m_RefreshTimer.isTimerDone())
            {
                m_RefreshTimer.stop();
                m_QueueTimer.stop();
                if (m_QueueTimer.isTimerDone())
                    m_TimeoutOnQueueTimer = true;
                else
                    m_TimeoutOnQueueTimer = false;
                m_RequestingServerData = true;
                locateFarmers();

            }
            else
                m_RefreshTimer.InternalTimer.Start();
        }

        private void updateQueueTimerEvent()
        {
            m_QueueTimer.InternalTimer.Stop();

            CustomArgs l_CustomArgs = new CustomArgs(m_QueueTimer.getTimeLeft());
            queueTimerUpdated(this, l_CustomArgs);

            if (m_QueueTimer.isTimerDone())
            {
                m_QueueTimer.stop();
                m_TimeoutOnQueueTimer = true;
            }
            else
            {
                m_TimeoutOnQueueTimer = false;
                m_QueueTimer.InternalTimer.Start();
            }
        }

        private void updateReconnectTimerEvent()
        {
            m_ReconnectTimer.InternalTimer.Stop();

            CustomArgs l_CustomArgs = new CustomArgs(m_ReconnectTimer.getTimeLeft());
            reconnectTimerUpdated(this, l_CustomArgs);
        }

        private void updateConnectedTimerEvent()
        {
            m_ConnectedTimer.InternalTimer.Stop();

            CustomArgs l_CustomArgs = new CustomArgs(m_ConnectedTimer.getElapsedTime());
            connectedTimerUpdated(this, l_CustomArgs);

            m_ConnectedTimer.InternalTimer.Start();
        }

//-->Http requests and handlers

        /*
         * Validate response
         * Codes:
         * 1 = OK
         * 2 = Reconnect necessarily
         */ 
        private int validateResponse(string p_Response, string p_Method)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            int l_ValidCode = 1;
            //58 {"redirect":"http:\/\/beta.grepolis.com\/start?nosession"}8|10 _srvtime1303484209
            if (!p_Response.Contains(m_DisconnectedString) && !p_Response.Contains("start?nosession"))//Checks if you're still connected with the server
            {
                if (p_Response.Contains(": startIndex"))//Error caused by game engine
                {
                    l_ValidCode = 3;
                    if (l_Settings.AdvDebugMode)
                        l_IOHandler.saveServerResponse(p_Method, p_Response);
                }
                else
                {
                    //Add future checks here
                    l_ValidCode = 1;
                }
            }
            else//you're disconnected from server
            {
                l_ValidCode = 2;
            }

            return l_ValidCode;
        }

        private void processValidatedResponse(int p_ValidCode)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            switch (p_ValidCode)
            {
                case 2://Reconnect necessarily
                    startReconnect();
                    if (l_Settings.AdvDebugMode)
                        l_IOHandler.debug("Server response indicated that you were no longer logged in.");
                    break;
                case 3://Game server error, reconnect necessarily
                    startReconnect();
                    if (l_Settings.AdvDebugMode)
                        l_IOHandler.debug("Server response indicated that there was a gameserver error.");
                    break;
            }
        }

        public void locateFarmers()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "locatefarmers";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Locating farmers");
                //http://###.grepolis.com/game/data?action=get&town_id=#####&h=###########
                //json	{"type":"map"}
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/data?action=get&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"type\":\"map\"}");//json {"type":"map"}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in locateFarmers(): " + e.Message);
            }
        }

        private void locateFarmersResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                //{"id":####,"name":"********","dir":"n","expansion_stage":4,"x":###,"y":###,"ox":###,"oy":###,"offer":"stone","demand":"iron","mood":14,"relation_status":1,"ratio":1.25,"loot":1303078581,"lootable_human":"tomorrow at 12:16 AM ","looted":1303042581}
                int l_Index = -1;
                int l_Index2 = -1;
                string l_Search = "";
                string l_ID = "";
                string l_Name = "";
                string l_IslandX = "";
                string l_IslandY = "";
                string l_Mood = "";
                string l_RelationStatus = "";
                string l_LootTimer = "";
                string l_LootTimerHuman = "";

                List<string> l_ProcessedFarmers = new List<string>();

                int l_ValidCode = validateResponse(p_Response, "locateFarmersResponse");
                if (l_ValidCode == 1)
                {
                    l_Search = "\"relation_status\":";
                    l_Index = p_Response.IndexOf(l_Search, 0);

                    while (l_Index != -1)
                    {
                        while (!p_Response[l_Index].Equals('{'))
                            l_Index--;
                        l_Search = "\"id\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        //Get farmers id
                        l_ID = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"name\":\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        //Get farmers name
                        l_Name = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Name = fixSpecialCharacters(l_Name);
                        l_Search = "\"x\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);

                        //Get farmers island x coord
                        l_IslandX = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"y\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        //Get farmers island y coord
                        l_IslandY = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"mood\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        //Get farmers mood
                        l_Mood = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"relation_status\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        //Get farmers relation status
                        l_RelationStatus = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        if (l_RelationStatus.Equals("1"))
                        {
                            l_Search = "\"loot\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            //Get farmers loot time
                            l_LootTimer = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"lootable_human\":\"";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            //Get farmers loot time human
                            l_LootTimerHuman = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_LootTimerHuman = fixSpecialCharacters(l_LootTimerHuman);
                        }
                        else
                        {
                            l_LootTimer = "0";
                            l_LootTimerHuman = "Not available";
                        }

                        //Add or update farmer
                        if (m_Player.Towns[m_CurrentTownIntern].IslandX.Equals(l_IslandX) && m_Player.Towns[m_CurrentTownIntern].IslandY.Equals(l_IslandY))
                        {
                            if (m_Player.Towns[m_CurrentTownIntern].isUniqueFarm(l_ID))
                                m_Player.Towns[m_CurrentTownIntern].Farmers.Add(new Farmer(l_ID, l_Name, l_IslandX, l_IslandY, l_Mood, l_RelationStatus, l_LootTimer, l_LootTimerHuman));
                            else
                            {
                                l_Index2 = m_Player.Towns[m_CurrentTownIntern].getFarmersIndex(l_ID);
                                m_Player.Towns[m_CurrentTownIntern].Farmers[l_Index2].Mood = int.Parse(l_Mood);
                                m_Player.Towns[m_CurrentTownIntern].Farmers[l_Index2].RelationStatus = l_RelationStatus.Equals("1");
                                m_Player.Towns[m_CurrentTownIntern].Farmers[l_Index2].LootTimer = l_LootTimer;
                                m_Player.Towns[m_CurrentTownIntern].Farmers[l_Index2].LootTimerHuman = l_LootTimerHuman;
                            }
                        }

                        //Search next farmer
                        l_Search = "\"relation_status\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                    }

                    //Update server time
                    l_Search = "mtime";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    ServerTimer = p_Response.Substring(l_Index + l_Search.Length, p_Response.Length - (l_Index + l_Search.Length));

                    updateFarmerGUIEvent();
                    stateManagerDelay();
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("locateFarmersResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void generalInfo()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "generalinfo";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Collecting town data");
                //http://###.grepolis.com/game/notify?action=fetch&town_id=####&h=###########
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/notify?action=fetch&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"bar_mask\":4095,\"no_sysmsg\":false,\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"bar_mask":4095,"no_sysmsg":false,"town_id":"####","nlreq_id":0}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in generalInfo(): " + e.Message);
            }
        }

        private void generalInfoResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string l_Search = "";
                int l_Index = -1;
                int l_Index2 = -1;
                //Notifications
                string l_Notifications = "";
                string l_Notify_id = "";
                string l_Time = "";
                string l_Type = "";
                string l_Subject = "";
                //Resources
                string l_Wood = "0";
                string l_Stone = "0";
                string l_Iron = "0";
                string l_Storage = "0";
                string l_ResourcePlenty = "";
                string l_ResourceRare = "";
                //Building queue
                string l_BuildingName = "";
                string l_BuildingQueueParsed = "";

                int l_ValidCode = validateResponse(p_Response, "generalInfoResponse");
                if (l_ValidCode == 1)
                {
                    //Get notifications
                    l_Search = "\"notifications\":[";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    l_Notifications = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("]", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    if (l_Notifications.Length > 0)
                    {
                        l_Search = "\"notify_id\":";
                        l_Index = l_Notifications.IndexOf(l_Search, 0);
                        while (l_Index != -1)
                        {
                            l_Notify_id = l_Notifications.Substring(l_Index + l_Search.Length, l_Notifications.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"time\":";
                            l_Index = l_Notifications.IndexOf(l_Search, l_Index);
                            l_Time = l_Notifications.Substring(l_Index + l_Search.Length, l_Notifications.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"type\":\"";
                            l_Index = l_Notifications.IndexOf(l_Search, l_Index);
                            l_Type = l_Notifications.Substring(l_Index + l_Search.Length, l_Notifications.IndexOf("\"", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"subject\":\"";
                            l_Index = l_Notifications.IndexOf(l_Search, l_Index);
                            l_Subject = l_Notifications.Substring(l_Index + l_Search.Length, l_Notifications.IndexOf("\"}", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            if (l_Subject.Contains("class="))
                            {
                                l_Search = "<b>";
                                l_Index2 = l_Subject.IndexOf(l_Search);
                                l_Subject = l_Subject.Substring(l_Index2 + l_Search.Length, l_Subject.IndexOf("<", l_Index2 + l_Search.Length) - (l_Index2 + l_Search.Length));
                            }
                            //Add notification
                            m_Player.addNotification(l_Notify_id, l_Time, l_Type, l_Subject);

                            //Search next notification
                            l_Search = "\"notify_id\":";
                            l_Index = l_Notifications.IndexOf(l_Search, l_Index);
                        }
                    }
                    //Get resources
                    l_Search = "{\"wood\":";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    l_Wood = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    l_Search = "\"stone\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    l_Stone = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    l_Search = "\"iron\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    l_Iron = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("}", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Get storage
                    l_Search = "\"storage\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    l_Storage = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Get resource constraints
                    l_Search = "{\"plenty\":\"";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    l_ResourcePlenty = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    l_Search = "\"rare\":\"";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    l_ResourceRare = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\"}", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Set resource info
                    m_Player.Towns[m_CurrentTownIntern].Wood = int.Parse(l_Wood);
                    m_Player.Towns[m_CurrentTownIntern].Stone = int.Parse(l_Stone);
                    m_Player.Towns[m_CurrentTownIntern].Iron = int.Parse(l_Iron);
                    m_Player.Towns[m_CurrentTownIntern].Storage = int.Parse(l_Storage);
                    m_Player.Towns[m_CurrentTownIntern].ResourcePlenty = l_ResourcePlenty;
                    m_Player.Towns[m_CurrentTownIntern].ResourceRare = l_ResourceRare;

                    //Zeus
                    l_Search = "\"current\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorZeus = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                    l_Search = "\"production\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorZeusProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Poseidon
                    l_Search = "\"current\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorPoseidon = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                    l_Search = "\"production\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorPoseidonProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Hera
                    l_Search = "\"current\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorHera = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                    l_Search = "\"production\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorHeraProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Athena
                    l_Search = "\"current\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorAthene = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                    l_Search = "\"production\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorAtheneProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Hades
                    l_Search = "\"current\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorHades = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                    l_Search = "\"production\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.FavorHadesProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    //Current god
                    l_Search = "\"god\":\"";
                    l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                    l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                    if (l_Index == -1)
                        m_Player.Towns[m_CurrentTownIntern].God = "none";
                    else
                        m_Player.Towns[m_CurrentTownIntern].God = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                    //Being conquered
                    l_Search = "\"has_conquerer\":";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    m_Player.Towns[m_CurrentTownIntern].HasConquerer = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                    //Population
                    l_Search = "\"population\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.Towns[m_CurrentTownIntern].Population = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length)));

                    //Building queue
                    l_Search = "\"building_orders\":";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    l_Search = "\"building_type\":\"";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    while (l_Index != -1)
                    {
                        l_BuildingName = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_BuildingQueueParsed += l_BuildingName + ";";
                        //Search for next building
                        l_Search = "\"building_type\":\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                    }
                    m_Player.Towns[m_CurrentTownIntern].setIngameBuildingQueue(l_BuildingQueueParsed);

                    //Incomming attacks
                    l_Search = "\"incomming_attack_count\":";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    bool l_ParseSuccess = false;
                    int l_ParseResult = -1;
                    l_ParseSuccess = int.TryParse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("}", l_Index + l_Search.Length) - (l_Index + l_Search.Length)), out l_ParseResult);
                    if (!l_ParseSuccess)
                        l_ParseSuccess = int.TryParse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length)), out l_ParseResult);
                    m_Player.Towns[m_CurrentTownIntern].IncomingAttacks = l_ParseResult;

                    //Server time
                    l_Search = "_srvtime";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    ServerTimer = p_Response.Substring(l_Index + l_Search.Length, p_Response.Length - (l_Index + l_Search.Length));

                    updateGUIGodEvent();

                    if (m_Player.Towns[m_CurrentTownIntern].HasConquerer.Equals("1"))
                    {
                        if (m_IsBotRunning)
                        {
                            final();
                        }
                        else
                        {
                            m_CurrentTownIntern++;
                            if (m_CurrentTownIntern < m_Player.Towns.Count)
                                locateFarmers();
                            else
                            {
                                setGuiToLoggedInStateEvent();
                            }
                        }
                    }
                    else
                    {
                        stateManagerDelay();
                    }
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("generalInfoResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }
   
        private void countArmy()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "countarmy";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Collecting army data");
                //http://###.grepolis.com/game/menu_bubble?action=troops&town_id=####&h=###########
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/menu_bubble?action=troops&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"send_tmpl\":false,\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"send_tmpl":false,"town_id":"####","nlreq_id":##}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in countArmy(): " + e.Message);
            }
        }

        private void countArmyResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                int l_Index = -1;
                int l_UnitIndex = -1;
                string l_Unit = "";
                string l_LocalUnit = "";
                string l_UnitsLeft = "0";
                string l_Type = "";
                string l_Search = "";
                string l_CurrentAmount = "";
                string l_TotalAmount = "";
                string l_MaxBuild = "";

                int l_ValidCode = validateResponse(p_Response, "countArmyResponse");
                if (l_ValidCode == 1)
                {
                    //empty army queue (0-15 land, 16-23 navy)
                    for (int i = 0; i < 24; i++)
                    {
                        m_Player.Towns[m_CurrentTownIntern].ArmyUnits[i].QueueGame = 0;
                    }
                    //Reset size of army unit queue
                    m_Player.Towns[m_CurrentTownIntern].SizeOfLandUnitQueue = 0;
                    m_Player.Towns[m_CurrentTownIntern].SizeOfNavyUnitQueue = 0;

                    l_Search = "{\"unit_id\":\"";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    while (l_Index != -1)
                    {
                        l_Unit = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"units_left\":";
                        l_Index = l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_UnitsLeft = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"type\":\"";
                        l_Index = l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_Type = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\"", l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                        l_UnitIndex = m_Player.Towns[m_CurrentTownIntern].getIndexArmyUnit(l_Unit);
                        if (l_UnitIndex != -1)
                        {
                            m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].QueueGame += int.Parse(l_UnitsLeft);
                            if (l_Type.Equals("ground"))
                                m_Player.Towns[m_CurrentTownIntern].SizeOfLandUnitQueue++;
                            else if (l_Type.Equals("naval"))
                                m_Player.Towns[m_CurrentTownIntern].SizeOfNavyUnitQueue++;
                        }

                        l_Search = "{\"unit_id\":\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                    }
                    m_Player.Towns[m_CurrentTownIntern].SizeOfLandUnitQueue = p_Response.Split(new[] { "ground" }, StringSplitOptions.None).Length - 1;
                    m_Player.Towns[m_CurrentTownIntern].SizeOfNavyUnitQueue = p_Response.Split(new[] { "naval" }, StringSplitOptions.None).Length - 1;

                    //Reset count of army units (Extra safety when switching between gods when bot is running)
                    m_Player.Towns[m_CurrentTownIntern].resetArmyCurrentAmount();

                    //Current, total and trainable unit amount
                    l_Search = "{\"id\":\"";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    while (l_Index != -1)
                    {
                        l_Unit = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"name\":\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_LocalUnit = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_LocalUnit = fixSpecialCharacters(l_LocalUnit);
                        l_Search = "\"count\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                        l_CurrentAmount = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"total\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                        l_TotalAmount = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"max_build\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                        l_MaxBuild = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                        l_UnitIndex = m_Player.Towns[m_CurrentTownIntern].getIndexArmyUnit(l_Unit);
                        if (l_UnitIndex != -1)
                        {
                            m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].LocalName = l_LocalUnit;
                            m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].CurrentAmount = int.Parse(l_CurrentAmount);
                            m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].TotalAmount = int.Parse(l_TotalAmount);
                            m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].MaxBuild = int.Parse(l_MaxBuild);
                        }

                        l_Search = "{\"id\":\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                    }

                    //Server time
                    l_Search = "_srvtime";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    ServerTimer = p_Response.Substring(l_Index + l_Search.Length, p_Response.Length - (l_Index + l_Search.Length));

                    stateManagerDelay();
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("countArmyResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void updateBuildings()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "updatebuildings";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Collecting building data");
                //http://###.grepolis.com/game/building_main?action=index&town_id=#####&h=###########
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/building_main?action=index&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"town_id":"#####","nlreq_id":##}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateBuildings(): " + e.Message);
            }
        }

        private void updateBuildingsResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string l_Search = "";
                int l_Index = -1;
                string l_Upgradable = "";
                string l_DevName = "";
                int l_DevNameIndex = -1;
                string l_LocalName = "";
                int l_Level = 0;

                int l_ValidCode = validateResponse(p_Response, "updateBuildingsResponse");
                if (l_ValidCode == 1)
                {

                    l_Search = "\\\"next_level\\\":";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    while (l_Index != -1)
                    {
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_Level = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length))) - 1;//The -1 is needed because we search for the next level
                        l_Search = "\\\"can_upgrade\\\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_Upgradable = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\\\"controller\\\":\\\"building_";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_DevName = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\\\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\\\"name\\\":\\\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_LocalName = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\\\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_LocalName = fixSpecialCharacters(l_LocalName);

                        //Update building
                        if (!l_DevName.Equals("place"))
                        {
                            l_DevNameIndex = m_Player.Towns[m_CurrentTownIntern].getIndexBuilding(l_DevName);
                            m_Player.Towns[m_CurrentTownIntern].Buildings[l_DevNameIndex].LocalName = l_LocalName;
                            m_Player.Towns[m_CurrentTownIntern].Buildings[l_DevNameIndex].Level = l_Level;
                            m_Player.Towns[m_CurrentTownIntern].Buildings[l_DevNameIndex].Upgradable = l_Upgradable.Equals("true");
                        }

                        //Search for next building
                        l_Search = "\\\"next_level\\\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                    }

                    //Server time
                    l_Search = "_srvtime";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    ServerTimer = p_Response.Substring(l_Index + l_Search.Length, p_Response.Length - (l_Index + l_Search.Length));

                    if (m_IsBotRunning)
                    {
                        //Continue here when the bot is already running.
                        stateManagerDelay();
                    }
                    else
                    {
                        m_CurrentTownIntern++;
                        if (m_CurrentTownIntern < m_Player.Towns.Count)
                            locateFarmers();
                        else
                        {
                            setGuiToLoggedInStateEvent();
                        }
                    }
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("updateBuildingsResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void updateMovementInfo()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "updatemovementinfo";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Collecting movement data");
                //http://###.grepolis.com/game/menu_bubble?action=movement&town_id=#####&h=###########
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/menu_bubble?action=movement&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"send_tmpl\":false,\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"send_tmpl":false,"town_id":"#####","nlreq_id":##}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateMovementInfo(): " + e.Message);
            }
        }

        private void updateMovementInfoResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                //todo Extract movement information
                stateManagerDelay();
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("updateMovementInfoResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void updateTradeInfo()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "updatetradeinfo";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Collecting trade data");
                //http://###.grepolis.com/game/menu_bubble?action=movement&town_id=#####&h=###########
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/menu_bubble?action=trade&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"send_tmpl\":false,\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"send_tmpl":false,"town_id":"#####","nlreq_id":##}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateTradeInfo(): " + e.Message);
            }
        }

        private void updateTradeInfoResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                //todo Extract trade info
                stateManagerDelay();
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("updateTradeInfoResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void updateCulturalInfo()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "updateculturalinfo";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Collecting cultural data");
                //http://###.grepolis.com/game/building_place?action=culture&town_id=#####&h=###########
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/building_place?action=culture&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\"}");//json={"town_id":"######"}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateCulturalInfo(): " + e.Message);
            }
        }

        private void updateCulturalInfoResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string l_Search = "";
                string l_CulturalLevelStr = "";
                string l_CulturalCitiesStr = "";
                int l_Index = -1;


                int l_ValidCode = validateResponse(p_Response, "updateCulturalInfoResponse");
                if (l_ValidCode == 1)
                {
                    //Update cultural stats
                    l_Search = "<div id=\"place_culture_level\">";
                    l_Index = p_Response.IndexOf(l_Search, 0);
                    l_CulturalLevelStr = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("<", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    l_CulturalLevelStr = fixSpecialCharacters(l_CulturalLevelStr);

                    m_Player.CulturalLevelStr = l_CulturalLevelStr;
                    l_Search = "<div id=\"place_culture_towns\">";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    l_CulturalCitiesStr = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("<", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    l_CulturalCitiesStr = fixSpecialCharacters(l_CulturalCitiesStr);

                    m_Player.CulturalCitiesStr = l_CulturalCitiesStr;
                    l_Search = "<div id=\"place_culture_count\">";
                    l_Index = p_Response.IndexOf(l_Search, l_Index);
                    m_Player.CulturalPointsStr = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("<", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                    m_Player.CulturalPointsCurrent = int.Parse(m_Player.CulturalPointsStr.Split('/')[0]);
                    m_Player.CulturalPointsMax = int.Parse(m_Player.CulturalPointsStr.Split('/')[1]);

                    //todo check status cultural festivals

                    stateManagerDelay();
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("updateCulturalInfoResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void updateResearch()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "updateresearch";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Collecting research data");
                //http://###.grepolis.com/game/building_academy?action=index&town_id=#####&h=###########
                NameValueCollection l_Content = new NameValueCollection();
                string l_Url = "http://" + l_Settings.GenServer + "/game/building_academy?action=index&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                Uri l_Uri = new Uri(l_Url);
                l_Content.Add("json", "{\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"town_id":"#####","nlreq_id":#####}

                m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                m_HttpHandler.Headers.Remove("X-Requested-With");
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateResearch(): " + e.Message);
            }
        }

        private void updateResearchResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                //todo Extract research info
                stateManagerDelay();
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("updateResearchResponse", e.Message + "\n" + p_Response);

                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void attackFarmers()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            int l_FarmersIndex = -1;
            bool l_attackFarmersComplete = true;

            try
            {
                m_State = "attackfarmers";
                setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Demanding resources from farmers");
                //http://###.grepolis.com/game/farm_town_info?action=claim_load&town_id=#####&h=###########

                //Reset limit status every 2 hours
                if (DateTime.Now.Subtract(Player.FarmersResetTime).Hours >= 2)
                {
                    Player.FarmersResetTime = DateTime.Now;
                    Player.resetDailyFarmLimit();
                }

                if (m_Player.Towns[m_CurrentTownIntern].Farmers.Count > 0 && m_Player.Towns[m_CurrentTownIntern].FarmersLootEnabled)//Check if current town has farmers on his Island
                {
                    if (!m_Player.Towns[m_CurrentTownIntern].isStorageFull())//Check if storage is full
                    {
                        l_FarmersIndex = m_Player.Towns[m_CurrentTownIntern].getLootableFarmer(ServerTimer);
                        if (l_FarmersIndex != -1)//Check if there is a farmer that we can loot
                        {
                            l_attackFarmersComplete = false;

                            NameValueCollection l_Content = new NameValueCollection();
                            string l_Url = "http://" + l_Settings.GenServer + "/game/farm_town_info?action=claim_load&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                            Uri l_Uri = new Uri(l_Url);

                            if (m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].Mood >= m_Player.Towns[m_CurrentTownIntern].FarmersMinMood && !m_Player.Towns[m_CurrentTownIntern].FarmersFriendlyDemandsOnly)//Check if the type of the loot is non-friendly
                            {
                                l_Content.Add("json", "{\"target_id\":\"" + m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].ID + "\",\"claim_type\":\"double\",\"time\":" + (int.Parse(m_Player.Towns[m_CurrentTownIntern].FarmersLootInterval) * 60).ToString() + ",\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + Nlreq_id + "}");//json={"target_id":"####","claim_type":"double","time":###,"town_id":"#####","nlreq_id":###} (Hostile)
                            }
                            else//Type of loot is friendly
                            {
                                l_Content.Add("json", "{\"target_id\":\"" + m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].ID + "\",\"claim_type\":\"normal\",\"time\":" + (int.Parse(m_Player.Towns[m_CurrentTownIntern].FarmersLootInterval) * 60).ToString() + ",\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + Nlreq_id + "}");//json={"target_id":"####","claim_type":"normal","time":###,"town_id":"#####","nlreq_id":###} (Friendly)
                            }

                            m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                            m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                            m_HttpHandler.Headers.Remove("X-Requested-With");
                        }
                        else
                        {
                            logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": No farmers available at the moment.");
                        }
                    }
                    else
                    {
                        logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Farmers are temporarily disabled for this town, your warehouse is full.");
                    }
                }
                else
                {
                    logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Farmers are disabled for this town.");
                }

                if (l_attackFarmersComplete)
                {
                    if (m_TimeoutOnQueueTimer)
                    {
                        checkBuildingQueue();
                    }
                    else
                    {
                        final();
                    }
                }
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in attackFarmers(): " + e.Message);
            }
        }

        private void attackFarmersResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string l_Search = "";
                int l_Index = -1;
                int l_FarmersIndex = -1;
                string l_RelationStatus = "0";
                string l_LootTimerHuman = "Not available";
                string l_Mood = "0";
                string l_ServerTimer = "0";
                string l_Wood = "";
                string l_Stone = "";
                string l_Iron = "";
                string l_Error = "";

                int l_ValidCode = validateResponse(p_Response, "attackFarmersResponse");
                if (l_ValidCode == 1)
                {
                    if (!p_Response.Contains("{\"error\":\""))
                    {
                        l_FarmersIndex = m_Player.Towns[m_CurrentTownIntern].getLootableFarmer(ServerTimer);
                        l_Search = "{\"relation_status\":";
                        l_Index = p_Response.IndexOf(l_Search, 0);
                        l_RelationStatus = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"lootable_human\":\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_LootTimerHuman = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_LootTimerHuman = fixSpecialCharacters(l_LootTimerHuman);

                        l_Search = "\"satisfaction\":\"";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_Mood = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "{\"wood\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_Wood = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"stone\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_Stone = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "\"iron\":";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_Iron = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("},", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        l_Search = "_srvtime";
                        l_Index = p_Response.IndexOf(l_Search, l_Index);
                        l_ServerTimer = p_Response.Substring(l_Index + l_Search.Length, p_Response.Length - (l_Index + l_Search.Length));

                        m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].RelationStatus = l_RelationStatus.Equals("1");
                        m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].LootTimerHuman = l_LootTimerHuman;
                        if (l_Mood.Contains("."))
                            l_Mood = l_Mood.Substring(0, l_Mood.IndexOf("."));
                        m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].Mood = int.Parse(l_Mood);
                        m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].LootTimer = (long.Parse(l_ServerTimer) + long.Parse(m_Player.Towns[m_CurrentTownIntern].FarmersLootInterval)).ToString();

                        m_Player.Towns[m_CurrentTownIntern].Wood = int.Parse(l_Wood);
                        m_Player.Towns[m_CurrentTownIntern].Stone = int.Parse(l_Stone);
                        m_Player.Towns[m_CurrentTownIntern].Iron = int.Parse(l_Iron);

                        ServerTimer = l_ServerTimer;

                        logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Successfull looted/demanded resources from farmer " + m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].Name);

                        m_State = "attackfarmersContinue";
                        stateManagerDelay();
                    }
                    else
                    {
                        //Set daily limit reached
                        l_FarmersIndex = m_Player.Towns[m_CurrentTownIntern].getLootableFarmer(ServerTimer);
                        m_Player.Towns[m_CurrentTownIntern].Farmers[l_FarmersIndex].FarmersLimitReached = true;

                        if (l_Settings.AdvDebugMode)
                        {
                            l_Search = "{\"error\":\"";
                            l_Index = p_Response.IndexOf(l_Search, 0);
                            l_Error = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\"", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Error = fixSpecialCharacters(l_Error);

                            l_IOHandler.debug("Ingame error in attackFarmersResponse(): " + l_Error);
                        }
                        attackFarmers();
                        /*if (m_TimeoutOnQueueTimer)
                            checkBuildingQueue();
                        else
                            final();*/
                    }
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("attackFarmersResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void checkBuildingQueue()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "checkbuildingqueue";
                m_AddedExtraBuildingInNonTargetQueueMode = false;

                if (m_Player.Towns[m_CurrentTownIntern].BuildingQueueEnabled)
                {
                    string l_Building = "";
                    if (m_Player.Towns[m_CurrentTownIntern].BuildingLevelsTargetEnabled)
                    {
                        //Automatic mode activad
                        int l_LowestLevel = 41;
                        int l_Level = 0;
                        string l_BuildingTemp = "";
                        for (int i = 0; i < m_Player.Towns[m_CurrentTownIntern].Buildings.Count; i++)
                        {
                            l_BuildingTemp = m_Player.Towns[m_CurrentTownIntern].Buildings[i].DevName;
                            l_Level = m_Player.Towns[m_CurrentTownIntern].Buildings[i].Level + m_Player.Towns[m_CurrentTownIntern].countIngameBuildingQueueByName(l_BuildingTemp);
                            if (m_Player.Towns[m_CurrentTownIntern].Buildings[i].Upgradable)
                            {
                                if (l_Level < m_Player.Towns[m_CurrentTownIntern].Buildings[i].TargetLevel)
                                {
                                    if(l_Level < l_LowestLevel)
                                    {
                                        l_Building = l_BuildingTemp;
                                        l_LowestLevel = l_Level;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //Normal mode activated
                        if (m_Player.Towns[m_CurrentTownIntern].BuildingQueue.Count > 0)
                        {
                            //Check if max level is already reached
                            int l_BuildingIndex = m_Player.Towns[m_CurrentTownIntern].getIndexBuilding(m_Player.Towns[m_CurrentTownIntern].peekQueueBuilding());
                            while (m_Player.Towns[m_CurrentTownIntern].Buildings[l_BuildingIndex].Level >= m_Player.Towns[m_CurrentTownIntern].Buildings[l_BuildingIndex].MaxLevel)
                            {
                                m_Player.Towns[m_CurrentTownIntern].dequeueBuilding();
                                l_BuildingIndex = m_Player.Towns[m_CurrentTownIntern].getIndexBuilding(m_Player.Towns[m_CurrentTownIntern].peekQueueBuilding());
                            }

                            if (m_Player.Towns[m_CurrentTownIntern].Buildings[l_BuildingIndex].Upgradable)
                            {
                                l_Building = m_Player.Towns[m_CurrentTownIntern].peekQueueBuilding();
                            }
                            else
                            {
                                //Check if you don't have enough population
                                if (m_Player.Towns[m_CurrentTownIntern].Population < 10)
                                {
                                    //Check if you can/should build a farm
                                    if(!m_Player.Towns[m_CurrentTownIntern].peekQueueBuilding().Equals("farm") &&
                                        !m_Player.Towns[m_CurrentTownIntern].getIngameBuildingQueue().Contains("farm") &&
                                        m_Player.Towns[m_CurrentTownIntern].Buildings[m_Player.Towns[m_CurrentTownIntern].getIndexBuilding("farm")].Upgradable &&
                                        m_Player.Towns[m_CurrentTownIntern].Buildings[m_Player.Towns[m_CurrentTownIntern].getIndexBuilding("farm")].Level < m_Player.Towns[m_CurrentTownIntern].Buildings[m_Player.Towns[m_CurrentTownIntern].getIndexBuilding("farm")].MaxLevel) 
                                    {
                                        l_Building = "farm";
                                        m_AddedExtraBuildingInNonTargetQueueMode = true;
                                    }
                                }
                                //Check if you need to build a warehouse (Extra checks needed to get this to work correctly)
                                /*if (!l_Building.Equals("farm") &&
                                    !m_Player.Towns[m_CurrentTownIntern].peekQueueBuilding().Equals("storage") &&
                                    !m_Player.Towns[m_CurrentTownIntern].getIngameBuildingQueue().Contains("storage") &&
                                    m_Player.Towns[m_CurrentTownIntern].Buildings[m_Player.Towns[m_CurrentTownIntern].getIndexBuilding("storage")].Upgradable &&
                                    m_Player.Towns[m_CurrentTownIntern].Buildings[m_Player.Towns[m_CurrentTownIntern].getIndexBuilding("storage")].Level < m_Player.Towns[m_CurrentTownIntern].Buildings[m_Player.Towns[m_CurrentTownIntern].getIndexBuilding("storage")].MaxLevel)
                                {
                                    l_Building = "storage";
                                    m_AddedExtraBuildingInNonTargetQueueMode = true;
                                }*/
                            }
                        }
                    }

                    if (!l_Building.Equals(""))
                    {
                        setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Checking building queue");
                        //http://###.grepolis.com/game/building_main?action=build&town_id=13511&h=3e2806c1a1e
                        NameValueCollection l_Content = new NameValueCollection();
                        String l_Url = "http://" + l_Settings.GenServer + "/game/building_main?action=build&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                        Uri l_Uri = new Uri(l_Url);
                        l_Content.Add("json", "{\"building\": \"" + l_Building + "\",\"wnd_main\":1,\"wnd_index\":1,\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"building":"####","wnd_main":1,"wnd_index":1,"town_id":"#####","nlreq_id":##}

                        logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": " + m_Player.Towns[m_CurrentTownIntern].Buildings[m_Player.Towns[m_CurrentTownIntern].getIndexBuilding(l_Building)].LocalName + " added to the ingame queue.");

                        m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                        m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                        m_HttpHandler.Headers.Remove("X-Requested-With");
                    }
                    else
                    {
                        logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Nothing to build.");

                        setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Nothing to build");
                        //Note This method is called when there is nothing to build (1)
                        checkLandArmyQueue();
                    }
                }
                else
                {
                    logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Building queue is disabled.");

                    //Building queue is disabled for this town
                    setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Building queue is disabled for this town");
                    //Note This method is called when there is nothing to build (2)
                    checkLandArmyQueue();
                }
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in checkBuildingQueue(): " + e.Message);
            }
        }

        private void checkBuildingQueueResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string l_Search = "\"bar\":";
                int l_Index = -1;
                string l_Wood = "0";
                string l_Stone = "0";
                string l_Iron = "0";
                string l_BuildingName = "";
                string l_BuildingQueueParsed = "";

                int l_ValidCode = validateResponse(p_Response, "checkBuildingQueueResponse");
                if (l_ValidCode == 1)
                {
                    if (!p_Response.Contains("{\"error\":\""))
                    {
                        setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Building added to ingame queue");
                        if (!m_Player.Towns[m_CurrentTownIntern].BuildingLevelsTargetEnabled && !m_AddedExtraBuildingInNonTargetQueueMode)
                        {
                            m_Player.Towns[m_CurrentTownIntern].dequeueBuilding();
                            updateBuildingQueueEvent(m_CurrentTownIntern.ToString());
                        }

                        l_Index = p_Response.IndexOf(l_Search, 0);
                        if (l_Index != -1)
                        {
                            //Update resources
                            l_Search = "\"resources\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Search = "\"wood\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Wood = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"stone\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Stone = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"iron\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Iron = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("}", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Set resource info
                            m_Player.Towns[m_CurrentTownIntern].Wood = int.Parse(l_Wood);
                            m_Player.Towns[m_CurrentTownIntern].Stone = int.Parse(l_Stone);
                            m_Player.Towns[m_CurrentTownIntern].Iron = int.Parse(l_Iron);

                            //Population
                            l_Search = "\"population\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.Towns[m_CurrentTownIntern].Population = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length)));

                            //Building queue
                            l_Search = "\"building_orders\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Search = "\"building_type\":\"";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            while (l_Index != -1)
                            {
                                l_BuildingName = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                                l_BuildingQueueParsed += l_BuildingName + ";";
                                //Search for next building
                                l_Search = "\"building_type\":\"";
                                l_Index = p_Response.IndexOf(l_Search, l_Index + l_Search.Length);
                            }
                            m_Player.Towns[m_CurrentTownIntern].setIngameBuildingQueue(l_BuildingQueueParsed);

                        }
                        //Update building queue

                        final();
                    }
                    else
                    {
                        if (l_Settings.AdvDebugMode)
                        {
                            //l_IOHandler.debug("Ingame error while adding building to ingame queue: " + p_Response.Substring(p_Response.IndexOf(":\"", 0) + 2, p_Response.IndexOf("\"}", 0) - (p_Response.IndexOf(":\"", 0)) + 2));
                            l_IOHandler.debug("Ingame error while adding building to ingame queue");
                        }
                        //Note This method is called when there is nothing to build (3)
                        checkLandArmyQueue();
                    }
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("checkBuildingQueueResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void checkLandArmyQueue()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "checklandarmyqueue";
                int l_Amount = 0;

                //Check if it's possible to queue an unit at this moment
                if (m_Player.Towns[m_CurrentTownIntern].UnitQueueEnabled && m_Player.Towns[m_CurrentTownIntern].SizeOfLandUnitQueue < 7)
                {
                    //Check which unit you need to queue
                    int l_UnitIndex = m_Player.Towns[m_CurrentTownIntern].getMostTrainableLandUnitInQueue();
                    if (l_UnitIndex != -1)
                    {
                        //Check how many you can queue
                        l_Amount = m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].QueueBot - (m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].TotalAmount + m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].QueueGame);
                        if (m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].MaxBuild < l_Amount)
                            l_Amount = m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].MaxBuild;
                        //Final check
                        if (l_Amount > 0)
                        {
                            setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Checking land unit queue");
                            //http://###.grepolis.com/game/building_barracks?action=build&town_id=#####&h=###########
                            NameValueCollection l_Content = new NameValueCollection();
                            String l_Url = "http://" + l_Settings.GenServer + "/game/building_barracks?action=build&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                            Uri l_Uri = new Uri(l_Url);
                            l_Content.Add("json", "{\"unit_id\":\"" + m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].Name + "\",\"amount\":" + l_Amount.ToString() + ",\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"unit_id":"archer","amount":#,"town_id":"#####","nlreq_id":##}

                            logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": " + m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].LocalName + "(" + l_Amount.ToString() + ") added to the ingame queue.");

                            m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                            m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                            m_HttpHandler.Headers.Remove("X-Requested-With");
                        }
                        else
                        {
                            //Note This method is called when there is nothing to Train in the barracks (1)
                            checkNavyArmyQueue();
                        }
                    }
                    else
                    {
                        //Note This method is called when there is nothing to Train in the barracks (2)
                        checkNavyArmyQueue();
                    }
                }
                else
                {
                    //Note This method is called when there is nothing to Train in the barracks (3)
                    logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Army queue is diabled for this town (or queue is full).");
                    checkNavyArmyQueue();
                }
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in checkLandArmyQueue(): " + e.Message);
            }
        }

        private void checkLandArmyQueueResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string l_Search = "\"bar\":";
                int l_Index = -1;
                string l_Wood = "0";
                string l_Stone = "0";
                string l_Iron = "0";
                string l_Error = "";

                int l_ValidCode = validateResponse(p_Response, "checkLandArmyQueueResponse");
                if (l_ValidCode == 1)
                {
                    if (!p_Response.Contains("{\"error\":\""))
                    {
                        setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Land unit(s) added to ingame queue");

                        l_Index = p_Response.IndexOf(l_Search, 0);
                        if (l_Index != -1)
                        {
                            //Update resources
                            l_Search = "\"resources\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Search = "\"wood\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Wood = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"stone\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Stone = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"iron\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Iron = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("}", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Set resource info
                            m_Player.Towns[m_CurrentTownIntern].Wood = int.Parse(l_Wood);
                            m_Player.Towns[m_CurrentTownIntern].Stone = int.Parse(l_Stone);
                            m_Player.Towns[m_CurrentTownIntern].Iron = int.Parse(l_Iron);

                            //Zeus
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorZeus = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorZeusProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Poseidon
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorPoseidon = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorPoseidonProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Hera
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHera = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHeraProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Athene
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorAthene = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorAtheneProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Hades
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHades = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHadesProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                            //Population
                            l_Search = "\"population\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.Towns[m_CurrentTownIntern].Population = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length)));

                            //Nlreq_id
                            l_Search = "\"notify_id\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Nlreq_id = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        }
                        final();
                    }
                    else
                    {
                        if (l_Settings.AdvDebugMode)
                        {
                            l_Search = "{\"error\":\"";
                            l_Index = p_Response.IndexOf(l_Search, 0);
                            l_Error = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\"", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Error = fixSpecialCharacters(l_Error);

                            l_IOHandler.debug("Ingame error in checkLandArmyQueueResponse(): " + l_Error);
                            
                        }
                        //Note This method is called when there is nothing to Train in the barracks (4)
                        checkNavyArmyQueue();
                    }
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("checkLandArmyQueueResponse", e.Message + "\n" + p_Response);
                
                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void checkNavyArmyQueue()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_State = "checknavyarmyqueue";
                int l_Amount = 0;

                //Check if it's possible to queue an unit at this moment
                if (m_Player.Towns[m_CurrentTownIntern].UnitQueueEnabled && m_Player.Towns[m_CurrentTownIntern].SizeOfNavyUnitQueue < 7)
                {
                    //Check which unit you need to queue
                    int l_UnitIndex = m_Player.Towns[m_CurrentTownIntern].getMostTrainableNavyUnitInQueue();
                    if (l_UnitIndex != -1)
                    {
                        //Check how many you can queue
                        l_Amount = m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].QueueBot - (m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].TotalAmount + m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].QueueGame);
                        if (m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].MaxBuild < l_Amount)
                            l_Amount = m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].MaxBuild;
                        //Final check
                        if (l_Amount > 0)
                        {
                            setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Checking navy unit queue");
                            //http://###.grepolis.com/game/building_docks?action=build&town_id=#####&h=###########
                            NameValueCollection l_Content = new NameValueCollection();
                            String l_Url = "http://" + l_Settings.GenServer + "/game/building_docks?action=build&town_id=" + m_Player.Towns[m_CurrentTownIntern].TownID + "&h=" + m_H;
                            Uri l_Uri = new Uri(l_Url);
                            l_Content.Add("json", "{\"unit_id\":\"" + m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].Name + "\",\"amount\":" + l_Amount.ToString() + ",\"town_id\":\"" + m_Player.Towns[m_CurrentTownIntern].TownID + "\",\"nlreq_id\":" + m_Nlreq_id + "}");//json={"unit_id":"####","amount":#,"town_id":"###","nlreq_id":###}

                            logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": " + m_Player.Towns[m_CurrentTownIntern].ArmyUnits[l_UnitIndex].LocalName + "(" + l_Amount.ToString() + ") added to the ingame queue.");

                            m_HttpHandler.Headers.Add("X-Requested-With", "XMLHttpRequest");
                            m_HttpHandler.UploadValuesAsync(l_Uri, l_Content);
                            m_HttpHandler.Headers.Remove("X-Requested-With");
                        }
                        else
                        {
                            //Note This method is called when there is nothing to Train in the docks (1)
                            final();
                        }
                    }
                    else
                    {
                        //Note This method is called when there is nothing to Train in the docks (2)
                        final();
                    }
                }
                else
                {
                    //Note This method is called when there is nothing to Train in the docks (3)
                    logEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Navy queue is diabled for this town (or queue is full).");
                    final();
                }
            }
            catch (Exception e)
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in checkNavyArmyQueue(): " + e.Message);
            }
        }

        private void checkNavyArmyQueueResponse(string p_Response)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string l_Search = "\"bar\":";
                int l_Index = -1;
                string l_Wood = "0";
                string l_Stone = "0";
                string l_Iron = "0";
                string l_Error = "";

                int l_ValidCode = validateResponse(p_Response, "checkNavyArmyQueueResponse");
                if (l_ValidCode == 1)
                {
                    if (!p_Response.Contains("{\"error\":\""))
                    {
                        setStatusBarEvent(m_Player.Towns[m_CurrentTownIntern].Name + ": Navy unit(s) added to ingame queue");

                        l_Index = p_Response.IndexOf(l_Search, 0);
                        if (l_Index != -1)
                        {
                            //Update resources
                            l_Search = "\"resources\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Search = "\"wood\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Wood = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"stone\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Stone = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"iron\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            l_Iron = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("}", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Set resource info
                            m_Player.Towns[m_CurrentTownIntern].Wood = int.Parse(l_Wood);
                            m_Player.Towns[m_CurrentTownIntern].Stone = int.Parse(l_Stone);
                            m_Player.Towns[m_CurrentTownIntern].Iron = int.Parse(l_Iron);

                            //Zeus
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorZeus = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorZeusProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Poseidon
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorPoseidon = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorPoseidonProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Hera
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHera = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHeraProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Athene
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorAthene = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorAtheneProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            //Hades
                            l_Search = "\"current\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHades = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length)));
                            l_Search = "\"production\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.FavorHadesProduction = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(',', l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                            //Population
                            l_Search = "\"population\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Player.Towns[m_CurrentTownIntern].Population = int.Parse(p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length)));

                            //Nlreq_id
                            l_Search = "\"notify_id\":";
                            l_Index = p_Response.IndexOf(l_Search, l_Index);
                            m_Nlreq_id = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        }
                        final();
                    }
                    else
                    {
                        if (l_Settings.AdvDebugMode)
                        {
                            l_Search = "{\"error\":\"";
                            l_Index = p_Response.IndexOf(l_Search, 0);
                            l_Error = p_Response.Substring(l_Index + l_Search.Length, p_Response.IndexOf("\"", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Error = fixSpecialCharacters(l_Error);

                            l_IOHandler.debug("Ingame error in checkNavyArmyQueueResponse(): " + l_Error);
                        }
                        //Note This method is called when there is nothing to Train in the docks (4)
                        final();
                    }
                }
                else
                {
                    processValidatedResponse(l_ValidCode);
                }
                m_RetryCountServerError = 0;
            }
            catch (Exception e)
            {
                setStatusBarEvent("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");
                l_IOHandler.debug("(#" + m_RetryCountServerError + ")Critical error occurred. Server response saved in Response dir.");

                if (l_Settings.AdvDebugMode)
                    l_IOHandler.saveServerResponse("checkNavyArmyQueueResponse", e.Message + "\n" + p_Response);

                if (m_RetryCountServerError < 1)
                {
                    m_RetryCountServerError++;
                    retryManager();
                }
                else
                {
                    m_RetryCountServerError = 0;
                    startReconnect();
                }
            }
        }

        private void final()
        {
            IOHandler l_IOHandler = IOHandler.Instance;

            if(m_IsBotRunning)
                randomizeStateManagerMode();

            m_CurrentTownIntern++;
            if (m_CurrentTownIntern < m_Player.Towns.Count)
                locateFarmers();
            else
            {
                setGuiToTimeoutProcessedStateEvent();
                l_IOHandler.saveTownsSettings(m_Player);
            }
        }

//-->Event handlers

        private void m_HttpHandler_UploadValuesCompleted(object sender, System.Net.UploadValuesCompletedEventArgs e)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            if (e.Error == null)
            {
                m_RetryCount = 0;
                string l_Response = Encoding.Default.GetString(e.Result);
                responseManager(l_Response);
            }
            else
            {
                if (m_RetryCount >= 1)
                {
                    startReconnect();
                    if (l_Settings.AdvDebugMode)
                        l_IOHandler.debug("Exception in m_HttpHandler_UploadValuesCompleted(): " + "(" + m_State + "#" + m_RetryCount.ToString() + ") " + e.Error.Message);
                }
                else
                {
                    if (l_Settings.AdvDebugMode)
                        l_IOHandler.debug("Exception in m_HttpHandler_UploadValuesCompleted(): " + "(" + m_State + "#" + m_RetryCount.ToString() + ") " + e.Error.Message);
                    retryManager();
                }
                
            }
        }

        private void retryManager()
        {
            m_RetryCount++;
            switch (m_State)
            {
                case "locatefarmers":
                    locateFarmers();
                    break;
                case "generalinfo":
                    generalInfo();
                    break;
                case "countarmy":
                    countArmy();
                    break;
                case "updatebuildings":
                    updateBuildings();
                    break;
                case "updatemovementinfo":
                    updateMovementInfo();
                    break;
                case "updatetradeinfo":
                    updateTradeInfo();
                    break;
                case "updateculturalinfo":
                    updateCulturalInfo();
                    break;
                case "updateresearch":
                    updateResearch();
                    break;
                case "attackfarmers":
                    attackFarmers();
                    break;
                case "checkbuildingqueue":
                    checkBuildingQueue();
                    break;
                case "checklandarmyqueue":
                    checkLandArmyQueue();
                    break;
                case "checknavyarmyqueue":
                    checkNavyArmyQueue();
                    break;
            }
        }

        private void stateManagerDelay()
        {
            Random l_Random = new Random();
            int l_Delay = l_Random.Next(1000,3000);

            if (m_State.Equals("attackfarmersContinue"))
                m_RequestTimer.Interval = l_Delay;
            else
                m_RequestTimer.Interval = 100;

            CustomArgs l_CustomArgs = new CustomArgs("");
            serverRequestDelayRequested(this, l_CustomArgs);
        }

        private void stateManager()
        {
            //Should be enough randomizing for the time being.
            switch (m_StateManagerMode)
            {
                case 0:
                    stateManager1();
                    break;
                case 1:
                    stateManager2();
                    break;
                case 2:
                    stateManager3();
                    break;
            }
        }

        private void stateManager1()
        {
            switch (m_State)
            {
                case "locatefarmers":
                    generalInfo();
                    break;
                case "generalinfo":
                    countArmy();
                    break;
                case "countarmy":
                    updateBuildings();
                    break;
                case "updatebuildings":
                    updateMovementInfo();
                    break;
                case "updatemovementinfo":
                    updateTradeInfo();
                    break;
                case "updatetradeinfo":
                    updateCulturalInfo();
                    break;
                case "updateculturalinfo":
                    updateResearch();
                    break;
                case "attackfarmersContinue":
                    attackFarmers();
                    break;
                case "updateresearch":
                    attackFarmers();//This should be the last call when randomizing!!!
                    break;
            }
        }

        private void stateManager2()
        {
            switch (m_State)
            {
                case "locatefarmers":
                    generalInfo();
                    break;
                case "generalinfo":
                    updateMovementInfo();
                    break;
                case "updatemovementinfo":
                    updateTradeInfo();
                    break;
                case "updatetradeinfo":
                    updateCulturalInfo();
                    break;
                case "updateculturalinfo":
                    updateResearch();
                    break;
                case "updateresearch":
                    countArmy();
                    break;
                case "countarmy":
                    updateBuildings();
                    break;
                case "attackfarmersContinue":
                    attackFarmers();
                    break;
                case "updatebuildings":
                    attackFarmers();//This should be the last call when randomizing!!!
                    break;
            }
        }

        private void stateManager3()
        {
            switch (m_State)
            {
                case "locatefarmers":
                    generalInfo();
                    break;
                case "generalinfo":
                    updateBuildings();
                    break;
                case "updatebuildings":
                    countArmy();
                    break;
                case "countarmy":
                    updateCulturalInfo();
                    break;
                case "updateculturalinfo":
                    updateResearch();
                    break;
                case "updateresearch":
                    updateTradeInfo();
                    break;
                case "updatetradeinfo":
                    updateMovementInfo();
                    break;
                case "attackfarmersContinue":
                    attackFarmers();
                    break;
                case "updatemovementinfo":
                    attackFarmers();//This should be the last call when randomizing!!!
                    break;
            }
        }

        private void responseManager(string p_Response)
        {
            string l_Response = p_Response;
            switch (m_State)
            {
                /*case "login":
                    loginResponse(l_Response);
                    break;*/
                case "locatefarmers":
                    locateFarmersResponse(l_Response);
                    break;
                case "generalinfo":
                    generalInfoResponse(l_Response);
                    break;
                case "countarmy":
                    countArmyResponse(l_Response);
                    break;
                case "updatebuildings":
                    updateBuildingsResponse(l_Response);
                    break;
                case "updatemovementinfo":
                    updateMovementInfoResponse(l_Response);
                    break;
                case "updatetradeinfo":
                    updateTradeInfoResponse(l_Response);
                    break;
                case "updateculturalinfo":
                    updateCulturalInfoResponse(l_Response);
                    break;
                case "updateresearch":
                    updateResearchResponse(l_Response);
                    break;
                case "attackfarmers":
                    attackFarmersResponse(l_Response);
                    break;
                case "checkbuildingqueue":
                    checkBuildingQueueResponse(l_Response);
                    break;
                case "checklandarmyqueue":
                    checkLandArmyQueueResponse(l_Response);
                    break;
                case "checknavyarmyqueue":
                    checkNavyArmyQueueResponse(l_Response);
                    break;
            }
        }

        private void m_HttpHandler_DownloadStringCompleted(object sender, System.Net.DownloadStringCompletedEventArgs e)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            if (e.Error == null)
            {
                string l_Response = e.Result;
                /*
                switch (m_State)
                {
                    //At the moment there are no GET requests
                }
                */ 
            }
            else
            {
                startReconnect();
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in m_HttpHandler_DownloadStringCompleted(): " + e.Error.Message);
            }
        }

        void m_HttpHandlerCheckVersion_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            if (e.Error == null)
            {
                string l_Response = e.Result;
                versionInfoEvent(l_Response);
            }
            else
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in m_HttpHandlerCheckVersion_DownloadStringCompleted(): " + e.Error.Message);
            }

        }

        private void RefreshTimer_InternalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (m_IsBotRunning)
            {
                updateRefreshTimerEvent();
            }
            else
            {
                stop();
            }
        }

        private void QueueTimer_InternalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (m_IsBotRunning)
            {
                updateQueueTimerEvent();
            }
            else
            {
                stop();
            }
        }

        private void ReconnectTimer_InternalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            updateReconnectTimerEvent();
        }

        private void ConnectedTimer_InternalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            updateConnectedTimerEvent();
        }

        private void m_RequestTimer_Tick(object sender, EventArgs e)
        {
            m_RequestTimer.Stop();
            stateManager();
        }
    }
}
