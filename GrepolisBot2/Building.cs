﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrepolisBot2
{
    class Building
    {
        //private string m_Name = "";
        private string m_LocalName = "";
        private string m_DevName = "";
        private int m_Level = 0;
        //private int m_LevelOffset = 0;
        private int m_MaxLevel = 0;
        private int m_TargetLevel = 0;
        private bool m_Upgradable = false;

//-->Constructors

        public Building()
        {

        }

        public Building(string p_DevName, int p_MaxLevel)
        {
            m_DevName = p_DevName;
            m_MaxLevel = p_MaxLevel;
        }

//-->Attributes

        /*public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }*/

        public string LocalName
        {
            get { return m_LocalName; }
            set { m_LocalName = value; }
        }

        public string DevName
        {
            get { return m_DevName; }
            set { m_DevName = value; }
        }

        public int Level
        {
            get { return m_Level; }
            set { m_Level = value; }
        }

        /*public int LevelOffset
        {
            get { return m_LevelOffset; }
            set { m_LevelOffset = value; }
        }*/

        public int MaxLevel
        {
            get { return m_MaxLevel; }
            set { m_MaxLevel = value; }
        }

        public int TargetLevel
        {
            get { return m_TargetLevel; }
            set { m_TargetLevel = value; }
        }

        public bool Upgradable
        {
            get { return m_Upgradable; }
            set { m_Upgradable = value; }
        }

//-->Methods

        /*
         * Returns Total Level, Level+LevelOffset.
         */
        /*public int getTotalLevel()
        {
            return m_Level + m_LevelOffset;
        }*/

        public override string ToString()
        {
            return m_LocalName + " " + m_Level.ToString();
        }
    }
}
