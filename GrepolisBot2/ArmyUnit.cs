﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrepolisBot2
{
    class ArmyUnit
    {
        private string m_Name = "";
        private string m_LocalName = "";
        private int m_CurrentAmount = 0;//Currently in town
        private int m_TotalAmount = 0;//Total units (Includes those on a mission)
        private int m_MaxBuild = 0;//How many you can build with the current resources
        private int m_QueueBot = 0;//Target training amount
        private int m_QueueGame = 0;//Current amount that is being trained ingame
        //private string m_NeededAttackAmount = "0";
        //private string m_NeededSupportAmount = "0";
        //private int m_ReadyForTraining = 0;

//-->Constructors

        public ArmyUnit()
        {

        }

        public ArmyUnit(string p_Name)
        {
            m_Name = p_Name;
        }

//-->Attributes

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string LocalName
        {
            get { return m_LocalName; }
            set { m_LocalName = value; }
        }

        public int CurrentAmount
        {
            get { return m_CurrentAmount; }
            set { m_CurrentAmount = value; }
        }

        public int TotalAmount
        {
            get { return m_TotalAmount; }
            set { m_TotalAmount = value; }
        }

        public int QueueBot
        {
            get { return m_QueueBot; }
            set { m_QueueBot = value; }
        }

        public int QueueGame
        {
            get { return m_QueueGame; }
            set { m_QueueGame = value; }
        }

        public int MaxBuild
        {
            get { return m_MaxBuild; }
            set { m_MaxBuild = value; }
        }

        /*public string NeededAttackAmount
        {
            get { return m_NeededAttackAmount; }
            set { m_NeededAttackAmount = value; }
        }*/

        /*public string NeededSupportAmount
        {
            get { return m_NeededSupportAmount; }
            set { m_NeededSupportAmount = value; }
        }*/

        /*public int ReadyForTraining
        {
            get { return m_ReadyForTraining; }
            set { m_ReadyForTraining = value; }
        }*/
    }
}
