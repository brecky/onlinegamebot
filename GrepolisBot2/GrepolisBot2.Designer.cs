﻿namespace GrepolisBot2
{
    partial class GrepolisBot2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GrepolisBot2));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPagePlayer = new System.Windows.Forms.TabPage();
            this.linkLabelLatestVersion = new System.Windows.Forms.LinkLabel();
            this.labelReload = new System.Windows.Forms.Label();
            this.groupBoxNotes = new System.Windows.Forms.GroupBox();
            this.textBoxNotes = new System.Windows.Forms.TextBox();
            this.groupBoxTimers = new System.Windows.Forms.GroupBox();
            this.labelConnectedTimer = new System.Windows.Forms.Label();
            this.labelConnected = new System.Windows.Forms.Label();
            this.labelReconnectTimeLeft = new System.Windows.Forms.Label();
            this.labelReconnect = new System.Windows.Forms.Label();
            this.labelQueueTimeLeft = new System.Windows.Forms.Label();
            this.labelNextUpdateTimeLeft = new System.Windows.Forms.Label();
            this.labelQueue = new System.Windows.Forms.Label();
            this.labelNextUpdate = new System.Windows.Forms.Label();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.buttonRestoreGUI = new System.Windows.Forms.Button();
            this.buttonTownStartStop = new System.Windows.Forms.Button();
            this.buttonTownLogin = new System.Windows.Forms.Button();
            this.tabPageBuildings = new System.Windows.Forms.TabPage();
            this.flowLayoutPanelBuildings = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageUnits = new System.Windows.Forms.TabPage();
            this.flowLayoutPanelUnits = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageCulture = new System.Windows.Forms.TabPage();
            this.groupBoxCulture = new System.Windows.Forms.GroupBox();
            this.labelCulturalCities = new System.Windows.Forms.Label();
            this.labelCulturalLevel = new System.Windows.Forms.Label();
            this.progressBarCP = new System.Windows.Forms.ProgressBar();
            this.tabPageFarmers = new System.Windows.Forms.TabPage();
            this.flowLayoutPanelFarmers = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageNotifications = new System.Windows.Forms.TabPage();
            this.groupBoxNotifications = new System.Windows.Forms.GroupBox();
            this.textBoxNotifications = new System.Windows.Forms.TextBox();
            this.tabPageLog = new System.Windows.Forms.TabPage();
            this.groupBoxLog = new System.Windows.Forms.GroupBox();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.groupBoxGUI = new System.Windows.Forms.GroupBox();
            this.labelGUIBuildingLevelTargetColor = new System.Windows.Forms.Label();
            this.labelGUIBuildingLevelTarget = new System.Windows.Forms.Label();
            this.groupBoxProxy = new System.Windows.Forms.GroupBox();
            this.labelProxyEnabled = new System.Windows.Forms.Label();
            this.checkBoxProxyEnabled = new System.Windows.Forms.CheckBox();
            this.textBoxProxyPassword = new System.Windows.Forms.TextBox();
            this.textBoxProxyUserName = new System.Windows.Forms.TextBox();
            this.labelProxyPassword = new System.Windows.Forms.Label();
            this.labelProxyUserName = new System.Windows.Forms.Label();
            this.textBoxProxyServer = new System.Windows.Forms.TextBox();
            this.labelProxyServer = new System.Windows.Forms.Label();
            this.buttonSettingsCancel = new System.Windows.Forms.Button();
            this.buttonSettingsOK = new System.Windows.Forms.Button();
            this.groupBoxBuilding = new System.Windows.Forms.GroupBox();
            this.labelTimer = new System.Windows.Forms.Label();
            this.numericUpDownTimerQueue = new System.Windows.Forms.NumericUpDown();
            this.groupBoxAdvanced = new System.Windows.Forms.GroupBox();
            this.numericUpDownTimerRefresh = new System.Windows.Forms.NumericUpDown();
            this.labelRefreshTimer = new System.Windows.Forms.Label();
            this.numericUpDownTimerReconnect = new System.Windows.Forms.NumericUpDown();
            this.labelReconnectTimer = new System.Windows.Forms.Label();
            this.labelDebugMode = new System.Windows.Forms.Label();
            this.checkBoxDebugMode = new System.Windows.Forms.CheckBox();
            this.groupBoxMail = new System.Windows.Forms.GroupBox();
            this.numericUpDownMailPort = new System.Windows.Forms.NumericUpDown();
            this.textBoxMailServer = new System.Windows.Forms.TextBox();
            this.textBoxMailPassword = new System.Windows.Forms.TextBox();
            this.textBoxMailUsername = new System.Windows.Forms.TextBox();
            this.labelMailServer = new System.Windows.Forms.Label();
            this.labelMailPort = new System.Windows.Forms.Label();
            this.labelMailPassword = new System.Windows.Forms.Label();
            this.labelMailUsername = new System.Windows.Forms.Label();
            this.checkBoxEmail = new System.Windows.Forms.CheckBox();
            this.labelNotify = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.groupBoxGeneral = new System.Windows.Forms.GroupBox();
            this.checkBoxAutoStart = new System.Windows.Forms.CheckBox();
            this.labelAutoStart = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelMainServer = new System.Windows.Forms.Label();
            this.textBoxMainServer = new System.Windows.Forms.TextBox();
            this.labelServer = new System.Windows.Forms.Label();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.labelTrayIcon = new System.Windows.Forms.Label();
            this.checkBoxTrayIcon = new System.Windows.Forms.CheckBox();
            this.tabPageBrowser = new System.Windows.Forms.TabPage();
            this.webBrowserGrepo = new System.Windows.Forms.WebBrowser();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.labelBuildingTooltips = new System.Windows.Forms.Label();
            this.checkBoxGUIBuildingTooltips = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPagePlayer.SuspendLayout();
            this.groupBoxNotes.SuspendLayout();
            this.groupBoxTimers.SuspendLayout();
            this.groupBoxControls.SuspendLayout();
            this.tabPageBuildings.SuspendLayout();
            this.tabPageUnits.SuspendLayout();
            this.tabPageCulture.SuspendLayout();
            this.groupBoxCulture.SuspendLayout();
            this.tabPageFarmers.SuspendLayout();
            this.tabPageNotifications.SuspendLayout();
            this.groupBoxNotifications.SuspendLayout();
            this.tabPageLog.SuspendLayout();
            this.groupBoxLog.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.groupBoxGUI.SuspendLayout();
            this.groupBoxProxy.SuspendLayout();
            this.groupBoxBuilding.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerQueue)).BeginInit();
            this.groupBoxAdvanced.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerReconnect)).BeginInit();
            this.groupBoxMail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMailPort)).BeginInit();
            this.groupBoxGeneral.SuspendLayout();
            this.tabPageBrowser.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPagePlayer);
            this.tabControl1.Controls.Add(this.tabPageBuildings);
            this.tabControl1.Controls.Add(this.tabPageUnits);
            this.tabControl1.Controls.Add(this.tabPageCulture);
            this.tabControl1.Controls.Add(this.tabPageFarmers);
            this.tabControl1.Controls.Add(this.tabPageNotifications);
            this.tabControl1.Controls.Add(this.tabPageLog);
            this.tabControl1.Controls.Add(this.tabPageSettings);
            this.tabControl1.Controls.Add(this.tabPageBrowser);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1063, 554);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPagePlayer
            // 
            this.tabPagePlayer.Controls.Add(this.linkLabelLatestVersion);
            this.tabPagePlayer.Controls.Add(this.labelReload);
            this.tabPagePlayer.Controls.Add(this.groupBoxNotes);
            this.tabPagePlayer.Controls.Add(this.groupBoxTimers);
            this.tabPagePlayer.Controls.Add(this.groupBoxControls);
            this.tabPagePlayer.Location = new System.Drawing.Point(4, 22);
            this.tabPagePlayer.Name = "tabPagePlayer";
            this.tabPagePlayer.Size = new System.Drawing.Size(1055, 528);
            this.tabPagePlayer.TabIndex = 3;
            this.tabPagePlayer.Text = "Player";
            this.tabPagePlayer.UseVisualStyleBackColor = true;
            // 
            // linkLabelLatestVersion
            // 
            this.linkLabelLatestVersion.AutoSize = true;
            this.linkLabelLatestVersion.Location = new System.Drawing.Point(20, 20);
            this.linkLabelLatestVersion.Name = "linkLabelLatestVersion";
            this.linkLabelLatestVersion.Size = new System.Drawing.Size(123, 13);
            this.linkLabelLatestVersion.TabIndex = 6;
            this.linkLabelLatestVersion.TabStop = true;
            this.linkLabelLatestVersion.Text = "Latest version: unknown";
            // 
            // labelReload
            // 
            this.labelReload.AutoSize = true;
            this.labelReload.Image = global::GrepolisBot2.Properties.Resources.reload_icon;
            this.labelReload.Location = new System.Drawing.Point(5, 18);
            this.labelReload.Margin = new System.Windows.Forms.Padding(0);
            this.labelReload.MaximumSize = new System.Drawing.Size(16, 16);
            this.labelReload.MinimumSize = new System.Drawing.Size(16, 16);
            this.labelReload.Name = "labelReload";
            this.labelReload.Size = new System.Drawing.Size(16, 16);
            this.labelReload.TabIndex = 5;
            this.labelReload.DoubleClick += new System.EventHandler(this.labelReload_DoubleClick);
            // 
            // groupBoxNotes
            // 
            this.groupBoxNotes.Controls.Add(this.textBoxNotes);
            this.groupBoxNotes.Location = new System.Drawing.Point(215, 3);
            this.groupBoxNotes.Margin = new System.Windows.Forms.Padding(0);
            this.groupBoxNotes.Name = "groupBoxNotes";
            this.groupBoxNotes.Size = new System.Drawing.Size(836, 525);
            this.groupBoxNotes.TabIndex = 3;
            this.groupBoxNotes.TabStop = false;
            this.groupBoxNotes.Text = "Notes";
            // 
            // textBoxNotes
            // 
            this.textBoxNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxNotes.Location = new System.Drawing.Point(3, 16);
            this.textBoxNotes.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxNotes.Multiline = true;
            this.textBoxNotes.Name = "textBoxNotes";
            this.textBoxNotes.Size = new System.Drawing.Size(830, 506);
            this.textBoxNotes.TabIndex = 2;
            this.textBoxNotes.MouseLeave += new System.EventHandler(this.textBoxNotes_MouseLeave);
            // 
            // groupBoxTimers
            // 
            this.groupBoxTimers.Controls.Add(this.labelConnectedTimer);
            this.groupBoxTimers.Controls.Add(this.labelConnected);
            this.groupBoxTimers.Controls.Add(this.labelReconnectTimeLeft);
            this.groupBoxTimers.Controls.Add(this.labelReconnect);
            this.groupBoxTimers.Controls.Add(this.labelQueueTimeLeft);
            this.groupBoxTimers.Controls.Add(this.labelNextUpdateTimeLeft);
            this.groupBoxTimers.Controls.Add(this.labelQueue);
            this.groupBoxTimers.Controls.Add(this.labelNextUpdate);
            this.groupBoxTimers.Location = new System.Drawing.Point(8, 289);
            this.groupBoxTimers.Name = "groupBoxTimers";
            this.groupBoxTimers.Size = new System.Drawing.Size(200, 123);
            this.groupBoxTimers.TabIndex = 1;
            this.groupBoxTimers.TabStop = false;
            this.groupBoxTimers.Text = "Timers";
            // 
            // labelConnectedTimer
            // 
            this.labelConnectedTimer.AutoSize = true;
            this.labelConnectedTimer.Location = new System.Drawing.Point(100, 100);
            this.labelConnectedTimer.Name = "labelConnectedTimer";
            this.labelConnectedTimer.Size = new System.Drawing.Size(49, 13);
            this.labelConnectedTimer.TabIndex = 7;
            this.labelConnectedTimer.Text = "00:00:00";
            // 
            // labelConnected
            // 
            this.labelConnected.AutoSize = true;
            this.labelConnected.Location = new System.Drawing.Point(6, 100);
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.Size = new System.Drawing.Size(62, 13);
            this.labelConnected.TabIndex = 6;
            this.labelConnected.Text = "Connected:";
            // 
            // labelReconnectTimeLeft
            // 
            this.labelReconnectTimeLeft.AutoSize = true;
            this.labelReconnectTimeLeft.Location = new System.Drawing.Point(100, 75);
            this.labelReconnectTimeLeft.Name = "labelReconnectTimeLeft";
            this.labelReconnectTimeLeft.Size = new System.Drawing.Size(49, 13);
            this.labelReconnectTimeLeft.TabIndex = 5;
            this.labelReconnectTimeLeft.Text = "00:00:00";
            // 
            // labelReconnect
            // 
            this.labelReconnect.AutoSize = true;
            this.labelReconnect.Location = new System.Drawing.Point(6, 75);
            this.labelReconnect.Name = "labelReconnect";
            this.labelReconnect.Size = new System.Drawing.Size(63, 13);
            this.labelReconnect.TabIndex = 4;
            this.labelReconnect.Text = "Reconnect:";
            // 
            // labelQueueTimeLeft
            // 
            this.labelQueueTimeLeft.AutoSize = true;
            this.labelQueueTimeLeft.Location = new System.Drawing.Point(100, 50);
            this.labelQueueTimeLeft.Name = "labelQueueTimeLeft";
            this.labelQueueTimeLeft.Size = new System.Drawing.Size(49, 13);
            this.labelQueueTimeLeft.TabIndex = 3;
            this.labelQueueTimeLeft.Text = "00:00:00";
            // 
            // labelNextUpdateTimeLeft
            // 
            this.labelNextUpdateTimeLeft.AutoSize = true;
            this.labelNextUpdateTimeLeft.Location = new System.Drawing.Point(100, 25);
            this.labelNextUpdateTimeLeft.Name = "labelNextUpdateTimeLeft";
            this.labelNextUpdateTimeLeft.Size = new System.Drawing.Size(49, 13);
            this.labelNextUpdateTimeLeft.TabIndex = 2;
            this.labelNextUpdateTimeLeft.Text = "00:00:00";
            // 
            // labelQueue
            // 
            this.labelQueue.AutoSize = true;
            this.labelQueue.Location = new System.Drawing.Point(6, 50);
            this.labelQueue.Name = "labelQueue";
            this.labelQueue.Size = new System.Drawing.Size(42, 13);
            this.labelQueue.TabIndex = 1;
            this.labelQueue.Text = "Queue:";
            // 
            // labelNextUpdate
            // 
            this.labelNextUpdate.AutoSize = true;
            this.labelNextUpdate.Location = new System.Drawing.Point(6, 25);
            this.labelNextUpdate.Name = "labelNextUpdate";
            this.labelNextUpdate.Size = new System.Drawing.Size(70, 13);
            this.labelNextUpdate.TabIndex = 0;
            this.labelNextUpdate.Text = "Next Update:";
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.buttonRestoreGUI);
            this.groupBoxControls.Controls.Add(this.buttonTownStartStop);
            this.groupBoxControls.Controls.Add(this.buttonTownLogin);
            this.groupBoxControls.Location = new System.Drawing.Point(8, 418);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(200, 110);
            this.groupBoxControls.TabIndex = 0;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "Controlls";
            // 
            // buttonRestoreGUI
            // 
            this.buttonRestoreGUI.Location = new System.Drawing.Point(6, 55);
            this.buttonRestoreGUI.Name = "buttonRestoreGUI";
            this.buttonRestoreGUI.Size = new System.Drawing.Size(75, 23);
            this.buttonRestoreGUI.TabIndex = 0;
            this.buttonRestoreGUI.Text = "Restore GUI";
            this.buttonRestoreGUI.UseVisualStyleBackColor = true;
            this.buttonRestoreGUI.Click += new System.EventHandler(this.buttonRestoreGUI_Click);
            // 
            // buttonTownStartStop
            // 
            this.buttonTownStartStop.Enabled = false;
            this.buttonTownStartStop.Location = new System.Drawing.Point(90, 25);
            this.buttonTownStartStop.Name = "buttonTownStartStop";
            this.buttonTownStartStop.Size = new System.Drawing.Size(75, 23);
            this.buttonTownStartStop.TabIndex = 1;
            this.buttonTownStartStop.Text = "Start";
            this.buttonTownStartStop.UseVisualStyleBackColor = true;
            this.buttonTownStartStop.Click += new System.EventHandler(this.buttonTownStartStop_Click);
            // 
            // buttonTownLogin
            // 
            this.buttonTownLogin.Location = new System.Drawing.Point(6, 25);
            this.buttonTownLogin.Name = "buttonTownLogin";
            this.buttonTownLogin.Size = new System.Drawing.Size(75, 23);
            this.buttonTownLogin.TabIndex = 0;
            this.buttonTownLogin.Text = "Login";
            this.buttonTownLogin.UseVisualStyleBackColor = true;
            this.buttonTownLogin.Click += new System.EventHandler(this.buttonTownLogin_Click);
            // 
            // tabPageBuildings
            // 
            this.tabPageBuildings.Controls.Add(this.flowLayoutPanelBuildings);
            this.tabPageBuildings.Location = new System.Drawing.Point(4, 22);
            this.tabPageBuildings.Name = "tabPageBuildings";
            this.tabPageBuildings.Size = new System.Drawing.Size(1055, 528);
            this.tabPageBuildings.TabIndex = 4;
            this.tabPageBuildings.Text = "Buildings";
            this.tabPageBuildings.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanelBuildings
            // 
            this.flowLayoutPanelBuildings.AutoScroll = true;
            this.flowLayoutPanelBuildings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelBuildings.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelBuildings.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelBuildings.Name = "flowLayoutPanelBuildings";
            this.flowLayoutPanelBuildings.Size = new System.Drawing.Size(1055, 528);
            this.flowLayoutPanelBuildings.TabIndex = 0;
            this.flowLayoutPanelBuildings.WrapContents = false;
            // 
            // tabPageUnits
            // 
            this.tabPageUnits.Controls.Add(this.flowLayoutPanelUnits);
            this.tabPageUnits.Location = new System.Drawing.Point(4, 22);
            this.tabPageUnits.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageUnits.Name = "tabPageUnits";
            this.tabPageUnits.Size = new System.Drawing.Size(1055, 528);
            this.tabPageUnits.TabIndex = 5;
            this.tabPageUnits.Text = "Units";
            this.tabPageUnits.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanelUnits
            // 
            this.flowLayoutPanelUnits.AutoScroll = true;
            this.flowLayoutPanelUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelUnits.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelUnits.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelUnits.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanelUnits.Name = "flowLayoutPanelUnits";
            this.flowLayoutPanelUnits.Size = new System.Drawing.Size(1055, 528);
            this.flowLayoutPanelUnits.TabIndex = 0;
            this.flowLayoutPanelUnits.WrapContents = false;
            // 
            // tabPageCulture
            // 
            this.tabPageCulture.Controls.Add(this.groupBoxCulture);
            this.tabPageCulture.Location = new System.Drawing.Point(4, 22);
            this.tabPageCulture.Name = "tabPageCulture";
            this.tabPageCulture.Size = new System.Drawing.Size(1055, 528);
            this.tabPageCulture.TabIndex = 6;
            this.tabPageCulture.Text = "Culture";
            this.tabPageCulture.UseVisualStyleBackColor = true;
            // 
            // groupBoxCulture
            // 
            this.groupBoxCulture.Controls.Add(this.labelCulturalCities);
            this.groupBoxCulture.Controls.Add(this.labelCulturalLevel);
            this.groupBoxCulture.Controls.Add(this.progressBarCP);
            this.groupBoxCulture.Location = new System.Drawing.Point(8, 3);
            this.groupBoxCulture.Name = "groupBoxCulture";
            this.groupBoxCulture.Size = new System.Drawing.Size(1035, 73);
            this.groupBoxCulture.TabIndex = 1;
            this.groupBoxCulture.TabStop = false;
            this.groupBoxCulture.Text = "Cultural Points";
            // 
            // labelCulturalCities
            // 
            this.labelCulturalCities.AutoSize = true;
            this.labelCulturalCities.Location = new System.Drawing.Point(950, 16);
            this.labelCulturalCities.Name = "labelCulturalCities";
            this.labelCulturalCities.Size = new System.Drawing.Size(55, 13);
            this.labelCulturalCities.TabIndex = 2;
            this.labelCulturalCities.Text = "Cities: 1/2";
            // 
            // labelCulturalLevel
            // 
            this.labelCulturalLevel.AutoSize = true;
            this.labelCulturalLevel.Location = new System.Drawing.Point(7, 20);
            this.labelCulturalLevel.Name = "labelCulturalLevel";
            this.labelCulturalLevel.Size = new System.Drawing.Size(79, 13);
            this.labelCulturalLevel.TabIndex = 1;
            this.labelCulturalLevel.Text = "Cultural level: 0";
            // 
            // progressBarCP
            // 
            this.progressBarCP.Location = new System.Drawing.Point(6, 39);
            this.progressBarCP.Name = "progressBarCP";
            this.progressBarCP.Size = new System.Drawing.Size(1023, 23);
            this.progressBarCP.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarCP.TabIndex = 0;
            // 
            // tabPageFarmers
            // 
            this.tabPageFarmers.Controls.Add(this.flowLayoutPanelFarmers);
            this.tabPageFarmers.Location = new System.Drawing.Point(4, 22);
            this.tabPageFarmers.Name = "tabPageFarmers";
            this.tabPageFarmers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFarmers.Size = new System.Drawing.Size(1055, 528);
            this.tabPageFarmers.TabIndex = 7;
            this.tabPageFarmers.Text = "Farmers";
            this.tabPageFarmers.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanelFarmers
            // 
            this.flowLayoutPanelFarmers.AutoScroll = true;
            this.flowLayoutPanelFarmers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelFarmers.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelFarmers.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanelFarmers.Name = "flowLayoutPanelFarmers";
            this.flowLayoutPanelFarmers.Size = new System.Drawing.Size(1049, 522);
            this.flowLayoutPanelFarmers.TabIndex = 1;
            this.flowLayoutPanelFarmers.WrapContents = false;
            // 
            // tabPageNotifications
            // 
            this.tabPageNotifications.Controls.Add(this.groupBoxNotifications);
            this.tabPageNotifications.Location = new System.Drawing.Point(4, 22);
            this.tabPageNotifications.Name = "tabPageNotifications";
            this.tabPageNotifications.Size = new System.Drawing.Size(1055, 528);
            this.tabPageNotifications.TabIndex = 9;
            this.tabPageNotifications.Text = "Notifications";
            this.tabPageNotifications.UseVisualStyleBackColor = true;
            // 
            // groupBoxNotifications
            // 
            this.groupBoxNotifications.Controls.Add(this.textBoxNotifications);
            this.groupBoxNotifications.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxNotifications.Location = new System.Drawing.Point(0, 0);
            this.groupBoxNotifications.Name = "groupBoxNotifications";
            this.groupBoxNotifications.Size = new System.Drawing.Size(1055, 528);
            this.groupBoxNotifications.TabIndex = 0;
            this.groupBoxNotifications.TabStop = false;
            this.groupBoxNotifications.Text = "Notifications";
            // 
            // textBoxNotifications
            // 
            this.textBoxNotifications.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxNotifications.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxNotifications.Location = new System.Drawing.Point(3, 16);
            this.textBoxNotifications.Multiline = true;
            this.textBoxNotifications.Name = "textBoxNotifications";
            this.textBoxNotifications.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxNotifications.Size = new System.Drawing.Size(1049, 509);
            this.textBoxNotifications.TabIndex = 0;
            // 
            // tabPageLog
            // 
            this.tabPageLog.Controls.Add(this.groupBoxLog);
            this.tabPageLog.Location = new System.Drawing.Point(4, 22);
            this.tabPageLog.Name = "tabPageLog";
            this.tabPageLog.Size = new System.Drawing.Size(1055, 528);
            this.tabPageLog.TabIndex = 8;
            this.tabPageLog.Text = "Log";
            this.tabPageLog.UseVisualStyleBackColor = true;
            // 
            // groupBoxLog
            // 
            this.groupBoxLog.Controls.Add(this.textBoxLog);
            this.groupBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxLog.Location = new System.Drawing.Point(0, 0);
            this.groupBoxLog.Margin = new System.Windows.Forms.Padding(0);
            this.groupBoxLog.Name = "groupBoxLog";
            this.groupBoxLog.Size = new System.Drawing.Size(1055, 528);
            this.groupBoxLog.TabIndex = 5;
            this.groupBoxLog.TabStop = false;
            this.groupBoxLog.Text = "Log";
            // 
            // textBoxLog
            // 
            this.textBoxLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLog.Location = new System.Drawing.Point(3, 16);
            this.textBoxLog.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(1049, 509);
            this.textBoxLog.TabIndex = 2;
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.groupBoxGUI);
            this.tabPageSettings.Controls.Add(this.groupBoxProxy);
            this.tabPageSettings.Controls.Add(this.buttonSettingsCancel);
            this.tabPageSettings.Controls.Add(this.buttonSettingsOK);
            this.tabPageSettings.Controls.Add(this.groupBoxBuilding);
            this.tabPageSettings.Controls.Add(this.groupBoxAdvanced);
            this.tabPageSettings.Controls.Add(this.groupBoxMail);
            this.tabPageSettings.Controls.Add(this.groupBoxGeneral);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Size = new System.Drawing.Size(1055, 528);
            this.tabPageSettings.TabIndex = 2;
            this.tabPageSettings.Text = "Settings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // groupBoxGUI
            // 
            this.groupBoxGUI.Controls.Add(this.checkBoxGUIBuildingTooltips);
            this.groupBoxGUI.Controls.Add(this.labelBuildingTooltips);
            this.groupBoxGUI.Controls.Add(this.labelGUIBuildingLevelTargetColor);
            this.groupBoxGUI.Controls.Add(this.labelGUIBuildingLevelTarget);
            this.groupBoxGUI.Location = new System.Drawing.Point(290, 3);
            this.groupBoxGUI.Name = "groupBoxGUI";
            this.groupBoxGUI.Size = new System.Drawing.Size(200, 170);
            this.groupBoxGUI.TabIndex = 108;
            this.groupBoxGUI.TabStop = false;
            this.groupBoxGUI.Text = "GUI";
            // 
            // labelGUIBuildingLevelTargetColor
            // 
            this.labelGUIBuildingLevelTargetColor.AutoSize = true;
            this.labelGUIBuildingLevelTargetColor.BackColor = System.Drawing.Color.GreenYellow;
            this.labelGUIBuildingLevelTargetColor.Location = new System.Drawing.Point(120, 25);
            this.labelGUIBuildingLevelTargetColor.MaximumSize = new System.Drawing.Size(26, 13);
            this.labelGUIBuildingLevelTargetColor.MinimumSize = new System.Drawing.Size(26, 13);
            this.labelGUIBuildingLevelTargetColor.Name = "labelGUIBuildingLevelTargetColor";
            this.labelGUIBuildingLevelTargetColor.Size = new System.Drawing.Size(26, 13);
            this.labelGUIBuildingLevelTargetColor.TabIndex = 1;
            this.labelGUIBuildingLevelTargetColor.Click += new System.EventHandler(this.labelGUIBuildingLevelTargetColor_Click);
            // 
            // labelGUIBuildingLevelTarget
            // 
            this.labelGUIBuildingLevelTarget.AutoSize = true;
            this.labelGUIBuildingLevelTarget.Location = new System.Drawing.Point(6, 25);
            this.labelGUIBuildingLevelTarget.Name = "labelGUIBuildingLevelTarget";
            this.labelGUIBuildingLevelTarget.Size = new System.Drawing.Size(90, 13);
            this.labelGUIBuildingLevelTarget.TabIndex = 0;
            this.labelGUIBuildingLevelTarget.Text = "Building lvl. target";
            // 
            // groupBoxProxy
            // 
            this.groupBoxProxy.Controls.Add(this.labelProxyEnabled);
            this.groupBoxProxy.Controls.Add(this.checkBoxProxyEnabled);
            this.groupBoxProxy.Controls.Add(this.textBoxProxyPassword);
            this.groupBoxProxy.Controls.Add(this.textBoxProxyUserName);
            this.groupBoxProxy.Controls.Add(this.labelProxyPassword);
            this.groupBoxProxy.Controls.Add(this.labelProxyUserName);
            this.groupBoxProxy.Controls.Add(this.textBoxProxyServer);
            this.groupBoxProxy.Controls.Add(this.labelProxyServer);
            this.groupBoxProxy.Location = new System.Drawing.Point(833, 383);
            this.groupBoxProxy.Name = "groupBoxProxy";
            this.groupBoxProxy.Size = new System.Drawing.Size(210, 138);
            this.groupBoxProxy.TabIndex = 107;
            this.groupBoxProxy.TabStop = false;
            this.groupBoxProxy.Text = "Proxy";
            this.groupBoxProxy.Visible = false;
            // 
            // labelProxyEnabled
            // 
            this.labelProxyEnabled.AutoSize = true;
            this.labelProxyEnabled.Location = new System.Drawing.Point(6, 25);
            this.labelProxyEnabled.Name = "labelProxyEnabled";
            this.labelProxyEnabled.Size = new System.Drawing.Size(46, 13);
            this.labelProxyEnabled.TabIndex = 107;
            this.labelProxyEnabled.Text = "Enabled";
            // 
            // checkBoxProxyEnabled
            // 
            this.checkBoxProxyEnabled.AutoSize = true;
            this.checkBoxProxyEnabled.Location = new System.Drawing.Point(100, 25);
            this.checkBoxProxyEnabled.Name = "checkBoxProxyEnabled";
            this.checkBoxProxyEnabled.Size = new System.Drawing.Size(15, 14);
            this.checkBoxProxyEnabled.TabIndex = 106;
            this.checkBoxProxyEnabled.UseVisualStyleBackColor = true;
            // 
            // textBoxProxyPassword
            // 
            this.textBoxProxyPassword.Location = new System.Drawing.Point(100, 100);
            this.textBoxProxyPassword.Name = "textBoxProxyPassword";
            this.textBoxProxyPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxProxyPassword.TabIndex = 105;
            this.textBoxProxyPassword.UseSystemPasswordChar = true;
            // 
            // textBoxProxyUserName
            // 
            this.textBoxProxyUserName.Location = new System.Drawing.Point(100, 75);
            this.textBoxProxyUserName.Name = "textBoxProxyUserName";
            this.textBoxProxyUserName.Size = new System.Drawing.Size(100, 20);
            this.textBoxProxyUserName.TabIndex = 104;
            // 
            // labelProxyPassword
            // 
            this.labelProxyPassword.AutoSize = true;
            this.labelProxyPassword.Location = new System.Drawing.Point(6, 100);
            this.labelProxyPassword.Name = "labelProxyPassword";
            this.labelProxyPassword.Size = new System.Drawing.Size(53, 13);
            this.labelProxyPassword.TabIndex = 103;
            this.labelProxyPassword.Text = "Password";
            // 
            // labelProxyUserName
            // 
            this.labelProxyUserName.AutoSize = true;
            this.labelProxyUserName.Location = new System.Drawing.Point(6, 75);
            this.labelProxyUserName.Name = "labelProxyUserName";
            this.labelProxyUserName.Size = new System.Drawing.Size(60, 13);
            this.labelProxyUserName.TabIndex = 102;
            this.labelProxyUserName.Text = "User Name";
            // 
            // textBoxProxyServer
            // 
            this.textBoxProxyServer.Location = new System.Drawing.Point(100, 50);
            this.textBoxProxyServer.Name = "textBoxProxyServer";
            this.textBoxProxyServer.Size = new System.Drawing.Size(100, 20);
            this.textBoxProxyServer.TabIndex = 101;
            this.textBoxProxyServer.Text = "http://1.2.3.4:3128";
            // 
            // labelProxyServer
            // 
            this.labelProxyServer.AutoSize = true;
            this.labelProxyServer.Location = new System.Drawing.Point(6, 50);
            this.labelProxyServer.Name = "labelProxyServer";
            this.labelProxyServer.Size = new System.Drawing.Size(38, 13);
            this.labelProxyServer.TabIndex = 100;
            this.labelProxyServer.Text = "Server";
            // 
            // buttonSettingsCancel
            // 
            this.buttonSettingsCancel.Location = new System.Drawing.Point(89, 505);
            this.buttonSettingsCancel.Name = "buttonSettingsCancel";
            this.buttonSettingsCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonSettingsCancel.TabIndex = 106;
            this.buttonSettingsCancel.TabStop = false;
            this.buttonSettingsCancel.Text = "Cancel";
            this.buttonSettingsCancel.UseVisualStyleBackColor = true;
            this.buttonSettingsCancel.Click += new System.EventHandler(this.buttonSettingsCancel_Click);
            // 
            // buttonSettingsOK
            // 
            this.buttonSettingsOK.Location = new System.Drawing.Point(8, 505);
            this.buttonSettingsOK.Name = "buttonSettingsOK";
            this.buttonSettingsOK.Size = new System.Drawing.Size(75, 23);
            this.buttonSettingsOK.TabIndex = 105;
            this.buttonSettingsOK.TabStop = false;
            this.buttonSettingsOK.Text = "OK";
            this.buttonSettingsOK.UseVisualStyleBackColor = true;
            this.buttonSettingsOK.Click += new System.EventHandler(this.buttonSettingsOK_Click);
            // 
            // groupBoxBuilding
            // 
            this.groupBoxBuilding.Controls.Add(this.labelTimer);
            this.groupBoxBuilding.Controls.Add(this.numericUpDownTimerQueue);
            this.groupBoxBuilding.Location = new System.Drawing.Point(8, 173);
            this.groupBoxBuilding.Name = "groupBoxBuilding";
            this.groupBoxBuilding.Size = new System.Drawing.Size(275, 59);
            this.groupBoxBuilding.TabIndex = 104;
            this.groupBoxBuilding.TabStop = false;
            this.groupBoxBuilding.Text = "Building / Training Queue";
            // 
            // labelTimer
            // 
            this.labelTimer.AutoSize = true;
            this.labelTimer.Location = new System.Drawing.Point(6, 25);
            this.labelTimer.Name = "labelTimer";
            this.labelTimer.Size = new System.Drawing.Size(79, 13);
            this.labelTimer.TabIndex = 99;
            this.labelTimer.Text = "Timer (Minutes)";
            // 
            // numericUpDownTimerQueue
            // 
            this.numericUpDownTimerQueue.Location = new System.Drawing.Point(125, 25);
            this.numericUpDownTimerQueue.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numericUpDownTimerQueue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTimerQueue.Name = "numericUpDownTimerQueue";
            this.numericUpDownTimerQueue.Size = new System.Drawing.Size(134, 20);
            this.numericUpDownTimerQueue.TabIndex = 11;
            this.numericUpDownTimerQueue.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // groupBoxAdvanced
            // 
            this.groupBoxAdvanced.Controls.Add(this.numericUpDownTimerRefresh);
            this.groupBoxAdvanced.Controls.Add(this.labelRefreshTimer);
            this.groupBoxAdvanced.Controls.Add(this.numericUpDownTimerReconnect);
            this.groupBoxAdvanced.Controls.Add(this.labelReconnectTimer);
            this.groupBoxAdvanced.Controls.Add(this.labelDebugMode);
            this.groupBoxAdvanced.Controls.Add(this.checkBoxDebugMode);
            this.groupBoxAdvanced.Location = new System.Drawing.Point(8, 407);
            this.groupBoxAdvanced.Name = "groupBoxAdvanced";
            this.groupBoxAdvanced.Size = new System.Drawing.Size(275, 92);
            this.groupBoxAdvanced.TabIndex = 103;
            this.groupBoxAdvanced.TabStop = false;
            this.groupBoxAdvanced.Text = "Advanced";
            // 
            // numericUpDownTimerRefresh
            // 
            this.numericUpDownTimerRefresh.Location = new System.Drawing.Point(125, 25);
            this.numericUpDownTimerRefresh.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTimerRefresh.Name = "numericUpDownTimerRefresh";
            this.numericUpDownTimerRefresh.Size = new System.Drawing.Size(134, 20);
            this.numericUpDownTimerRefresh.TabIndex = 103;
            this.numericUpDownTimerRefresh.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // labelRefreshTimer
            // 
            this.labelRefreshTimer.AutoSize = true;
            this.labelRefreshTimer.Location = new System.Drawing.Point(6, 25);
            this.labelRefreshTimer.Name = "labelRefreshTimer";
            this.labelRefreshTimer.Size = new System.Drawing.Size(102, 13);
            this.labelRefreshTimer.TabIndex = 102;
            this.labelRefreshTimer.Text = "Refresh Timer (Min.)";
            // 
            // numericUpDownTimerReconnect
            // 
            this.numericUpDownTimerReconnect.Location = new System.Drawing.Point(125, 50);
            this.numericUpDownTimerReconnect.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTimerReconnect.Name = "numericUpDownTimerReconnect";
            this.numericUpDownTimerReconnect.Size = new System.Drawing.Size(134, 20);
            this.numericUpDownTimerReconnect.TabIndex = 101;
            this.numericUpDownTimerReconnect.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelReconnectTimer
            // 
            this.labelReconnectTimer.AutoSize = true;
            this.labelReconnectTimer.Location = new System.Drawing.Point(6, 50);
            this.labelReconnectTimer.Name = "labelReconnectTimer";
            this.labelReconnectTimer.Size = new System.Drawing.Size(118, 13);
            this.labelReconnectTimer.TabIndex = 100;
            this.labelReconnectTimer.Text = "Reconnect Timer (Min.)";
            // 
            // labelDebugMode
            // 
            this.labelDebugMode.AutoSize = true;
            this.labelDebugMode.Location = new System.Drawing.Point(6, 75);
            this.labelDebugMode.Name = "labelDebugMode";
            this.labelDebugMode.Size = new System.Drawing.Size(69, 13);
            this.labelDebugMode.TabIndex = 99;
            this.labelDebugMode.Text = "Debug Mode";
            // 
            // checkBoxDebugMode
            // 
            this.checkBoxDebugMode.AutoSize = true;
            this.checkBoxDebugMode.Checked = true;
            this.checkBoxDebugMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDebugMode.Location = new System.Drawing.Point(125, 75);
            this.checkBoxDebugMode.Name = "checkBoxDebugMode";
            this.checkBoxDebugMode.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDebugMode.TabIndex = 12;
            this.checkBoxDebugMode.UseVisualStyleBackColor = true;
            // 
            // groupBoxMail
            // 
            this.groupBoxMail.Controls.Add(this.numericUpDownMailPort);
            this.groupBoxMail.Controls.Add(this.textBoxMailServer);
            this.groupBoxMail.Controls.Add(this.textBoxMailPassword);
            this.groupBoxMail.Controls.Add(this.textBoxMailUsername);
            this.groupBoxMail.Controls.Add(this.labelMailServer);
            this.groupBoxMail.Controls.Add(this.labelMailPort);
            this.groupBoxMail.Controls.Add(this.labelMailPassword);
            this.groupBoxMail.Controls.Add(this.labelMailUsername);
            this.groupBoxMail.Controls.Add(this.checkBoxEmail);
            this.groupBoxMail.Controls.Add(this.labelNotify);
            this.groupBoxMail.Controls.Add(this.labelEmail);
            this.groupBoxMail.Controls.Add(this.textBoxEmail);
            this.groupBoxMail.Location = new System.Drawing.Point(8, 232);
            this.groupBoxMail.Name = "groupBoxMail";
            this.groupBoxMail.Size = new System.Drawing.Size(275, 175);
            this.groupBoxMail.TabIndex = 102;
            this.groupBoxMail.TabStop = false;
            this.groupBoxMail.Text = "E-Mail";
            // 
            // numericUpDownMailPort
            // 
            this.numericUpDownMailPort.Location = new System.Drawing.Point(125, 151);
            this.numericUpDownMailPort.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numericUpDownMailPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMailPort.Name = "numericUpDownMailPort";
            this.numericUpDownMailPort.Size = new System.Drawing.Size(134, 20);
            this.numericUpDownMailPort.TabIndex = 111;
            this.numericUpDownMailPort.Value = new decimal(new int[] {
            587,
            0,
            0,
            0});
            // 
            // textBoxMailServer
            // 
            this.textBoxMailServer.Location = new System.Drawing.Point(125, 125);
            this.textBoxMailServer.Name = "textBoxMailServer";
            this.textBoxMailServer.Size = new System.Drawing.Size(134, 20);
            this.textBoxMailServer.TabIndex = 110;
            this.textBoxMailServer.Text = "smtp.gmail.com";
            // 
            // textBoxMailPassword
            // 
            this.textBoxMailPassword.Location = new System.Drawing.Point(125, 100);
            this.textBoxMailPassword.Name = "textBoxMailPassword";
            this.textBoxMailPassword.Size = new System.Drawing.Size(134, 20);
            this.textBoxMailPassword.TabIndex = 109;
            this.textBoxMailPassword.Text = "password";
            this.textBoxMailPassword.UseSystemPasswordChar = true;
            // 
            // textBoxMailUsername
            // 
            this.textBoxMailUsername.Location = new System.Drawing.Point(125, 75);
            this.textBoxMailUsername.Name = "textBoxMailUsername";
            this.textBoxMailUsername.Size = new System.Drawing.Size(134, 20);
            this.textBoxMailUsername.TabIndex = 108;
            this.textBoxMailUsername.Text = "name@gmail.com";
            // 
            // labelMailServer
            // 
            this.labelMailServer.AutoSize = true;
            this.labelMailServer.Location = new System.Drawing.Point(6, 125);
            this.labelMailServer.Name = "labelMailServer";
            this.labelMailServer.Size = new System.Drawing.Size(38, 13);
            this.labelMailServer.TabIndex = 107;
            this.labelMailServer.Text = "Server";
            // 
            // labelMailPort
            // 
            this.labelMailPort.AutoSize = true;
            this.labelMailPort.Location = new System.Drawing.Point(6, 150);
            this.labelMailPort.Name = "labelMailPort";
            this.labelMailPort.Size = new System.Drawing.Size(26, 13);
            this.labelMailPort.TabIndex = 106;
            this.labelMailPort.Text = "Port";
            // 
            // labelMailPassword
            // 
            this.labelMailPassword.AutoSize = true;
            this.labelMailPassword.Location = new System.Drawing.Point(6, 100);
            this.labelMailPassword.Name = "labelMailPassword";
            this.labelMailPassword.Size = new System.Drawing.Size(53, 13);
            this.labelMailPassword.TabIndex = 105;
            this.labelMailPassword.Text = "Password";
            // 
            // labelMailUsername
            // 
            this.labelMailUsername.AutoSize = true;
            this.labelMailUsername.Location = new System.Drawing.Point(6, 75);
            this.labelMailUsername.Name = "labelMailUsername";
            this.labelMailUsername.Size = new System.Drawing.Size(60, 13);
            this.labelMailUsername.TabIndex = 104;
            this.labelMailUsername.Text = "User Name";
            // 
            // checkBoxEmail
            // 
            this.checkBoxEmail.AutoSize = true;
            this.checkBoxEmail.Checked = true;
            this.checkBoxEmail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEmail.Location = new System.Drawing.Point(125, 50);
            this.checkBoxEmail.Name = "checkBoxEmail";
            this.checkBoxEmail.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEmail.TabIndex = 102;
            this.checkBoxEmail.UseVisualStyleBackColor = true;
            // 
            // labelNotify
            // 
            this.labelNotify.AutoSize = true;
            this.labelNotify.Location = new System.Drawing.Point(6, 50);
            this.labelNotify.Name = "labelNotify";
            this.labelNotify.Size = new System.Drawing.Size(71, 13);
            this.labelNotify.TabIndex = 103;
            this.labelNotify.Text = "Notify By Mail";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(6, 25);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(32, 13);
            this.labelEmail.TabIndex = 100;
            this.labelEmail.Text = "Email";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(125, 25);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(134, 20);
            this.textBoxEmail.TabIndex = 101;
            this.textBoxEmail.Text = "name@domain.com";
            // 
            // groupBoxGeneral
            // 
            this.groupBoxGeneral.Controls.Add(this.checkBoxAutoStart);
            this.groupBoxGeneral.Controls.Add(this.labelAutoStart);
            this.groupBoxGeneral.Controls.Add(this.labelUserName);
            this.groupBoxGeneral.Controls.Add(this.labelPassword);
            this.groupBoxGeneral.Controls.Add(this.textBoxUserName);
            this.groupBoxGeneral.Controls.Add(this.textBoxPassword);
            this.groupBoxGeneral.Controls.Add(this.labelMainServer);
            this.groupBoxGeneral.Controls.Add(this.textBoxMainServer);
            this.groupBoxGeneral.Controls.Add(this.labelServer);
            this.groupBoxGeneral.Controls.Add(this.textBoxServer);
            this.groupBoxGeneral.Controls.Add(this.labelTrayIcon);
            this.groupBoxGeneral.Controls.Add(this.checkBoxTrayIcon);
            this.groupBoxGeneral.Location = new System.Drawing.Point(8, 3);
            this.groupBoxGeneral.Name = "groupBoxGeneral";
            this.groupBoxGeneral.Size = new System.Drawing.Size(275, 170);
            this.groupBoxGeneral.TabIndex = 100;
            this.groupBoxGeneral.TabStop = false;
            this.groupBoxGeneral.Text = "General";
            // 
            // checkBoxAutoStart
            // 
            this.checkBoxAutoStart.AutoSize = true;
            this.checkBoxAutoStart.Location = new System.Drawing.Point(125, 125);
            this.checkBoxAutoStart.Name = "checkBoxAutoStart";
            this.checkBoxAutoStart.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAutoStart.TabIndex = 101;
            this.checkBoxAutoStart.UseVisualStyleBackColor = true;
            // 
            // labelAutoStart
            // 
            this.labelAutoStart.AutoSize = true;
            this.labelAutoStart.Location = new System.Drawing.Point(6, 125);
            this.labelAutoStart.Name = "labelAutoStart";
            this.labelAutoStart.Size = new System.Drawing.Size(54, 13);
            this.labelAutoStart.TabIndex = 100;
            this.labelAutoStart.Text = "Auto Start";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Location = new System.Drawing.Point(6, 25);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(60, 13);
            this.labelUserName.TabIndex = 99;
            this.labelUserName.Text = "User Name";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(6, 50);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(53, 13);
            this.labelPassword.TabIndex = 99;
            this.labelPassword.Text = "Password";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(125, 25);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(134, 20);
            this.textBoxUserName.TabIndex = 0;
            this.textBoxUserName.Text = "name";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(125, 50);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(134, 20);
            this.textBoxPassword.TabIndex = 1;
            this.textBoxPassword.Text = "password";
            this.textBoxPassword.UseSystemPasswordChar = true;
            // 
            // labelMainServer
            // 
            this.labelMainServer.AutoSize = true;
            this.labelMainServer.Location = new System.Drawing.Point(6, 75);
            this.labelMainServer.Name = "labelMainServer";
            this.labelMainServer.Size = new System.Drawing.Size(64, 13);
            this.labelMainServer.TabIndex = 99;
            this.labelMainServer.Text = "Main Server";
            // 
            // textBoxMainServer
            // 
            this.textBoxMainServer.Location = new System.Drawing.Point(125, 75);
            this.textBoxMainServer.Name = "textBoxMainServer";
            this.textBoxMainServer.Size = new System.Drawing.Size(134, 20);
            this.textBoxMainServer.TabIndex = 2;
            this.textBoxMainServer.Text = "en.grepolis.com";
            // 
            // labelServer
            // 
            this.labelServer.AutoSize = true;
            this.labelServer.Location = new System.Drawing.Point(6, 100);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(38, 13);
            this.labelServer.TabIndex = 99;
            this.labelServer.Text = "Server";
            // 
            // textBoxServer
            // 
            this.textBoxServer.Location = new System.Drawing.Point(125, 100);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(134, 20);
            this.textBoxServer.TabIndex = 3;
            this.textBoxServer.Text = "en1.grepolis.com";
            // 
            // labelTrayIcon
            // 
            this.labelTrayIcon.AutoSize = true;
            this.labelTrayIcon.Location = new System.Drawing.Point(6, 150);
            this.labelTrayIcon.Name = "labelTrayIcon";
            this.labelTrayIcon.Size = new System.Drawing.Size(87, 13);
            this.labelTrayIcon.TabIndex = 99;
            this.labelTrayIcon.Text = "Minimize To Tray";
            // 
            // checkBoxTrayIcon
            // 
            this.checkBoxTrayIcon.AutoSize = true;
            this.checkBoxTrayIcon.Checked = true;
            this.checkBoxTrayIcon.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTrayIcon.Location = new System.Drawing.Point(125, 150);
            this.checkBoxTrayIcon.Name = "checkBoxTrayIcon";
            this.checkBoxTrayIcon.Size = new System.Drawing.Size(15, 14);
            this.checkBoxTrayIcon.TabIndex = 4;
            this.checkBoxTrayIcon.UseVisualStyleBackColor = true;
            // 
            // tabPageBrowser
            // 
            this.tabPageBrowser.Controls.Add(this.webBrowserGrepo);
            this.tabPageBrowser.Location = new System.Drawing.Point(4, 22);
            this.tabPageBrowser.Name = "tabPageBrowser";
            this.tabPageBrowser.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBrowser.Size = new System.Drawing.Size(1055, 528);
            this.tabPageBrowser.TabIndex = 1;
            this.tabPageBrowser.Text = "Browser";
            this.tabPageBrowser.UseVisualStyleBackColor = true;
            // 
            // webBrowserGrepo
            // 
            this.webBrowserGrepo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserGrepo.Location = new System.Drawing.Point(3, 3);
            this.webBrowserGrepo.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserGrepo.Name = "webBrowserGrepo";
            this.webBrowserGrepo.ScriptErrorsSuppressed = true;
            this.webBrowserGrepo.Size = new System.Drawing.Size(1049, 522);
            this.webBrowserGrepo.TabIndex = 0;
            this.webBrowserGrepo.Url = new System.Uri("about:blank", System.UriKind.Absolute);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 560);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1069, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.statusStrip1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.22337F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1069, 582);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // labelBuildingTooltips
            // 
            this.labelBuildingTooltips.AutoSize = true;
            this.labelBuildingTooltips.Location = new System.Drawing.Point(6, 50);
            this.labelBuildingTooltips.Name = "labelBuildingTooltips";
            this.labelBuildingTooltips.Size = new System.Drawing.Size(84, 13);
            this.labelBuildingTooltips.TabIndex = 2;
            this.labelBuildingTooltips.Text = "Building Tooltips";
            // 
            // checkBoxGUIBuildingTooltips
            // 
            this.checkBoxGUIBuildingTooltips.AutoSize = true;
            this.checkBoxGUIBuildingTooltips.Checked = true;
            this.checkBoxGUIBuildingTooltips.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGUIBuildingTooltips.Location = new System.Drawing.Point(120, 50);
            this.checkBoxGUIBuildingTooltips.Name = "checkBoxGUIBuildingTooltips";
            this.checkBoxGUIBuildingTooltips.Size = new System.Drawing.Size(15, 14);
            this.checkBoxGUIBuildingTooltips.TabIndex = 3;
            this.checkBoxGUIBuildingTooltips.UseVisualStyleBackColor = true;
            // 
            // GrepolisBot2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 582);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1075, 50);
            this.Name = "GrepolisBot2";
            this.Text = "Grepolis Bot 2";
            this.Resize += new System.EventHandler(this.GrepolisBotII_Resize);
            this.tabControl1.ResumeLayout(false);
            this.tabPagePlayer.ResumeLayout(false);
            this.tabPagePlayer.PerformLayout();
            this.groupBoxNotes.ResumeLayout(false);
            this.groupBoxNotes.PerformLayout();
            this.groupBoxTimers.ResumeLayout(false);
            this.groupBoxTimers.PerformLayout();
            this.groupBoxControls.ResumeLayout(false);
            this.tabPageBuildings.ResumeLayout(false);
            this.tabPageUnits.ResumeLayout(false);
            this.tabPageCulture.ResumeLayout(false);
            this.groupBoxCulture.ResumeLayout(false);
            this.groupBoxCulture.PerformLayout();
            this.tabPageFarmers.ResumeLayout(false);
            this.tabPageNotifications.ResumeLayout(false);
            this.groupBoxNotifications.ResumeLayout(false);
            this.groupBoxNotifications.PerformLayout();
            this.tabPageLog.ResumeLayout(false);
            this.groupBoxLog.ResumeLayout(false);
            this.groupBoxLog.PerformLayout();
            this.tabPageSettings.ResumeLayout(false);
            this.groupBoxGUI.ResumeLayout(false);
            this.groupBoxGUI.PerformLayout();
            this.groupBoxProxy.ResumeLayout(false);
            this.groupBoxProxy.PerformLayout();
            this.groupBoxBuilding.ResumeLayout(false);
            this.groupBoxBuilding.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerQueue)).EndInit();
            this.groupBoxAdvanced.ResumeLayout(false);
            this.groupBoxAdvanced.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimerReconnect)).EndInit();
            this.groupBoxMail.ResumeLayout(false);
            this.groupBoxMail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMailPort)).EndInit();
            this.groupBoxGeneral.ResumeLayout(false);
            this.groupBoxGeneral.PerformLayout();
            this.tabPageBrowser.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageBrowser;
        private System.Windows.Forms.WebBrowser webBrowserGrepo;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.Button buttonSettingsCancel;
        private System.Windows.Forms.Button buttonSettingsOK;
        private System.Windows.Forms.GroupBox groupBoxBuilding;
        private System.Windows.Forms.Label labelTimer;
        private System.Windows.Forms.NumericUpDown numericUpDownTimerQueue;
        private System.Windows.Forms.GroupBox groupBoxAdvanced;
        private System.Windows.Forms.NumericUpDown numericUpDownTimerReconnect;
        private System.Windows.Forms.Label labelReconnectTimer;
        private System.Windows.Forms.Label labelDebugMode;
        private System.Windows.Forms.CheckBox checkBoxDebugMode;
        private System.Windows.Forms.GroupBox groupBoxMail;
        private System.Windows.Forms.TextBox textBoxMailServer;
        private System.Windows.Forms.TextBox textBoxMailPassword;
        private System.Windows.Forms.TextBox textBoxMailUsername;
        private System.Windows.Forms.Label labelMailServer;
        private System.Windows.Forms.Label labelMailPort;
        private System.Windows.Forms.Label labelMailPassword;
        private System.Windows.Forms.Label labelMailUsername;
        private System.Windows.Forms.CheckBox checkBoxEmail;
        private System.Windows.Forms.Label labelNotify;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.GroupBox groupBoxGeneral;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelMainServer;
        public System.Windows.Forms.TextBox textBoxMainServer;
        private System.Windows.Forms.Label labelServer;
        public System.Windows.Forms.TextBox textBoxServer;
        private System.Windows.Forms.Label labelTrayIcon;
        private System.Windows.Forms.CheckBox checkBoxTrayIcon;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TabPage tabPagePlayer;
        private System.Windows.Forms.GroupBox groupBoxControls;
        private System.Windows.Forms.Button buttonTownStartStop;
        private System.Windows.Forms.Button buttonTownLogin;
        private System.Windows.Forms.TabPage tabPageBuildings;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBuildings;
        private System.Windows.Forms.NumericUpDown numericUpDownMailPort;
        private System.Windows.Forms.CheckBox checkBoxAutoStart;
        private System.Windows.Forms.Label labelAutoStart;
        private System.Windows.Forms.NumericUpDown numericUpDownTimerRefresh;
        private System.Windows.Forms.Label labelRefreshTimer;
        private System.Windows.Forms.GroupBox groupBoxTimers;
        private System.Windows.Forms.Label labelQueueTimeLeft;
        private System.Windows.Forms.Label labelNextUpdateTimeLeft;
        private System.Windows.Forms.Label labelQueue;
        private System.Windows.Forms.Label labelNextUpdate;
        private System.Windows.Forms.Label labelReconnectTimeLeft;
        private System.Windows.Forms.Label labelReconnect;
        private System.Windows.Forms.TabPage tabPageUnits;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelUnits;
        private System.Windows.Forms.GroupBox groupBoxNotes;
        private System.Windows.Forms.TextBox textBoxNotes;
        private System.Windows.Forms.Label labelConnectedTimer;
        private System.Windows.Forms.Label labelConnected;
        private System.Windows.Forms.TabPage tabPageCulture;
        private System.Windows.Forms.ProgressBar progressBarCP;
        private System.Windows.Forms.GroupBox groupBoxCulture;
        private System.Windows.Forms.Label labelCulturalCities;
        private System.Windows.Forms.Label labelCulturalLevel;
        private System.Windows.Forms.TabPage tabPageFarmers;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFarmers;
        private System.Windows.Forms.Label labelReload;
        private System.Windows.Forms.GroupBox groupBoxProxy;
        private System.Windows.Forms.Label labelProxyEnabled;
        private System.Windows.Forms.CheckBox checkBoxProxyEnabled;
        private System.Windows.Forms.TextBox textBoxProxyPassword;
        private System.Windows.Forms.TextBox textBoxProxyUserName;
        private System.Windows.Forms.Label labelProxyPassword;
        private System.Windows.Forms.Label labelProxyUserName;
        private System.Windows.Forms.TextBox textBoxProxyServer;
        private System.Windows.Forms.Label labelProxyServer;
        private System.Windows.Forms.Button buttonRestoreGUI;
        private System.Windows.Forms.LinkLabel linkLabelLatestVersion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabPage tabPageLog;
        private System.Windows.Forms.GroupBox groupBoxLog;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.GroupBox groupBoxGUI;
        private System.Windows.Forms.Label labelGUIBuildingLevelTargetColor;
        private System.Windows.Forms.Label labelGUIBuildingLevelTarget;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TabPage tabPageNotifications;
        private System.Windows.Forms.GroupBox groupBoxNotifications;
        private System.Windows.Forms.TextBox textBoxNotifications;
        private System.Windows.Forms.CheckBox checkBoxGUIBuildingTooltips;
        private System.Windows.Forms.Label labelBuildingTooltips;
    }
}

