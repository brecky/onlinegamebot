﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Globalization;

namespace GrepolisBot2
{
    public partial class GrepolisBot2 : Form
    {
        //Useragent
        [DllImport("urlmon.dll", CharSet = CharSet.Ansi)]
        private static extern int UrlMkSetSessionOption(int dwOption, string pBuffer, int dwBufferLength, int dwReserved);
        const int URLMON_OPTION_USERAGENT = 0x10000001;

        //General
        private NotifyIcon m_NotifyIcon = new NotifyIcon();
        private ToolTip m_Tooltip = new ToolTip();

        //Custom classes
        private Controller m_Controller = new Controller();

        //Thread Handling
        public delegate void SetStatusBarCallback(object sender);
        public delegate void UpdateFarmerGUICallback();
        public delegate void SetGuiToTimeoutProcessedStateCallBack();
        public delegate void SetGuiToLoggedInStateCallBack();
        public delegate void UpdateGUIGodCallBack();
        public delegate void UpdateRefreshTimerCallBack(object sender);
        public delegate void UpdateQueueTimerCallBack(object sender);
        public delegate void UpdateReconnectTimerCallBack(object sender);
        public delegate void UpdateConnectedTimerCallBack(object sender);
        public delegate void UpdateBuildingQueueCallBack(object sender);
        public delegate void LogCallBack(object sender);
        public delegate void VersionInfoCallBack(object sender);
        public delegate void serverRequestDelayRequestCallBack();


//-->Constructor

        public GrepolisBot2()
        {
            InitializeComponent();
            initMisc();
            initEventHandlers();
            initNotifyIcon();
            loadSettings();
            initBrowser();
            initLinkLabel();
            initTimers();//Must be called after settings are loaded
            loadNotes();

            //Custom gui customization
            this.Text = String.Format("{0} {1}", AssemblyTitle, AssemblyVersion);
            colorDialog1.AllowFullOpen = false;

            //progressBarCP.CreateGraphics().DrawString("Test", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(progressBarCP.Width / 2 - 10, progressBarCP.Height / 2 - 7));
            m_Tooltip.SetToolTip(progressBarCP, "0/3");
            m_Controller.checkVersion();
        }

//-->Methods

        private void initMisc()
        {
            //Fix for "The remote server returned an error: (417) Expectation Failed."
            System.Net.ServicePointManager.Expect100Continue = false;
            toolStripStatusLabel1.Text = String.Format("{0} v{1}", AssemblyTitle, AssemblyVersion);
        }

        private void initBrowser()
        {
            Settings l_Settings = Settings.Instance;

            /** This part works with the useragent of the webbrowser control **/
            string l_UserAgent = "";
            string l_Js = @"<script type='text/javascript'>function getUserAgent(){document.write(navigator.userAgent)}</script>";
            webBrowserGrepo.Document.Write(l_Js);
            webBrowserGrepo.Document.InvokeScript("getUserAgent");
            l_UserAgent = webBrowserGrepo.DocumentText.Substring(l_Js.Length);
            //l_Settings.AdvUserAgent = l_UserAgent;//Disable this when using a custom user-agent

            /** This part works with a custom useragent **/
            UrlMkSetSessionOption(URLMON_OPTION_USERAGENT, l_Settings.AdvUserAgent, l_Settings.AdvUserAgent.Length, 0);

            webBrowserGrepo.ScriptErrorsSuppressed = true;
        }

        private void initEventHandlers()
        {
            m_NotifyIcon.Click += new EventHandler(m_NotifyIcon_Click);
            m_Controller.statusBarUpdated += new Controller.SetStatusBarHandler(m_Controller_statusBarUpdated);
            m_Controller.farmerGUIUpdated += new Controller.UpdateFarmerGUIHandler(m_Controller_farmerGUIUpdated);
            m_Controller.timeoutProcessedStateChanged += new Controller.SetGuiToTimeoutProcessedStateHandler(m_Controller_timeoutProcessedStateChanged);
            m_Controller.loggedInStateChanged += new Controller.SetGuiToLoggedInStateHandler(m_Controller_loggedInStateChanged);
            m_Controller.godGUIUpdated += new Controller.UpdateGUIGodHandler(m_Controller_godGUIUpdated);
            m_Controller.refreshTimerUpdated += new Controller.updateRefreshTimerHandler(m_Controller_refreshTimerUpdated);
            m_Controller.queueTimerUpdated += new Controller.updateQueueTimerHandler(m_Controller_queueTimerUpdated);
            m_Controller.reconnectTimerUpdated += new Controller.updateReconnectTimerHandler(m_Controller_reconnectTimerUpdated);
            m_Controller.connectedTimerUpdated += new Controller.updateConnectedTimerHandler(m_Controller_connectedTimerUpdated);
            m_Controller.buildingQueueUpdated += new Controller.updateBuildingQueueHandler(m_Controller_buildingQueueUpdated);
            m_Controller.versionInfoUpdated += new Controller.versionInfoHandler(m_Controller_versionInfoUpdated);
            m_Controller.logUpdated += new Controller.logHandler(m_Controller_logUpdated);
            m_Controller.serverRequestDelayRequested += new Controller.serverRequestDelayRequestHandler(m_Controller_serverRequestDelayRequested);
            webBrowserGrepo.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowserGrepo_DocumentCompleted);
        }

        /*
         * Initialize notify icon
         */
        private void initNotifyIcon()
        {
            m_NotifyIcon.Visible = true;
            m_NotifyIcon.BalloonTipText = "Grepolis bot 2";
            m_NotifyIcon.ShowBalloonTip(2);  //show balloon tip for 2 seconds
            m_NotifyIcon.Text = "Grepolis bot 2";
            m_NotifyIcon.Icon = this.Icon;
        }

        /*
         * Initialize timers 
         */
        private void initTimers()
        {
            m_Controller.initTimers();
        }

        private void initLinkLabel()
        {
            linkLabelLatestVersion.Text = "Latest version: unknown";
            linkLabelLatestVersion.Links.Add(0, linkLabelLatestVersion.Text.Length, "http://bots.uthar.nl/downloads");
            linkLabelLatestVersion.LinkClicked += new LinkLabelLinkClickedEventHandler(linkLabelLatestVersion_LinkClicked);
        }

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        private void loadNotes()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            textBoxNotes.Text = l_IOHandler.loadNotes();
        }

        /*
         * Load settings file
         */
        private void loadSettings()
        {
            //Note Modify this when adding new program settings (1/4)

            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                //Load settings from file
                l_IOHandler.loadProgramSettings();

                //General
                textBoxUserName.Text = l_Settings.GenUserName;
                textBoxPassword.Text = l_Settings.GenPassword;
                textBoxMainServer.Text = l_Settings.GenMainServer;
                textBoxServer.Text = l_Settings.GenServer;
                checkBoxAutoStart.Checked = l_Settings.GenAutoStart;
                checkBoxTrayIcon.Checked = l_Settings.GenToTryIcon;
                //Building/training queue
                numericUpDownTimerQueue.Value = l_Settings.QueueTimer;
                //E-Mail
                textBoxEmail.Text = l_Settings.MailAddress;
                checkBoxEmail.Checked = l_Settings.MailNotify;
                textBoxMailUsername.Text = l_Settings.MailUsername;
                textBoxMailPassword.Text = l_Settings.MailPassword;
                textBoxMailServer.Text = l_Settings.MailServer;
                numericUpDownMailPort.Value = l_Settings.MailPort;
                //Advanced
                numericUpDownTimerRefresh.Value = l_Settings.AdvTimerRefresh;
                numericUpDownTimerReconnect.Value = l_Settings.AdvTimerReconnect;
                checkBoxDebugMode.Checked = l_Settings.AdvDebugMode;
                //GUI
                labelGUIBuildingLevelTargetColor.BackColor = l_Settings.GUIBuildingLevelTargetColor;
                checkBoxGUIBuildingTooltips.Checked = l_Settings.GUIBuildingTooltipsEnabled;
                //Proxy
                checkBoxProxyEnabled.Checked = l_Settings.ProxyEnabled;
                textBoxProxyServer.Text = l_Settings.ProxyServer;
                textBoxProxyUserName.Text = l_Settings.ProxyUserName;
                textBoxProxyPassword.Text = l_Settings.ProxyPassword;
            }
            catch (Exception)
            {

            }
        }

        private void saveSettings()
        {
            //Note Modify this when adding new program settings (2/4)

            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                //General
                l_Settings.GenUserName = textBoxUserName.Text;
                l_Settings.GenPassword = textBoxPassword.Text;
                l_Settings.GenMainServer = textBoxMainServer.Text;
                l_Settings.GenServer = textBoxServer.Text;
                l_Settings.GenAutoStart = checkBoxAutoStart.Checked;
                l_Settings.GenToTryIcon = checkBoxTrayIcon.Checked;
                //Building/training queue
                l_Settings.QueueTimer = (int)numericUpDownTimerQueue.Value;
                //E-Mail
                l_Settings.MailAddress = textBoxEmail.Text;
                l_Settings.MailNotify = checkBoxEmail.Checked;
                l_Settings.MailUsername = textBoxMailUsername.Text;
                l_Settings.MailPassword = textBoxMailPassword.Text;
                l_Settings.MailServer = textBoxMailServer.Text;
                l_Settings.MailPort = (int)numericUpDownMailPort.Value;
                //Advanced
                l_Settings.AdvTimerRefresh = (int)numericUpDownTimerRefresh.Value;
                l_Settings.AdvTimerReconnect = (int)numericUpDownTimerReconnect.Value;
                l_Settings.AdvDebugMode = checkBoxDebugMode.Checked;
                //GUI
                l_Settings.GUIBuildingLevelTargetColor = labelGUIBuildingLevelTargetColor.BackColor;
                l_Settings.GUIBuildingTooltipsEnabled = checkBoxGUIBuildingTooltips.Checked;
                //Proxy
                l_Settings.ProxyEnabled = checkBoxProxyEnabled.Checked;
                l_Settings.ProxyServer = textBoxProxyServer.Text;
                l_Settings.ProxyUserName = textBoxProxyUserName.Text;
                l_Settings.ProxyPassword = textBoxProxyPassword.Text;

                //Save settings to file
                l_IOHandler.saveProgramSettings();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in saveSettings(): " + e.Message);
            }
        }

        private void login()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (!m_Controller.LoggedIn)
                {
                    m_Controller.State = "login";

                    setStatusBar("Connection to: " + l_Settings.GenUserName + "@" + l_Settings.GenServer);

                    string l_Url = "http://" + l_Settings.GenMainServer + "/start?action=login";
                    Uri l_Uri = new Uri(l_Url);
                    String l_ServerNr = "";
                    l_ServerNr = l_Settings.GenServer.Substring(0, l_Settings.GenServer.IndexOf(".", 0));

                    System.Text.Encoding l_Encoding = System.Text.Encoding.UTF8;
                    string l_PostData = "world=" + l_ServerNr + "&facebook_login=&portal_sid=&name=" + l_Settings.GenUserName + "&password=" + l_Settings.GenPassword;
                    byte[] l_PostDataBytes = l_Encoding.GetBytes(l_PostData);
                    webBrowserGrepo.Navigate(l_Uri, "", l_PostDataBytes, "Content-Type: application/x-www-form-urlencoded");
                    //webBrowserGrepo.Navigate("http://whatsmyuseragent.com");
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in login(): " + e.Message);
            }
        }

        private void disableProxyGUI()
        {
            checkBoxProxyEnabled.Enabled = false;
            textBoxProxyServer.Enabled = false;
            textBoxProxyUserName.Enabled = false;
            textBoxProxyPassword.Enabled = false;
        }

        private void updateBuildingQueueGUI()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (flowLayoutPanelBuildings.Controls.Count != 0)
                {
                    for (int i = 0; i < m_Controller.Player.Towns.Count; i++)
                    {
                        if (flowLayoutPanelBuildings.InvokeRequired)
                        {
                            flowLayoutPanelBuildings.Invoke(new MethodInvoker(delegate
                            {
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).QueueEnabled = m_Controller.Player.Towns[i].BuildingQueueEnabled;
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).UseTargetLevel = m_Controller.Player.Towns[i].BuildingLevelsTargetEnabled;
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingNames(m_Controller.Player.Towns[i].getBuildingNames());
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingLevelsTarget(m_Controller.Player.Towns[i].getBuildingLevelsTarget());
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).LevelTargetColor = l_Settings.GUIBuildingLevelTargetColor;
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingLevels(m_Controller.Player.Towns[i].getBuildingLevels());//Call this method after setBuildingLevelsTarget
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingQueueBot(m_Controller.Player.Towns[i].getBuildingQueue());
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingQueueGame(m_Controller.Player.Towns[i].getIngameBuildingQueue());
                                ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setResources(m_Controller.Player.Towns[i].getResourcesString());
                            }));
                        }
                        else
                        {
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).QueueEnabled = m_Controller.Player.Towns[i].BuildingQueueEnabled;
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).UseTargetLevel = m_Controller.Player.Towns[i].BuildingLevelsTargetEnabled;
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingNames(m_Controller.Player.Towns[i].getBuildingNames());
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingLevelsTarget(m_Controller.Player.Towns[i].getBuildingLevelsTarget());
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).LevelTargetColor = l_Settings.GUIBuildingLevelTargetColor;
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingLevels(m_Controller.Player.Towns[i].getBuildingLevels());//Call this method after setBuildingLevelsTarget
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingQueueBot(m_Controller.Player.Towns[i].getBuildingQueue());
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setBuildingQueueGame(m_Controller.Player.Towns[i].getIngameBuildingQueue());
                            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).setResources(m_Controller.Player.Towns[i].getResourcesString());
                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateBuildingQueueGUI(): " + e.Message);
            }
        }

        private void updateUnitQueueGUI()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (flowLayoutPanelUnits.Controls.Count != 0)
                {
                    for (int i = 0; i < m_Controller.Player.Towns.Count; i++)
                    {
                        if (flowLayoutPanelUnits.InvokeRequired)
                        {
                            flowLayoutPanelUnits.Invoke(new MethodInvoker(delegate
                            {
                                ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).QueueEnabled = m_Controller.Player.Towns[i].UnitQueueEnabled;
                                ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setTotalUnits(m_Controller.Player.Towns[i].getTotalUnits());
                                ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setUnitQueue(m_Controller.Player.Towns[i].getUnitQueue());
                                ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).Population = m_Controller.Player.Towns[i].Population.ToString();
                                ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setResources(m_Controller.Player.Towns[i].getResourcesString());
                                ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setFavor(m_Controller.Player.getFavorByTownIndex(i));
                                ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setGod(m_Controller.Player.Towns[i].God);
                            }));
                        }
                        else
                        {
                            ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).QueueEnabled = m_Controller.Player.Towns[i].UnitQueueEnabled;
                            ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setTotalUnits(m_Controller.Player.Towns[i].getTotalUnits());
                            ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setUnitQueue(m_Controller.Player.Towns[i].getUnitQueue());
                            ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).Population = m_Controller.Player.Towns[i].Population.ToString();
                            ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setResources(m_Controller.Player.Towns[i].getResourcesString());
                            ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setFavor(m_Controller.Player.getFavorByTownIndex(i));
                            ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).setGod(m_Controller.Player.Towns[i].God);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateUnitQueueGUI(): " + e.Message);
            }
        }

        private void updateCulturalGUI()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (labelCulturalLevel.InvokeRequired)
                    labelCulturalLevel.Invoke(new MethodInvoker(delegate { labelCulturalLevel.Text = m_Controller.Player.CulturalLevelStr; }));
                else
                    labelCulturalLevel.Text = m_Controller.Player.CulturalLevelStr;
                if (labelCulturalCities.InvokeRequired)
                    labelCulturalCities.Invoke(new MethodInvoker(delegate { labelCulturalCities.Text = m_Controller.Player.CulturalCitiesStr; }));
                else
                    labelCulturalCities.Text = m_Controller.Player.CulturalCitiesStr;
                if (progressBarCP.InvokeRequired)
                    progressBarCP.Invoke(new MethodInvoker(delegate { progressBarCP.Maximum = m_Controller.Player.CulturalPointsMax; progressBarCP.Value = m_Controller.Player.CulturalPointsCurrent; }));
                else
                {
                    progressBarCP.Maximum = m_Controller.Player.CulturalPointsMax;
                    progressBarCP.Value = m_Controller.Player.CulturalPointsCurrent;
                }

                if (progressBarCP.InvokeRequired)
                    progressBarCP.Invoke(new MethodInvoker(delegate { m_Tooltip.SetToolTip(progressBarCP, m_Controller.Player.CulturalPointsStr); }));
                else
                    m_Tooltip.SetToolTip(progressBarCP, m_Controller.Player.CulturalPointsStr);
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateCulturalGUI(): " + e.Message);
            }
        }

        private void updateFarmersGUI()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (flowLayoutPanelFarmers.Controls.Count != 0)
                {
                    for (int i = 0; i < m_Controller.Player.Towns.Count; i++)
                    {

                        if (flowLayoutPanelFarmers.InvokeRequired)
                        {
                            flowLayoutPanelFarmers.Invoke(new MethodInvoker(delegate
                            {
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).LootingEnabled = m_Controller.Player.Towns[i].FarmersLootEnabled;
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).FriendlyDemandsOnly = m_Controller.Player.Towns[i].FarmersFriendlyDemandsOnly;
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setMinMood(m_Controller.Player.Towns[i].FarmersMinMood);
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setLootInterval(m_Controller.Player.Towns[i].FarmersLootInterval);
                                if (m_Controller.Player.Towns[i].Farmers.Count > 0)
                                {
                                    ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersName(m_Controller.Player.Towns[i].getFarmersName());
                                    ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersStatus(m_Controller.Player.Towns[i].getFarmersRelation(), m_Controller.Player.Towns[i].getFarmersLimitReached());
                                    ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersLootTime(m_Controller.Player.Towns[i].getFarmersLootTime());
                                    ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersMood(m_Controller.Player.Towns[i].getFarmersMood());
                                }
                                else
                                    ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).hideFarmers();
                            }));
                        }
                        else
                        {
                            ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).LootingEnabled = m_Controller.Player.Towns[i].FarmersLootEnabled;
                            ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).FriendlyDemandsOnly = m_Controller.Player.Towns[i].FarmersFriendlyDemandsOnly;
                            ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setMinMood(m_Controller.Player.Towns[i].FarmersMinMood);
                            ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setLootInterval(m_Controller.Player.Towns[i].FarmersLootInterval);
                            if (m_Controller.Player.Towns[i].Farmers.Count > 0)
                            {
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersName(m_Controller.Player.Towns[i].getFarmersName());
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersStatus(m_Controller.Player.Towns[i].getFarmersRelation(), m_Controller.Player.Towns[i].getFarmersLimitReached());
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersLootTime(m_Controller.Player.Towns[i].getFarmersLootTime());
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).setFarmersMood(m_Controller.Player.Towns[i].getFarmersMood());
                            }
                            else
                                ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).hideFarmers();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateFarmersGUI(): " + e.Message);
            }
        }

        private void updateNotificationGUI()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (textBoxNotifications.InvokeRequired)
                {
                    textBoxNotifications.Invoke(new MethodInvoker(delegate
                    {
                        textBoxNotifications.Text = m_Controller.Player.getNotifications(m_Controller.ServerTimerOffset);
                    }));
                }
                else
                {
                    textBoxNotifications.Text = m_Controller.Player.getNotifications(m_Controller.ServerTimerOffset);
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in updateNotificationGUI(): " + e.Message);
            }
        }

        private void setStatusBarCrossThread(object p_Message)
        {
            try
            {
                if (statusStrip1.InvokeRequired)
                {
                    SetStatusBarCallback l_Delegate = new SetStatusBarCallback(setStatusBar);
                    this.Invoke(l_Delegate, new object[] { (string)p_Message });
                }
                else
                {
                    setStatusBar(p_Message);
                }
            }
            catch (Exception)
            {
                //No statusbar update this time :(
            }
        }

        /*
         * Set a message on the statusbar
         */
        private void setStatusBar(object p_Message)
        {
            toolStripStatusLabel1.Text = p_Message.ToString();
        }

        private void updateFarmerGUICrossThread()
        {
            //todo updateFarmerGUICrossThread()
        }

        private void updateFarmerGUI()
        {
            //todo updateFarmerGUI()
        }

        private void setGuiToLoggedInStateCrossThread()
        {
            try
            {
                if (buttonTownLogin.InvokeRequired || buttonTownStartStop.InvokeRequired)
                {
                    SetGuiToLoggedInStateCallBack l_Delegate = new SetGuiToLoggedInStateCallBack(setGuiToLoggedInState);
                    this.Invoke(l_Delegate);
                }
                else
                {
                    setGuiToLoggedInState();
                }
            }
            catch (Exception)
            {
                
            }
        }

        private void setGuiToLoggedInState()
        {
            //IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            buttonTownLogin.Enabled = false;
            m_Controller.IsBotReadyToStart = true;
            buttonTownStartStop.Enabled = true;
            disableProxyGUI();

            //Sort towns by name on first run only
            if (flowLayoutPanelBuildings.Controls.Count == 0 || flowLayoutPanelUnits.Controls.Count == 0 || flowLayoutPanelFarmers.Controls.Count == 0)
            {
                m_Controller.Player.sortTownsByName();
                log("Towns sorted alphabetically.");
            }

            setStatusBar("Connected to " + l_Settings.GenUserName + "@" + l_Settings.GenServer);
            //Add GrebBuildings control at first time only
            if (flowLayoutPanelBuildings.Controls.Count == 0)
            {
                for (int i = 0; i < m_Controller.Player.Towns.Count; i++)
                {
                    flowLayoutPanelBuildings.Controls.Add(new GrepBuildings.GrepBuildings(i.ToString(), m_Controller.Player.Towns[i].TownID, m_Controller.Player.Towns[i].Name, l_Settings.GUIBuildingTooltipsEnabled));
                    ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[i]).buildingQueueUpdated += new GrepBuildings.GrepBuildings.UpdateBuildingQueueHandler(GrepolisBotII_buildingQueueUpdated);
                }
            }
            //Add GrebUnits control at first time only
            if (flowLayoutPanelUnits.Controls.Count == 0)
            {
                for (int i = 0; i < m_Controller.Player.Towns.Count; i++)
                {
                    flowLayoutPanelUnits.Controls.Add(new GrepUnits.GrepUnits(i.ToString(), m_Controller.Player.Towns[i].TownID, m_Controller.Player.Towns[i].Name,m_Controller.Player.Towns[i].Population.ToString()));
                    ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[i]).unitQueueUpdated += new GrepUnits.GrepUnits.UpdateUnitQueueHandler(GrepolisBotII_unitQueueUpdated);
                }
            }
            //Add GrebFarmers control at first time only
            if (flowLayoutPanelFarmers.Controls.Count == 0)
            {
                for (int i = 0; i < m_Controller.Player.Towns.Count; i++)
                {
                    flowLayoutPanelFarmers.Controls.Add(new GrepFarmers.GrepFarmers(i.ToString(), m_Controller.Player.Towns[i].TownID, m_Controller.Player.Towns[i].Name));
                    ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[i]).farmersLootingUpdated += new GrepFarmers.GrepFarmers.UpdateFarmersLootHandler(GrepolisBotII_farmersLootingUpdated);
                }
            }

            m_Controller.Player.loadTownsSettings();
            //Note Update all GUI's here that have town specific data (1/2)
            updateBuildingQueueGUI();
            updateUnitQueueGUI();
            updateCulturalGUI();
            updateFarmersGUI();
            updateNotificationGUI();

            //Set player name
            tabControl1.TabPages[0].Text = textBoxUserName.Text + " ("+ l_Settings.GenServer.Substring(0,l_Settings.GenServer.IndexOf(".")) +")";
            m_NotifyIcon.BalloonTipText = "Grepolis bot 2 - " + tabControl1.TabPages[0].Text;
            m_NotifyIcon.Text = "Grepolis bot 2 - " + tabControl1.TabPages[0].Text;

            if (l_Settings.GenAutoStart || m_Controller.IsBotRunningOnce)
            {
                m_Controller.startbot();
                buttonTownStartStop.Text = "Stop";
            }
            m_Controller.IsBotRunningOnce = true;
            m_Controller.startConnectedTimer();
        }

        private void setGuiToTimeoutProcessedStateCrossThread()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                setGuiToTimeoutProcessedState();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in setGuiToTimeoutProcessedStateCrossThread(): " + e.Message);
            }
        }

        /*
         * Bot is done updating his data.
         */ 
        private void setGuiToTimeoutProcessedState()
        {
            //Note Update all GUI's here that have town specific data (2/2)
            updateBuildingQueueGUI();
            updateUnitQueueGUI();
            updateCulturalGUI();
            updateFarmersGUI();
            updateNotificationGUI();

            if (m_Controller.IsBotRunning || m_Controller.IsBotRunningOnce)
            {
                if (!m_Controller.RequestedToStopBot)
                {
                    m_Controller.resumeTimers();
                    toolStripStatusLabel1.Text = "Waiting";
                }
                else
                {
                    m_Controller.RequestedToStopBot = false;
                    toolStripStatusLabel1.Text = "Stopped";
                }
            }
        }

        private void updateGUIGodCrossThread()
        {
            //todo updateGUIGodCrossThread()
        }

        private void updateGUIGod()
        {
            //todo updateGUIGod()
        }

        private void updateRefreshTimerCrossThread(object p_TimeLeft)
        {
            try
            {
                if (labelNextUpdateTimeLeft.InvokeRequired || statusStrip1.InvokeRequired)
                {
                    UpdateRefreshTimerCallBack l_Delegate = new UpdateRefreshTimerCallBack(updateRefreshTimer);
                    this.Invoke(l_Delegate, new object[] { (string)p_TimeLeft });
                }
                else
                {
                    updateRefreshTimer(p_TimeLeft);
                }
            }
            catch (Exception)
            {
                //No timer update this time :(
            }
        }

        private void updateRefreshTimer(object p_TimeLeft)
        {
            if (p_TimeLeft.ToString().Contains("-"))
            {
                labelNextUpdateTimeLeft.Text = "00:00:00";
                toolStripStatusLabel1.Text = "Next update: 00:00:00";
            }
            else
            {
                labelNextUpdateTimeLeft.Text = p_TimeLeft.ToString();
                toolStripStatusLabel1.Text = "Next update: " + p_TimeLeft;
            }
        }

        private void updateQueueTimerCrossThread(object p_TimeLeft)
        {
            try
            {
                if (labelQueueTimeLeft.InvokeRequired)
                {
                    UpdateQueueTimerCallBack l_Delegate = new UpdateQueueTimerCallBack(updateQueueTimer);
                    this.Invoke(l_Delegate, new object[] { (string)p_TimeLeft });
                }
                else
                {
                    updateQueueTimer(p_TimeLeft);
                }
            }
            catch (Exception)
            {
                //No timer update this time :(
            }
        }

        private void updateQueueTimer(object p_TimeLeft)
        {
            if(p_TimeLeft.ToString().Contains("-"))
                labelQueueTimeLeft.Text = "00:00:00";
            else
                labelQueueTimeLeft.Text = p_TimeLeft.ToString();
        }

        private void updateReconnectTimerCrossThread(object p_TimeLeft)
        {
            try
            {
                if (labelReconnectTimeLeft.InvokeRequired || statusStrip1.InvokeRequired)
                {
                    UpdateReconnectTimerCallBack l_Delegate = new UpdateReconnectTimerCallBack(updateReconnectTimer);
                    this.Invoke(l_Delegate, new object[] { (string)p_TimeLeft });
                }
                else
                {
                    updateReconnectTimer(p_TimeLeft);
                }
            }
            catch (Exception)
            {
                //No timer update this time :(
            }
        }

        private void updateReconnectTimer(object p_TimeLeft)
        {
            if (p_TimeLeft.ToString().Contains("-"))
            {
                labelReconnectTimeLeft.Text = "00:00:00";
                toolStripStatusLabel1.Text = "Reconnect: 00:00:00";
            }
            else
            {
                labelReconnectTimeLeft.Text = p_TimeLeft.ToString();
                toolStripStatusLabel1.Text = "Reconnect: " + p_TimeLeft;
            }

            if (m_Controller.ReconnectTimer.isTimerDone())
            {
                m_Controller.ReconnectTimer.stop();
                login();
            }
            else
            {
                m_Controller.ReconnectTimer.InternalTimer.Start();
            }
        }

        private void updateConnectedTimerCrossThread(object p_TimeElapsed)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (labelConnectedTimer.InvokeRequired)
                {
                    UpdateConnectedTimerCallBack l_Delegate = new UpdateConnectedTimerCallBack(updateConnectedTimer);
                    this.Invoke(l_Delegate, new object[] { (string)p_TimeElapsed });
                }
                else
                {
                    updateConnectedTimer(p_TimeElapsed);
                }
            }
            catch (Exception)
            {
                //No timer update this time :(
                //if (l_Settings.AdvDebugMode)
                //   l_IOHandler.debug("Exception in updateConnectedTimerCrossThread(): " + ex.Message);
            }
        }

        private void updateConnectedTimer(object p_TimeElapsed)
        {
            labelConnectedTimer.Text = p_TimeElapsed.ToString();
        }

        private void updateBuildingQueueCrossThread(object p_Index)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (flowLayoutPanelBuildings.InvokeRequired)
                {
                    UpdateBuildingQueueCallBack l_Delegate = new UpdateBuildingQueueCallBack(updateBuildingQueue);
                    this.Invoke(l_Delegate, new object[] { (string)p_Index });
                }
                else
                {
                    updateBuildingQueue(p_Index);
                }
            }
            catch (Exception)
            {
                //Seems to always trow an invalid cast exception but the buildingqueue is updated fine because of setGuiToTimeoutProcessedState()
                //if (l_Settings.AdvDebugMode)
                //    l_IOHandler.debug("Exception in updateBuildingQueueCrossThread(): " + ex.Message);
            }
        }

        private void updateBuildingQueue(object p_Index)
        {
            IOHandler l_IOHandler = IOHandler.Instance;

            int l_Index = (int)p_Index;
            ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[l_Index]).removeItemFromQueueBotByIndex(0);
            l_IOHandler.saveTownsSettings(m_Controller.Player);
        }

        private void logCrossThread(object p_Message)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (textBoxLog.InvokeRequired)
                {
                    LogCallBack l_Delegate = new LogCallBack(log);
                    this.Invoke(l_Delegate, new object[] { (string)p_Message });
                }
                else
                {
                    log(p_Message);
                }
            }
            catch (Exception ex)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in logCrossThread(): " + ex.Message);
            }
        }

        private void log(object p_Message)
        {
            try
            {
                int l_Length = 0;
                string l_Message = DateTime.Now.ToLocalTime() + " " + p_Message.ToString();

                textBoxLog.Text = l_Message + Environment.NewLine + textBoxLog.Text;
                groupBoxLog.Text = "Log(" + textBoxLog.Lines.Length + ")";

                if (textBoxLog.Lines.Length > 500)
                {
                    for (int i = 501; i < textBoxLog.Lines.Length; i++)
                    {
                        l_Length += textBoxLog.Lines[i].Length;
                    }
                    textBoxLog.Text = textBoxLog.Text.Remove(textBoxLog.Text.Length - l_Length);
                    textBoxLog.Lines[textBoxLog.Lines.Length - 1].Remove(0);
                }
            }
            catch (Exception)
            {
                //No action required
            }
        }

        private void versionInfoCrossThread(object p_VersionInfo)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                if (linkLabelLatestVersion.InvokeRequired)
                {
                    VersionInfoCallBack l_Delegate = new VersionInfoCallBack(versionInfo);
                    this.Invoke(l_Delegate, new object[] { (string)p_VersionInfo });
                }
                else
                {
                    versionInfo(p_VersionInfo);
                }
            }
            catch (Exception ex)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in versionInfoCrossThread(): " + ex.Message);
            }
        }

        private void versionInfo(object p_VersionInfo)
        {
            linkLabelLatestVersion.Text = "Latest version: " + p_VersionInfo.ToString();
        }

        private void serverRequestDelayRequestCrossThread()
        {
            serverRequestDelayRequestCallBack l_Delegate = new serverRequestDelayRequestCallBack(serverRequestDelayRequest);
            this.Invoke(l_Delegate);
        }

        private void serverRequestDelayRequest()
        {
            m_Controller.RequestTimer.Start();
        }

//-->Event Handlers

        //Settings tab
        private void buttonSettingsOK_Click(object sender, EventArgs e)
        {
            saveSettings();
            updateBuildingQueueGUI();
        }

        private void buttonSettingsCancel_Click(object sender, EventArgs e)
        {
            loadSettings();
        }

        private void labelGUIBuildingLevelTargetColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                labelGUIBuildingLevelTargetColor.BackColor = colorDialog1.Color;
            }
        }
        //~Settings tab

        //Player tab
        private void buttonTownLogin_Click(object sender, EventArgs e)
        {
            //m_Controller.login();
            login();
        }

        private void buttonTownStartStop_Click(object sender, EventArgs e)
        {
            if (buttonTownStartStop.Text.Equals("Start"))
            {
                if (m_Controller.LoggedIn && m_Controller.IsBotReadyToStart && !m_Controller.RequestedToStopBot)
                {
                    m_Controller.startbot();
                    buttonTownStartStop.Text = "Stop";
                }
            }
            else
            {
                m_Controller.stop();
                buttonTownStartStop.Text = "Start";
            }
             
        }

        private void textBoxNotes_MouseLeave(object sender, EventArgs e)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            l_IOHandler.saveNotes(textBoxNotes.Text);
        }

        //~Player tab

        //Buildings tab
        private void GrepolisBotII_buildingQueueUpdated(object sender, GrepBuildings.GrepBuildingsArgs ca)
        {
            //MessageBox.Show(ca.getMessage());

            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].BuildingQueueEnabled = ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[int.Parse(ca.getMessage())]).QueueEnabled;
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].BuildingLevelsTargetEnabled = ((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[int.Parse(ca.getMessage())]).UseTargetLevel;
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].setBuildingsLevelTarget(((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[int.Parse(ca.getMessage())]).getBuildingLevelsTarget());
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].setBuildingQueue(((GrepBuildings.GrepBuildings)flowLayoutPanelBuildings.Controls[int.Parse(ca.getMessage())]).getBuildingQueue());

                l_IOHandler.saveTownsSettings(m_Controller.Player);
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in GrepolisBotII_buildingQueueUpdated(): " + e.Message);
            }
        }
        //~Buildings tab

        //Units tab
        private void GrepolisBotII_unitQueueUpdated(object sender, GrepUnits.GrepUnitsArgs ca)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].UnitQueueEnabled = ((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[int.Parse(ca.getMessage())]).QueueEnabled;
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].setUnitQueue(((GrepUnits.GrepUnits)flowLayoutPanelUnits.Controls[int.Parse(ca.getMessage())]).getUnitQueue());

                l_IOHandler.saveTownsSettings(m_Controller.Player);
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in GrepolisBotII_unitQueueUpdated(): " + e.Message);
            }
        }
        //~Units tab

        //Farmers tab
        private void GrepolisBotII_farmersLootingUpdated(object sender, GrepFarmers.GrepFarmersArgs ca)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].FarmersLootEnabled = ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[int.Parse(ca.getMessage())]).LootingEnabled;
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].FarmersFriendlyDemandsOnly = ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[int.Parse(ca.getMessage())]).FriendlyDemandsOnly;
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].FarmersMinMood = ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[int.Parse(ca.getMessage())]).MinMood;
                m_Controller.Player.Towns[int.Parse(ca.getMessage())].FarmersLootInterval = ((GrepFarmers.GrepFarmers)flowLayoutPanelFarmers.Controls[int.Parse(ca.getMessage())]).LootInterval;

                l_IOHandler.saveTownsSettings(m_Controller.Player);
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in GrepolisBotII_farmersLootingUpdated(): " + e.Message);
            }
        }
        //~Farmers tab

        //Cultural tab
        /*
         * 
         * 
         * 
         */
        //~Cultural tab

        //Browser tab
        private void webBrowserGrepo_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            if (m_Controller.State.Equals("login"))
            {
                if (webBrowserGrepo.Document.Url.AbsoluteUri.EndsWith("login=1"))
                {
                    m_Controller.setCookies(webBrowserGrepo.Document.Cookie);
                    string l_Response = webBrowserGrepo.DocumentText;
                    string l_Search = "";
                    int l_Index = -1;
                    int l_IndexF = -1;

                    if (l_Response.Contains("Game.townId"))
                    {
                        m_Controller.LoggedIn = true;
                        m_Controller.LoggedInOnce = true;
                        //Get csrfToken
                        l_Search = "Game.csrfToken = '";
                        l_Index = l_Response.IndexOf(l_Search, 0);
                        m_Controller.H = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf("'", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        //Get town id
                        l_Search = "Game.townId = '";
                        l_Index = l_Response.IndexOf(l_Search, l_Index);
                        m_Controller.Player.DefaultTownID = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf("'", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        //Get player id
                        l_Search = "Game.player_id = '";
                        l_Index = l_Response.IndexOf(l_Search, l_Index);
                        m_Controller.Player.PlayerID = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf("'", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        //Get server time
                        l_Search = "Game.server_time = '";
                        l_Index = l_Response.IndexOf(l_Search, l_Index);
                        m_Controller.ServerTimer = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf("'", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                        //Get server time offset
                        l_Search = "Game.server_gmt_offset = '";
                        l_Index = l_Response.IndexOf(l_Search, l_Index);
                        m_Controller.ServerTimerOffset = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf("'", l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                        //Get towns
                        string l_TownID = "";
                        string l_TownName = "";
                        string l_IslandX = "000";
                        string l_IslandY = "000";

                        l_Search = "ITowns.initialize({";
                        l_Index = l_Response.IndexOf(l_Search, l_Index);
                        l_Search = "\"towns\":[{";
                        l_Index = l_Response.IndexOf(l_Search, l_Index);
                        l_Search = "}]";
                        l_IndexF = l_Response.IndexOf(l_Search, l_Index);
                        l_Search = "\"id\":";
                        l_Index = l_Index = l_Response.IndexOf(l_Search, l_Index);
                        while (l_Index < l_IndexF)
                        {
                            l_TownID = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"name\":\"";
                            l_Index = l_Index = l_Response.IndexOf(l_Search, l_Index);
                            l_TownName = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf("\",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_TownName = m_Controller.fixSpecialCharacters(l_TownName);

                            l_Search = "\"island_x\":";
                            l_Index = l_Index = l_Response.IndexOf(l_Search, l_Index);
                            l_IslandX = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));
                            l_Search = "\"island_y\":";
                            l_Index = l_Index = l_Response.IndexOf(l_Search, l_Index);
                            l_IslandY = l_Response.Substring(l_Index + l_Search.Length, l_Response.IndexOf(",", l_Index + l_Search.Length) - (l_Index + l_Search.Length));

                            //Remove non-numeric characters from y-coord
                            while (!Char.IsNumber(l_IslandY[l_IslandY.Length - 1]))
                                l_IslandY = l_IslandY.Remove(l_IslandY.Length - 1);

                            if (m_Controller.Player.isUniqueTown(l_TownID))
                                m_Controller.Player.Towns.Add(new Town(l_TownID, l_TownName, l_IslandX, l_IslandY));

                            l_Search = "\"id\":";
                            l_Index = l_Index = l_Response.IndexOf(l_Search, l_Index);
                        }
                        m_Controller.locateFarmers();
                    }
                    else
                    {
                        if (m_Controller.LoggedInOnce)
                            m_Controller.startReconnect();
                        else
                            setStatusBar("Can't connect, check your settings");

                        if (l_Settings.AdvDebugMode)
                        {
                            l_IOHandler.saveServerResponse("loginResponse", l_Response);
                            l_IOHandler.debug("Critical error occurred. Server response saved in Response dir. (LoginResponse)");
                        }

                        log("Logged in. Can't find required information. See Response directory.");
                    }
                }
                else
                {
                    if (m_Controller.LoggedInOnce)
                        m_Controller.startReconnect();
                    else
                    {
                        setStatusBar("Can't connect, check your settings");
                        MessageBox.Show("Can't connect, check your settings");
                    }

                    if (l_Settings.AdvDebugMode)
                        l_IOHandler.debug("Login error in webBrowserGrepo_DocumentCompleted(): " + webBrowserGrepo.Document.Url.AbsoluteUri);
                    log("Can't login. Problem with url. See debug.txt.");
                }
            }
            else
            {
                m_Controller.setCookies(webBrowserGrepo.Document.Cookie);
            }
        }
        //~Browser tab

        //Form
        private void m_NotifyIcon_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void GrepolisBotII_Resize(object sender, EventArgs e)
        {
            Settings l_Settings = Settings.Instance;

            if (this.WindowState == FormWindowState.Minimized && l_Settings.GenToTryIcon == true)
            {
                this.ShowInTaskbar = false;
            }
        }

        private void labelReload_DoubleClick(object sender, EventArgs e)
        {
            m_Controller.checkVersion();
        }

        private void linkLabelLatestVersion_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Determine which link was clicked within the LinkLabel.
            linkLabelLatestVersion.Links[linkLabelLatestVersion.Links.IndexOf(e.Link)].Visited = true;
            // Display the appropriate link based on the value of the 
            // LinkData property of the Link object.
            String l_Target = e.Link.LinkData as String;
            System.Diagnostics.Process.Start(l_Target);
        }

        private void buttonRestoreGUI_Click(object sender, EventArgs e)
        {
            IOHandler l_IOHandler = IOHandler.Instance;

            this.Width = 1085;
            this.Height = 620;
        }
        //~Form

        //Controller class
        private void m_Controller_statusBarUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(setStatusBarCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_farmerGUIUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ThreadStart(updateFarmerGUICrossThread)).Start();
        }

        private void m_Controller_loggedInStateChanged(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ThreadStart(setGuiToLoggedInStateCrossThread)).Start();
        }

        private void m_Controller_timeoutProcessedStateChanged(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ThreadStart(setGuiToTimeoutProcessedStateCrossThread)).Start();
        }

        private void m_Controller_godGUIUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ThreadStart(updateGUIGodCrossThread)).Start();
        }

        private void m_Controller_refreshTimerUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(updateRefreshTimerCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_queueTimerUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(updateQueueTimerCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_reconnectTimerUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(updateReconnectTimerCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_connectedTimerUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(updateConnectedTimerCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_buildingQueueUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(updateBuildingQueueCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_logUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(logCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_versionInfoUpdated(object sender, CustomArgs ca)
        {
            new Thread(new ParameterizedThreadStart(versionInfoCrossThread)).Start(ca.getMessage());
        }

        private void m_Controller_serverRequestDelayRequested(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ThreadStart(serverRequestDelayRequestCrossThread)).Start();
        }

        //~Controller class
    }
}
