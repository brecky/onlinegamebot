﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrepolisBot2
{
    class Town
    {
        //General
        private string m_TownID = "";
        private string m_Name = "";
        private string m_God = "none";//none, zeus, poseidon, hera, athena, hades. 
        private string m_IslandX = "000";
        private string m_IslandY = "000";

        //Building Queue
        private string[] m_IngameBuildingQueue = { "", "", "", "", "", "", "" };
        private Queue<string> m_BuildingQueue = new Queue<string>();
        private bool m_BuildingQueueEnabled = true;
        private bool m_BuildingLevelsTargetEnabled = false;

        //Unit Queue
        private bool m_UnitQueueEnabled = true;
        private int m_SizeOfLandUnitQueue = 0;
        private int m_SizeOfNavyUnitQueue = 0;

        //Farmers
        private bool m_FarmersLootEnabled = false;
        private bool m_FarmersFriendlyDemandsOnly = false;
        private int m_FarmersMinMood = 80;
        private string m_FarmersLootInterval = "5";

        //Resources
        private int m_Wood = 0;
        private int m_Stone = 0;
        private int m_Iron = 0;
        private int m_Storage = 0;
        private int m_WoodProduction = 0;//Not yet extracted from html source
        private int m_StoneProduction = 0;//Not yet extracted from html source
        private int m_IronProduction = 0;//Not yet extracted from html source
        private string m_ResourcePlenty = "";
        private string m_ResourceRare = "";

        private int m_Population = 0;
        private string m_HasConquerer = "0";
        private int incomingAttacks = 0;

        //Custom classes
        private List<Farmer> m_Farmers = new List<Farmer>();
        private List<ArmyUnit> m_ArmyUnits = new List<ArmyUnit>();
        private List<Building> m_Buildings = new List<Building>();

        

//-->Constructors
        
        public Town()
        {
            
        }

        public Town(string p_TownID, string p_TownName, string p_IslandX, string p_IslandY)
        {
            m_TownID = p_TownID;
            m_Name = p_TownName;
            m_IslandX = p_IslandX;
            m_IslandY = p_IslandY;
            addArmyUnits();
            addBuildings();
        }

//-->Attributes
        
        //General
        public string TownID
        {
            get { return m_TownID; }
            set { m_TownID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public String God
        {
            get { return m_God; }
            set { m_God = value; }
        }

        public string IslandX
        {
            get { return m_IslandX; }
            set { m_IslandX = value; }
        }

        public string IslandY
        {
            get { return m_IslandY; }
            set { m_IslandY = value; }
        }

        //Building Queue
        public String[] IngameBuildingQueue
        {
            get { return m_IngameBuildingQueue; }
            set { m_IngameBuildingQueue = value; }
        }

        public Queue<string> BuildingQueue
        {
            get { return m_BuildingQueue; }
            set { m_BuildingQueue = value; }
        }

        public bool BuildingQueueEnabled
        {
            get { return m_BuildingQueueEnabled; }
            set { m_BuildingQueueEnabled = value; }
        }

        public bool BuildingLevelsTargetEnabled
        {
            get { return m_BuildingLevelsTargetEnabled; }
            set { m_BuildingLevelsTargetEnabled = value; }
        }

        public bool UnitQueueEnabled
        {
            get { return m_UnitQueueEnabled; }
            set { m_UnitQueueEnabled = value; }
        }

        public int SizeOfLandUnitQueue
        {
            get { return m_SizeOfLandUnitQueue; }
            set { m_SizeOfLandUnitQueue = value; }
        }

        public int SizeOfNavyUnitQueue
        {
            get { return m_SizeOfNavyUnitQueue; }
            set { m_SizeOfNavyUnitQueue = value; }
        }

        //Farmers
        public bool FarmersLootEnabled
        {
            get { return m_FarmersLootEnabled; }
            set { m_FarmersLootEnabled = value; }
        }

        public bool FarmersFriendlyDemandsOnly
        {
            get { return m_FarmersFriendlyDemandsOnly; }
            set { m_FarmersFriendlyDemandsOnly = value; }
        }

        public int FarmersMinMood
        {
            get { return m_FarmersMinMood; }
            set { m_FarmersMinMood = value; }
        }

        public string FarmersLootInterval
        {
            get { return m_FarmersLootInterval; }
            set { m_FarmersLootInterval = value; }
        }

        //Resources
        public int Wood
        {
            get { return m_Wood; }
            set { m_Wood = value; }
        }

        public int Stone
        {
            get { return m_Stone; }
            set { m_Stone = value; }
        }

        public int Iron
        {
            get { return m_Iron; }
            set { m_Iron = value; }
        }

        public int Storage
        {
            get { return m_Storage; }
            set { m_Storage = value; }
        }

        public int WoodProduction
        {
            get { return m_WoodProduction; }
            set { m_WoodProduction = value; }
        }

        public int StoneProduction
        {
            get { return m_StoneProduction; }
            set { m_StoneProduction = value; }
        }

        public int IronProduction
        {
            get { return m_IronProduction; }
            set { m_IronProduction = value; }
        }

        public string ResourcePlenty
        {
            get { return m_ResourcePlenty; }
            set { m_ResourcePlenty = value; }
        }

        public string ResourceRare
        {
            get { return m_ResourceRare; }
            set { m_ResourceRare = value; }
        }

        public int Population
        {
            get { return m_Population; }
            set { m_Population = value; }
        }

        public string HasConquerer
        {
            get { return m_HasConquerer; }
            set { m_HasConquerer = value; }
        }

        public int IncomingAttacks
        {
            get { return incomingAttacks; }
            set { incomingAttacks = value; }
        }

        public List<Farmer> Farmers
        {
            get { return m_Farmers; }
            set { m_Farmers = value; }
        }

        public List<ArmyUnit> ArmyUnits
        {
            get { return m_ArmyUnits; }
            set { m_ArmyUnits = value; }
        }

        public List<Building> Buildings
        {
            get { return m_Buildings; }
            set { m_Buildings = value; }
        }

//-->Methods

        public override string ToString()
        {
            return m_Name;
        }
        private void addBuildings()
        {
            m_Buildings.Add(new Building("main", 25));
            m_Buildings.Add(new Building("hide", 10));
            m_Buildings.Add(new Building("storage", 30));
            m_Buildings.Add(new Building("farm", 40));
            m_Buildings.Add(new Building("lumber", 40));
            m_Buildings.Add(new Building("stoner", 40));
            m_Buildings.Add(new Building("ironer", 40));
            m_Buildings.Add(new Building("market", 30));
            m_Buildings.Add(new Building("docks", 30));
            m_Buildings.Add(new Building("barracks", 30));
            m_Buildings.Add(new Building("wall", 25));
            m_Buildings.Add(new Building("academy", 30));
            m_Buildings.Add(new Building("temple", 25));
            m_Buildings.Add(new Building("theater", 1));
            m_Buildings.Add(new Building("thermal", 1));
            m_Buildings.Add(new Building("library", 1));
            m_Buildings.Add(new Building("lighthouse", 1));
            m_Buildings.Add(new Building("tower", 1));
            m_Buildings.Add(new Building("statue", 1));
            m_Buildings.Add(new Building("oracle", 1));
            m_Buildings.Add(new Building("trade_office", 1));
        }

        private void addArmyUnits()
        {
            m_ArmyUnits.Add(new ArmyUnit("sword"));
            m_ArmyUnits.Add(new ArmyUnit("slinger"));
            m_ArmyUnits.Add(new ArmyUnit("archer"));
            m_ArmyUnits.Add(new ArmyUnit("hoplite"));
            m_ArmyUnits.Add(new ArmyUnit("rider"));
            m_ArmyUnits.Add(new ArmyUnit("chariot"));
            m_ArmyUnits.Add(new ArmyUnit("catapult"));
            m_ArmyUnits.Add(new ArmyUnit("minotaur"));//(7)Zeus
            m_ArmyUnits.Add(new ArmyUnit("manticore"));//(8)Zeus
            m_ArmyUnits.Add(new ArmyUnit("centaur"));//(9)Athena
            m_ArmyUnits.Add(new ArmyUnit("pegasus"));//(10)Athena
            m_ArmyUnits.Add(new ArmyUnit("harpy"));//(11)Hera
            m_ArmyUnits.Add(new ArmyUnit("medusa"));//(12)Hera
            m_ArmyUnits.Add(new ArmyUnit("zyklop"));//(13)Poseidon
            m_ArmyUnits.Add(new ArmyUnit("cerberus"));//(14)Hades
            m_ArmyUnits.Add(new ArmyUnit("fury"));//(15)Hades
            m_ArmyUnits.Add(new ArmyUnit("big_transporter"));
            m_ArmyUnits.Add(new ArmyUnit("bireme"));
            m_ArmyUnits.Add(new ArmyUnit("attack_ship"));
            m_ArmyUnits.Add(new ArmyUnit("demolition_ship"));
            m_ArmyUnits.Add(new ArmyUnit("small_transporter"));
            m_ArmyUnits.Add(new ArmyUnit("trireme"));
            m_ArmyUnits.Add(new ArmyUnit("colonize_ship"));
            m_ArmyUnits.Add(new ArmyUnit("sea_monster"));//(23)Poseidon
        }

        public void resetDailyFarmLimit()
        {
            for (int i = 0; i < Farmers.Count; i++)
            {
                Farmers[i].FarmersLimitReached = false;
            }
        }

        public bool isUniqueFarm(String p_ID)
        {
            bool l_IsUnique = true;
            for (int i = 0; i < m_Farmers.Count; i++)
            {
                if (m_Farmers[i].ID == p_ID)
                    l_IsUnique = false;
            }
            return l_IsUnique;
        }

        public int getFarmersIndex(String p_ID)
        {
            int l_Index = -1;
            for (int i = 0; i < m_Farmers.Count; i++)
            {
                if (m_Farmers[i].ID == p_ID)
                {
                    l_Index = i;
                    break;
                }
            }
            return l_Index;
        }

        public void resetArmyCurrentAmount()
        {
            for (int i = 0; i < m_ArmyUnits.Count; i++)
            {
                m_ArmyUnits[i].CurrentAmount = 0;
            }
        }

        public int getIndexArmyUnit(string p_Name)
        {
            int l_Index = -1;
            for (int i = 0; i < m_ArmyUnits.Count; i++)
            {
                if (m_ArmyUnits[i].Name == p_Name)
                    l_Index = i;
            }
            return l_Index;
        }

        public int getIndexBuilding(string p_Name)
        {
            int l_Index = -1;
            for (int i = 0; i < m_Buildings.Count; i++)
            {
                if (m_Buildings[i].DevName == p_Name)
                    l_Index = i;
            }
            return l_Index;
        }

        public void setIngameBuildingQueue(string p_Queue)
        {
            string[] l_Buildings = new string[] { "", "" };//dynamic array
            l_Buildings = p_Queue.Split(';');
            for (int i = 0; i < m_IngameBuildingQueue.Length; i++)
            {
                if (i < l_Buildings.Length)
                    m_IngameBuildingQueue[i] = l_Buildings[i];
                else
                    m_IngameBuildingQueue[i] = "";
            }
        }

        public string getIngameBuildingQueue()
        {
            string l_IngameBuildingQueue = "";

            for (int i = 0; i < m_IngameBuildingQueue.Length; i++)
            {
                if (m_IngameBuildingQueue[i].Length > 0)
                    l_IngameBuildingQueue += m_IngameBuildingQueue[i] + ";";
                else
                    break;
            }

            return l_IngameBuildingQueue;
        }

        public int countIngameBuildingQueueByName(string p_Building)
        {
            int l_Count = 0;

            for (int i = 0; i < m_IngameBuildingQueue.Length; i++)
            {
                if (m_IngameBuildingQueue[i].Equals(p_Building))
                    l_Count++;
            }

            return l_Count;
        }

        public string getBuildingLevelsTarget()
        {
            string l_BuildingLevelsTarget = "";

            for (int i = 0; i < m_Buildings.Count; i++)
            {
                l_BuildingLevelsTarget += m_Buildings[i].TargetLevel.ToString() + ";";
            }

            return l_BuildingLevelsTarget;
        }

        /*
         * Return all the buildings in the queue as a string.
         */
        public string getBuildingQueue()
        {
            string l_Queue = "";
            // A queue can be enumerated without disturbing its contents.
            foreach (string l_Building in m_BuildingQueue)
            {
                l_Queue += l_Building;
                l_Queue += ";";
            }
            return l_Queue;
        }

        /*
         * Look at first building in queue without removing it.
         */
        public string peekQueueBuilding()
        {
            return m_BuildingQueue.Peek();
        }

        /*
         * Look at first building in queue and remove it.
         */
        public string dequeueBuilding()
        {
            return m_BuildingQueue.Dequeue();
        }


        public void setBuildingsLevelTarget(string p_BuildingsLevelTarget)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string[] l_BuildingsLevelsTarget = new string[] { "", "" };//dynamic array
                l_BuildingsLevelsTarget = p_BuildingsLevelTarget.Split(';');

                for (int i = 0; i < m_Buildings.Count; i++)
                {
                    m_Buildings[i].TargetLevel = int.Parse(l_BuildingsLevelsTarget[i]);
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in setBuildingsLevelTarget(): " + e.Message);
            }
        }

        public void setBuildingQueue(string p_BuildingQueue)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string[] l_BuildingQueue = new string[] { "", "" };//dynamic array
                l_BuildingQueue = p_BuildingQueue.Split(';');
                m_BuildingQueue.Clear();

                for (int i = 0; i < l_BuildingQueue.Length; i++)
                {
                    if (l_BuildingQueue[i].Length > 0)
                        m_BuildingQueue.Enqueue(l_BuildingQueue[i]);
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in setBuildingsLevelTarget(): " + e.Message);
            }
        }

        public string getBuildingLevels()
        {
            string l_BuildingLevels = "";

            for (int i = 0; i < m_Buildings.Count; i++)
            {
                l_BuildingLevels += m_Buildings[i].Level + ";";
            }

            return l_BuildingLevels;
        }

        public string getBuildingNames()
        {
            string l_BuildingNames = "";

            for (int i = 0; i < m_Buildings.Count; i++)
            {
                l_BuildingNames += m_Buildings[i].LocalName + ";";
            }

            return l_BuildingNames;
        }

        public void setUnitQueue(string p_TrainingTarget)
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            try
            {
                string[] l_TrainingTarget = new string[] { "", "" };//dynamic array
                l_TrainingTarget = p_TrainingTarget.Split(';');

                for (int i = 0; i < m_ArmyUnits.Count; i++)
                {
                    m_ArmyUnits[i].QueueBot = int.Parse(l_TrainingTarget[i]);
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in setBuildingsLevelTarget(): " + e.Message);
            }
        }

        public string getUnitQueue()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            string l_TrainingTarget = "";

            try
            {
                for (int i = 0; i < m_ArmyUnits.Count; i++)
                {
                    l_TrainingTarget += m_ArmyUnits[i].QueueBot + ";";
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in getUnitTrainingTarget(): " + e.Message);
            }
            
            return l_TrainingTarget;
        }

        public string getTotalUnits()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            string l_Units = "";

            try
            {
                for (int i = 0; i < m_ArmyUnits.Count; i++)
                {
                    l_Units += m_ArmyUnits[i].TotalAmount + ";";
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in getCurrentUnits(): " + e.Message);
            }

            return l_Units;
        }

        /*public int getFirstLandUnitInQueue()
        {
            //0 tm 15
            int l_UnitIndex = -1;
            for(int i = 0; i < 16; i++)
            {
                if (m_ArmyUnits[i].QueueBot > 0)
                {
                    if (m_ArmyUnits[i].QueueBot - (m_ArmyUnits[i].TotalAmount + m_ArmyUnits[i].QueueGame) > 0)
                    {
                        l_UnitIndex = i;
                        break;
                    }
                }
            }
            return l_UnitIndex;
        }*/

        public int getMostTrainableLandUnitInQueue()
        {
            //0 tm 15
            int l_UnitIndex = -1;
            int l_Trainable = -1;

            for (int i = 0; i < 16; i++)
            {
                if (m_ArmyUnits[i].QueueBot > 0)
                {
                    if (m_ArmyUnits[i].QueueBot - (m_ArmyUnits[i].TotalAmount + m_ArmyUnits[i].QueueGame) > 0 && m_ArmyUnits[i].MaxBuild > l_Trainable)
                    {
                        l_UnitIndex = i;
                        l_Trainable = m_ArmyUnits[i].MaxBuild;
                    }
                }
            }
            return l_UnitIndex;
        }

        /*public int getFirstNavyUnitInQueue()
        {
            //16 tm 23
            int l_UnitIndex = -1;
            for (int i = 16; i < 24; i++)
            {
                if (m_ArmyUnits[i].QueueBot > 0)
                {
                    if (m_ArmyUnits[i].QueueBot - (m_ArmyUnits[i].TotalAmount + m_ArmyUnits[i].QueueGame) > 0)
                    {
                        l_UnitIndex = i;
                        break;
                    }
                }
            }
            return l_UnitIndex;
        }*/

        public int getMostTrainableNavyUnitInQueue()
        {
            //16 tm 23
            int l_UnitIndex = -1;
            int l_Trainable = -1;

            for (int i = 16; i < 24; i++)
            {
                if (m_ArmyUnits[i].QueueBot > 0)
                {
                    if (m_ArmyUnits[i].QueueBot - (m_ArmyUnits[i].TotalAmount + m_ArmyUnits[i].QueueGame) > 0 && m_ArmyUnits[i].MaxBuild > l_Trainable)
                    {
                        l_UnitIndex = i;
                        l_Trainable = m_ArmyUnits[i].MaxBuild;
                    }
                }
            }
            return l_UnitIndex;
        }

        public string getFarmersName()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            string l_FarmersName = "";

            try
            {
                for (int i = 0; i < m_Farmers.Count; i++)
                {
                    l_FarmersName += m_Farmers[i].Name + ";";
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in getFarmersName(): " + e.Message);
            }

            return l_FarmersName;
        }

        public string getFarmersRelation()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            string l_FarmersRelation = "";

            try
            {
                for (int i = 0; i < m_Farmers.Count; i++)
                {
                    l_FarmersRelation += m_Farmers[i].RelationStatus.ToString() + ";";
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in getFarmersRelation(): " + e.Message);
            }

            return l_FarmersRelation;
        }

        public string getFarmersLimitReached()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            string l_FarmersLimit = "";

            try
            {
                for (int i = 0; i < m_Farmers.Count; i++)
                {
                    l_FarmersLimit += m_Farmers[i].FarmersLimitReached.ToString() + ";";
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in getFarmersLimitReached(): " + e.Message);
            }

            return l_FarmersLimit;
        }

        public string getFarmersLootTime()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            string l_FarmersLootTime = "";

            try
            {
                for (int i = 0; i < m_Farmers.Count; i++)
                {
                    l_FarmersLootTime += m_Farmers[i].LootTimerHuman + ";";
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in getFarmersLootTime(): " + e.Message);
            }

            return l_FarmersLootTime;
        }

        public string getFarmersMood()
        {
            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;

            string l_FarmersMood = "";

            try
            {
                for (int i = 0; i < m_Farmers.Count; i++)
                {
                    l_FarmersMood += m_Farmers[i].Mood + ";";
                }
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in getFarmersMood(): " + e.Message);
            }

            return l_FarmersMood;
        }

        public int getLootableFarmer(string p_ServerTime)
        {
            int l_FarmersIndex = -1;

            for (int i = 0; i < m_Farmers.Count; i++)
            {
                if (m_Farmers[i].isLootable(p_ServerTime))
                {
                    l_FarmersIndex = i;
                    break;
                }
            }

            return l_FarmersIndex;
        }

        public bool isStorageFull()
        {
            return (m_Storage == m_Wood && m_Wood == m_Stone && m_Stone == m_Iron);
        }

        public string getResourcesString()
        {
            return Wood.ToString() + ";" + Stone.ToString() + ";" + Iron.ToString() + ";";
        }
    }
}
