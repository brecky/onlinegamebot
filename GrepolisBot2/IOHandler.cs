﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;

namespace GrepolisBot2
{
    sealed class IOHandler
    {
        //Singleton
        private static readonly IOHandler m_Instance = new IOHandler();

//-->Constructor

        private IOHandler()
        {

        }

//-->Attributes

        public static IOHandler Instance
        {
            get
            {
                return m_Instance;
            }
        }

//--Methods

        public void debug(string p_Message)
        {
            Settings l_Settings = Settings.Instance;

            TextWriter l_TwDebug = new StreamWriter(l_Settings.AdvDebugFile, true);
            l_TwDebug.WriteLine(DateTime.Now.ToLocalTime() + " " + p_Message);
            l_TwDebug.Close();
        }

        public void saveNotes(string p_Notes)
        {
            Settings l_Settings = Settings.Instance;

            try
            {
                TextWriter l_TwNotes = new StreamWriter(l_Settings.AdvNotesFile, false);
                l_TwNotes.WriteLine(p_Notes);
                l_TwNotes.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    debug("Exception in saveNotes(): " + e.Message);
            }
        }

        public string loadNotes()
        {
            Settings l_Settings = Settings.Instance;

            string l_Notes = "";

            try
            {
                TextReader l_TrNotes = new StreamReader(l_Settings.AdvNotesFile);
                l_Notes = l_TrNotes.ReadToEnd();
                l_TrNotes.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    debug("Exception in saveNotes(): " + e.Message);
            }

            return l_Notes;
        }

        public void saveProgramSettings()
        {
            //Note Modify this when adding new program settings (3/4)
            Settings l_Settings = Settings.Instance;

            try
            {
                XmlTextWriter l_Writer = new XmlTextWriter(l_Settings.AdvProgramSettingsFile, Encoding.UTF8);
                l_Writer.Formatting = Formatting.Indented;
                l_Writer.WriteStartDocument();
                l_Writer.WriteComment("Program settings.");
                l_Writer.WriteStartElement("Users");//Start of Users
                //
                l_Writer.WriteStartElement("User");//Start of User
                l_Writer.WriteStartAttribute("GenUserName");
                l_Writer.WriteString(l_Settings.GenUserName);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("GenPassword");
                l_Writer.WriteString(l_Settings.GenPassword);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("GenMainServer");
                l_Writer.WriteString(l_Settings.GenMainServer);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("GenServer");
                l_Writer.WriteString(l_Settings.GenServer);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("GenAutoStart");
                l_Writer.WriteString(l_Settings.GenAutoStart.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("GenToTryIcon");
                l_Writer.WriteString(l_Settings.GenToTryIcon.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("QueueTimer");
                l_Writer.WriteString(l_Settings.QueueTimer.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("MailAddress");
                l_Writer.WriteString(l_Settings.MailAddress);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("MailNotify");
                l_Writer.WriteString(l_Settings.MailNotify.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("MailUsername");
                l_Writer.WriteString(l_Settings.MailUsername);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("MailPassword");
                l_Writer.WriteString(l_Settings.MailPassword);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("MailServer");
                l_Writer.WriteString(l_Settings.MailServer);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("MailPort");
                l_Writer.WriteString(l_Settings.MailPort.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvTimerRefresh");
                l_Writer.WriteString(l_Settings.AdvTimerRefresh.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvTimerReconnect");
                l_Writer.WriteString(l_Settings.AdvTimerReconnect.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvDebugMode");
                l_Writer.WriteString(l_Settings.AdvDebugMode.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvDebugFile");
                l_Writer.WriteString(l_Settings.AdvDebugFile);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvProgramSettingsFile");
                l_Writer.WriteString(l_Settings.AdvProgramSettingsFile);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvTownsSettingsFile");
                l_Writer.WriteString(l_Settings.AdvTownsSettingsFile);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvUserAgent");
                l_Writer.WriteString(l_Settings.AdvUserAgent);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvNotesFile");
                l_Writer.WriteString(l_Settings.AdvNotesFile);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("AdvDonatorSettingsFile");
                l_Writer.WriteString(l_Settings.AdvDonatorSettingsFile);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("GUIBuildingLevelTargetColor");
                l_Writer.WriteString(l_Settings.GUIBuildingLevelTargetColor.ToArgb().ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("GUIBuildingTooltipsEnabled");
                l_Writer.WriteString(l_Settings.GUIBuildingTooltipsEnabled.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("ProxyEnabled");
                l_Writer.WriteString(l_Settings.ProxyEnabled.ToString());
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("ProxyServer");
                l_Writer.WriteString(l_Settings.ProxyServer);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("ProxyUserName");
                l_Writer.WriteString(l_Settings.ProxyUserName);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteStartAttribute("ProxyPassword");
                l_Writer.WriteString(l_Settings.ProxyPassword);
                l_Writer.WriteEndAttribute();
                l_Writer.WriteEndElement();//End of User
                //
                l_Writer.WriteEndElement();//End of Users
                l_Writer.WriteEndDocument();
                l_Writer.Flush();
                l_Writer.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    debug("Exception in saveProgramSettings(): " + e.Message);
            }
        }

        public void loadProgramSettings()
        {
            //Note Modify this when adding new program settings (4/4)
            Settings l_Settings = Settings.Instance;

            try
            {
                XmlTextReader l_TextReader = new XmlTextReader(l_Settings.AdvProgramSettingsFile);

                l_TextReader.Read();

                // If the node has value
                while (l_TextReader.Read())
                {
                    // Move to fist element
                    l_TextReader.MoveToElement();

                    if (l_TextReader.Name.Equals("User"))
                    {
                        //General
                        if (l_TextReader.GetAttribute("GenUserName") != null)
                            l_Settings.GenUserName = l_TextReader.GetAttribute("GenUserName");
                        if (l_TextReader.GetAttribute("GenPassword") != null)
                            l_Settings.GenPassword = l_TextReader.GetAttribute("GenPassword");
                        if (l_TextReader.GetAttribute("GenMainServer") != null) 
                            l_Settings.GenMainServer = l_TextReader.GetAttribute("GenMainServer");
                        if (l_TextReader.GetAttribute("GenServer") != null) 
                            l_Settings.GenServer = l_TextReader.GetAttribute("GenServer");
                        if (l_TextReader.GetAttribute("GenAutoStart") != null) 
                            l_Settings.GenAutoStart = l_TextReader.GetAttribute("GenAutoStart").Equals("True");
                        if (l_TextReader.GetAttribute("GenToTryIcon") != null) 
                            l_Settings.GenToTryIcon = l_TextReader.GetAttribute("GenToTryIcon").Equals("True");
                        //Building/training queue
                        if (l_TextReader.GetAttribute("QueueTimer") != null) 
                            l_Settings.QueueTimer = int.Parse(l_TextReader.GetAttribute("QueueTimer"));
                        //E-Mail
                        if (l_TextReader.GetAttribute("MailAddress") != null) 
                            l_Settings.MailAddress = l_TextReader.GetAttribute("MailAddress");
                        if (l_TextReader.GetAttribute("MailNotify") != null) 
                            l_Settings.MailNotify = l_TextReader.GetAttribute("MailNotify").Equals("True");
                        if (l_TextReader.GetAttribute("MailUsername") != null) 
                            l_Settings.MailUsername = l_TextReader.GetAttribute("MailUsername");
                        if (l_TextReader.GetAttribute("MailPassword") != null) 
                            l_Settings.MailPassword = l_TextReader.GetAttribute("MailPassword");
                        if (l_TextReader.GetAttribute("MailServer") != null) 
                            l_Settings.MailServer = l_TextReader.GetAttribute("MailServer");
                        if (l_TextReader.GetAttribute("MailPort") != null) 
                            l_Settings.MailPort = int.Parse(l_TextReader.GetAttribute("MailPort"));
                        //Advanced
                        if (l_TextReader.GetAttribute("AdvTimerRefresh") != null) 
                            l_Settings.AdvTimerRefresh = int.Parse(l_TextReader.GetAttribute("AdvTimerRefresh"));
                        if (l_TextReader.GetAttribute("AdvTimerReconnect") != null) 
                            l_Settings.AdvTimerReconnect = int.Parse(l_TextReader.GetAttribute("AdvTimerReconnect"));
                        if (l_TextReader.GetAttribute("AdvDebugMode") != null) 
                            l_Settings.AdvDebugMode = l_TextReader.GetAttribute("AdvDebugMode").Equals("True");
                        if (l_TextReader.GetAttribute("AdvDebugFile") != null) 
                            l_Settings.AdvDebugFile = l_TextReader.GetAttribute("AdvDebugFile");
                        if (l_TextReader.GetAttribute("AdvProgramSettingsFile") != null) 
                            l_Settings.AdvProgramSettingsFile = l_TextReader.GetAttribute("AdvProgramSettingsFile");
                        if (l_TextReader.GetAttribute("AdvTownsSettingsFile") != null) 
                            l_Settings.AdvTownsSettingsFile = l_TextReader.GetAttribute("AdvTownsSettingsFile");
                        if (l_TextReader.GetAttribute("AdvUserAgent") != null) 
                            l_Settings.AdvUserAgent = l_TextReader.GetAttribute("AdvUserAgent");
                        if (l_TextReader.GetAttribute("AdvNotesFile") != null) 
                            l_Settings.AdvNotesFile = l_TextReader.GetAttribute("AdvNotesFile");
                        if (l_TextReader.GetAttribute("AdvDonatorSettingsFile") != null)
                            l_Settings.AdvDonatorSettingsFile = l_TextReader.GetAttribute("AdvDonatorSettingsFile");
                        //GUI
                        if (l_TextReader.GetAttribute("GUIBuildingLevelTargetColor") != null)
                            l_Settings.GUIBuildingLevelTargetColor = Color.FromArgb(int.Parse(l_TextReader.GetAttribute("GUIBuildingLevelTargetColor")));
                        if (l_TextReader.GetAttribute("GUIBuildingTooltipsEnabled") != null)
                            l_Settings.GUIBuildingTooltipsEnabled = l_TextReader.GetAttribute("GUIBuildingTooltipsEnabled").Equals("True");
                        //Proxy
                        if (l_TextReader.GetAttribute("ProxyEnabled") != null)
                            l_Settings.ProxyEnabled = l_TextReader.GetAttribute("ProxyEnabled").Equals("True");
                        if (l_TextReader.GetAttribute("ProxyServer") != null)
                            l_Settings.ProxyServer = l_TextReader.GetAttribute("ProxyServer");
                        if (l_TextReader.GetAttribute("ProxyUserName") != null)
                            l_Settings.ProxyUserName = l_TextReader.GetAttribute("ProxyUserName");
                        if (l_TextReader.GetAttribute("ProxyPassword") != null)
                            l_Settings.ProxyPassword = l_TextReader.GetAttribute("ProxyPassword");
                    }
                }
                l_TextReader.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    debug("Exception in loadProgramSettings(): " + e.Message);
            }
        }

        public void saveTownsSettings(Player p_Player)
        {
            //Note Modify this when adding new town settings (1/2)
            Settings l_Settings = Settings.Instance;

            try
            {
                XmlTextWriter l_Writer = new XmlTextWriter(l_Settings.AdvTownsSettingsFile, Encoding.UTF8);
                l_Writer.Formatting = Formatting.Indented;
                l_Writer.WriteStartDocument();
                l_Writer.WriteComment("Towns settings.");
                l_Writer.WriteStartElement("Towns");//Start of Towns
                for (int i = 0; i < p_Player.Towns.Count; i++)
                {
                    l_Writer.WriteStartElement("Town");//Start of Town
                    l_Writer.WriteStartAttribute("TownID");
                    l_Writer.WriteString(p_Player.Towns[i].TownID);
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("BuildingQueueEnabled");
                    l_Writer.WriteString(p_Player.Towns[i].BuildingQueueEnabled.ToString());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("BuildingLevelsTargetEnabled");
                    l_Writer.WriteString(p_Player.Towns[i].BuildingLevelsTargetEnabled.ToString());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("BuildingLevelsTarget");
                    l_Writer.WriteString(p_Player.Towns[i].getBuildingLevelsTarget());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("BuildingQueue");
                    l_Writer.WriteString(p_Player.Towns[i].getBuildingQueue());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("UnitQueueEnabled");
                    l_Writer.WriteString(p_Player.Towns[i].UnitQueueEnabled.ToString());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("UnitQueue");
                    l_Writer.WriteString(p_Player.Towns[i].getUnitQueue());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("FarmersLootEnabled");
                    l_Writer.WriteString(p_Player.Towns[i].FarmersLootEnabled.ToString());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("FarmersFriendlyDemandsOnly");
                    l_Writer.WriteString(p_Player.Towns[i].FarmersFriendlyDemandsOnly.ToString());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("FarmersMinMood");
                    l_Writer.WriteString(p_Player.Towns[i].FarmersMinMood.ToString());
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteStartAttribute("FarmersLootInterval");
                    l_Writer.WriteString(p_Player.Towns[i].FarmersLootInterval);
                    l_Writer.WriteEndAttribute();
                    l_Writer.WriteEndElement();//End of Town
                }
                l_Writer.WriteEndElement();//End of Towns
                l_Writer.WriteEndDocument();
                l_Writer.Flush();
                l_Writer.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    debug("Exception in saveTownsSettings(): " + e.Message);
            }
        }


        /*
        public void loadTownsSettings()
        {
            //Moved to the player class
        }
        */

        public void loadDonatorSettings()
        {
            Settings l_Settings = Settings.Instance;

            try
            {
                XmlTextReader l_TextReader = new XmlTextReader(l_Settings.AdvDonatorSettingsFile);

                l_TextReader.Read();

                // If the node has value
                while (l_TextReader.Read())
                {
                    // Move to fist element
                    l_TextReader.MoveToElement();

                    if (l_TextReader.Name.Equals("Verification"))
                    {
                        if (l_TextReader.GetAttribute("DonUserName") != null)
                            l_Settings.DonUserName = l_TextReader.GetAttribute("DonUserName");
                        if (l_TextReader.GetAttribute("DonPassword") != null)
                            l_Settings.DonPassword = l_TextReader.GetAttribute("DonPassword");
                    }
                }
                l_TextReader.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    debug("Exception in loadDonatorSettings(): " + e.Message);
            }
        }

        public void saveServerResponse(string p_Method, string p_Response)
        {
            Settings l_Settings = Settings.Instance;

            try
            {
                TextWriter l_TwResponse = new StreamWriter(l_Settings.AdvResponseDir + p_Method + ".txt", false);
                l_TwResponse.Write(p_Response);
                l_TwResponse.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    debug("Exception in saveServerResponse(): " + e.Message);
            }
        }
    }
}
