﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace GrepolisBot2
{
    class Player
    {
        //General
        string m_PlayerID = "";
        string m_DefaultTownID = "";

        //Gods
        int m_FavorZeus = 0;
        int m_FavorPoseidon = 0;
        int m_FavorHera = 0;
        int m_FavorAthene = 0;
        int m_FavorHades = 0;
        string m_FavorZeusProduction = "0";
        string m_FavorPoseidonProduction = "0";
        string m_FavorHeraProduction = "0";
        string m_FavorAtheneProduction = "0";
        string m_FavorHadesProduction = "0";

        //Cultural
        string m_CulturalLevelStr = "";
        string m_CulturalCitiesStr = "";
        string m_CulturalPointsStr = "";
        int m_CulturalPointsCurrent = 0;
        int m_CulturalPointsMax = 0;

        //Farmer
        private DateTime m_FarmersResetTime = DateTime.Now;

        //Custom classes
        private List<Town> m_Towns = new List<Town>();
        private List<Notification> m_Notifications = new List<Notification>();

//-->Constructor
        
        public Player()
        {

        }

//-->Attributes

        //General
        public string PlayerID
        {
            get { return m_PlayerID; }
            set { m_PlayerID = value; }
        }

        public string DefaultTownID
        {
            get { return m_DefaultTownID; }
            set { m_DefaultTownID = value; }
        }

        //Gods
        public int FavorZeus
        {
            get { return m_FavorZeus; }
            set { m_FavorZeus = value; }
        }

        public int FavorPoseidon
        {
            get { return m_FavorPoseidon; }
            set { m_FavorPoseidon = value; }
        }

        public int FavorHera
        {
            get { return m_FavorHera; }
            set { m_FavorHera = value; }
        }

        public int FavorAthene
        {
            get { return m_FavorAthene; }
            set { m_FavorAthene = value; }
        }

        public int FavorHades
        {
            get { return m_FavorHades; }
            set { m_FavorHades = value; }
        }

        public string FavorZeusProduction
        {
            get { return m_FavorZeusProduction; }
            set { m_FavorZeusProduction = value; }
        }

        public string FavorPoseidonProduction
        {
            get { return m_FavorPoseidonProduction; }
            set { m_FavorPoseidonProduction = value; }
        }

        public string FavorHeraProduction
        {
            get { return m_FavorHeraProduction; }
            set { m_FavorHeraProduction = value; }
        }

        public string FavorAtheneProduction
        {
            get { return m_FavorAtheneProduction; }
            set { m_FavorAtheneProduction = value; }
        }

        public string FavorHadesProduction
        {
            get { return m_FavorHadesProduction; }
            set { m_FavorHadesProduction = value; }
        }

        //Cultural
        public string CulturalLevelStr
        {
            get { return m_CulturalLevelStr; }
            set { m_CulturalLevelStr = value; }
        }

        public string CulturalCitiesStr
        {
            get { return m_CulturalCitiesStr; }
            set { m_CulturalCitiesStr = value; }
        }

        public string CulturalPointsStr
        {
            get { return m_CulturalPointsStr; }
            set { m_CulturalPointsStr = value; }
        }

        public int CulturalPointsCurrent
        {
            get { return m_CulturalPointsCurrent; }
            set { m_CulturalPointsCurrent = value; }
        }

        public int CulturalPointsMax
        {
            get { return m_CulturalPointsMax; }
            set { m_CulturalPointsMax = value; }
        }

        //Farmer
        public DateTime FarmersResetTime
        {
            get { return m_FarmersResetTime; }
            set { m_FarmersResetTime = value; }
        }

        //Custom classes
        public List<Town> Towns
        {
            get { return m_Towns; }
            set { m_Towns = value; }
        }

//-->Methods

        public void resetDailyFarmLimit()
        {
            for (int i = 0; i < Towns.Count; i++)
            {
                Towns[i].resetDailyFarmLimit();
            }
        }

        public string getFavorByTownIndex(int p_Index)
        {
            if (Towns[p_Index].God.Equals("zeus"))
                return FavorZeus.ToString() + " (+" + FavorZeusProduction + ")";
            else if (Towns[p_Index].God.Equals("poseidon"))
                return FavorPoseidon.ToString() + " (+" + FavorPoseidonProduction + ")";
            else if (Towns[p_Index].God.Equals("hera"))
                return FavorHera.ToString() + " (+" + FavorHeraProduction + ")";
            else if (Towns[p_Index].God.Equals("athena"))
                return FavorAthene.ToString() + " (+" + FavorAtheneProduction + ")";
            else if (Towns[p_Index].God.Equals("hades"))
                return FavorHades.ToString() + " (+" + FavorHadesProduction + ")";
            return "0 (+0)";
        }

        /*
         * Checks if town with p_ID is in current Town list
         */
        public bool isUniqueTown(String p_ID)
        {
            bool l_IsUnique = true;
            for (int i = 0; i < m_Towns.Count; i++)
            {
                if (m_Towns[i].TownID == p_ID)
                    l_IsUnique = false;
            }
            return l_IsUnique;
        }

        /*
         * Get town index by ID
         */
        public int getTownIndexByID(string p_ID)
        {
            int l_TownIndex = -1;
            for (int i = 0; i < m_Towns.Count; i++)
            {
                if (m_Towns[i].TownID == p_ID)
                    l_TownIndex = i;
            }
            return l_TownIndex;
        }

        public void sortTownsByName()
        {
            int l_CurrentPos;
            int l_MinimumPos;

            for (l_CurrentPos = 0; l_CurrentPos < m_Towns.Count; l_CurrentPos++)
            {
                /* Find the town name with the lowest alphabetic order in the current sub array*/
                l_MinimumPos = l_CurrentPos;
                for (int i = l_CurrentPos + 1; i < m_Towns.Count; i++)
                {
                    if (string.Compare(m_Towns[i].Name, m_Towns[l_MinimumPos].Name) == -1)
                    {
                        l_MinimumPos = i;
                    }
                }
                /* Swap the current town and the town with the lowest alphabetic order */
                if (l_MinimumPos != l_CurrentPos)
                {
                    Town l_Town = m_Towns[l_CurrentPos];
                    m_Towns[l_CurrentPos] = m_Towns[l_MinimumPos];
                    m_Towns[l_MinimumPos] = l_Town;
                }
            }
        }

        public void addNotification(string p_Notify_id, string p_Time, string p_Type, string p_Subject)
        {
            if(isUniqueNotification(p_Notify_id))
            {
                m_Notifications.Add(new Notification(p_Notify_id, p_Time, p_Type, p_Subject));
            }
        }

        public bool isUniqueNotification(string p_Notify_id)
        {
            bool l_IsUnique = true;
            for (int i = 0; i < m_Notifications.Count; i++)
            {
                if (m_Notifications[i].Notify_id.Equals(p_Notify_id))
                    l_IsUnique = false;
            }
            return l_IsUnique;
        }

        public string getNotifications(string p_Offset)
        {
            string l_Notifications = "";
            string l_Notification = "";

            for (int i = 0; i < m_Notifications.Count; i++)
            {
                if (m_Notifications[i].Subject.Equals(""))
                    l_Notification = m_Notifications[i].getHumanTime(p_Offset) + " " + m_Notifications[i].Type;
                else
                    l_Notification = m_Notifications[i].getHumanTime(p_Offset) + " " + m_Notifications[i].Subject;

                l_Notifications = l_Notification + System.Environment.NewLine + l_Notifications;
            }

            return l_Notifications;
        }

        public void loadTownsSettings()
        {
            //Note Modify this when adding new town settings (2/2)

            IOHandler l_IOHandler = IOHandler.Instance;
            Settings l_Settings = Settings.Instance;
            
            try
            {
                XmlTextReader l_TextReader = new XmlTextReader(l_Settings.AdvTownsSettingsFile);
                int l_TownIndex = -1;

                l_TextReader.Read();

                // If the node has value
                while (l_TextReader.Read())
                {
                    // Move to fist element
                    l_TextReader.MoveToElement();

                    if (l_TextReader.Name.Equals("Town"))
                    {
                        l_TownIndex = getTownIndexByID(l_TextReader.GetAttribute("TownID"));
                        if (l_TownIndex != -1)
                        {
                            if (l_TextReader.GetAttribute("BuildingQueueEnabled") != null)
                                m_Towns[l_TownIndex].BuildingQueueEnabled = l_TextReader.GetAttribute("BuildingQueueEnabled").Equals("True");
                            if (l_TextReader.GetAttribute("BuildingLevelsTargetEnabled") != null)
                                m_Towns[l_TownIndex].BuildingLevelsTargetEnabled = l_TextReader.GetAttribute("BuildingLevelsTargetEnabled").Equals("True");
                            if (l_TextReader.GetAttribute("BuildingLevelsTarget") != null)
                                m_Towns[l_TownIndex].setBuildingsLevelTarget(l_TextReader.GetAttribute("BuildingLevelsTarget"));
                            if (l_TextReader.GetAttribute("BuildingQueue") != null)
                                m_Towns[l_TownIndex].setBuildingQueue(l_TextReader.GetAttribute("BuildingQueue"));
                            if (l_TextReader.GetAttribute("UnitQueueEnabled") != null)
                                m_Towns[l_TownIndex].UnitQueueEnabled = l_TextReader.GetAttribute("UnitQueueEnabled").Equals("True");
                            if (l_TextReader.GetAttribute("UnitQueue") != null)
                                m_Towns[l_TownIndex].setUnitQueue(l_TextReader.GetAttribute("UnitQueue"));
                            if (l_TextReader.GetAttribute("FarmersLootEnabled") != null)
                                m_Towns[l_TownIndex].FarmersLootEnabled = l_TextReader.GetAttribute("FarmersLootEnabled").Equals("True");
                            if (l_TextReader.GetAttribute("FarmersFriendlyDemandsOnly") != null)
                                m_Towns[l_TownIndex].FarmersFriendlyDemandsOnly = l_TextReader.GetAttribute("FarmersFriendlyDemandsOnly").Equals("True");
                            if (l_TextReader.GetAttribute("FarmersMinMood") != null)
                                m_Towns[l_TownIndex].FarmersMinMood = int.Parse(l_TextReader.GetAttribute("FarmersMinMood"));
                            if (l_TextReader.GetAttribute("FarmersLootInterval") != null)
                                m_Towns[l_TownIndex].FarmersLootInterval = l_TextReader.GetAttribute("FarmersLootInterval");
                        }
                    }
                }
                l_TextReader.Close();
            }
            catch (Exception e)
            {
                if (l_Settings.AdvDebugMode)
                    l_IOHandler.debug("Exception in loadTownsSettings(): " + e.Message);
            }
        }
    }
}
