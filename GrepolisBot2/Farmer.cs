﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrepolisBot2
{
    class Farmer
    {
        private string m_ID = "";
        private string m_Name = "";
        private int m_Mood = 0;
        private string m_IslandX = "000";
        private string m_IslandY = "000";
        private bool m_RelationStatus = false;
        private string m_LootTimer = "";//When you are able to loot again displayed in seconds.
        private string m_LootTimerHuman = "";//Shows in understandable words when you're able to loot again.
        private bool m_FarmersLimitReached = false;

//-->Constructors

        public Farmer()
        {

        }

        public Farmer(string p_ID, string p_Name, string p_IslandX, string p_IslandY, string p_Mood, string p_RelationStatus, string p_LootTimer, string p_LootTimerHuman)
        {
            m_ID = p_ID;
            m_Name = p_Name;
            m_IslandX = p_IslandX;
            m_IslandY = p_IslandY;
            m_Mood = int.Parse(p_Mood);
            m_RelationStatus = p_RelationStatus.Equals("1");
            m_LootTimer = p_LootTimer;
            m_LootTimerHuman = p_LootTimerHuman;
        }

//-->Attributes

        public String ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public String Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public int Mood
        {
            get { return m_Mood; }
            set { m_Mood = value; }
        }

        public string IslandX
        {
            get { return m_IslandX; }
            set { m_IslandX = value; }
        }

        public string IslandY
        {
            get { return m_IslandY; }
            set { m_IslandY = value; }
        }

        public bool RelationStatus
        {
            get { return m_RelationStatus; }
            set { m_RelationStatus = value; }
        }

        public string LootTimer
        {
            get { return m_LootTimer; }
            set { m_LootTimer = value; }
        }

        public string LootTimerHuman
        {
            get { return m_LootTimerHuman; }
            set { m_LootTimerHuman = value; }
        }

        public bool FarmersLimitReached
        {
            get { return m_FarmersLimitReached; }
            set { m_FarmersLimitReached = value; }
        }

//-->Methods

        public bool isLootable(string p_ServerTime)
        {
            bool l_Lootable = false;
            long l_TimeRemaining = 1;

            l_TimeRemaining = long.Parse(m_LootTimer) - long.Parse(p_ServerTime);

            //Added an extra 20 seconds to handle server lag
            if (m_RelationStatus && l_TimeRemaining <= -20 && !FarmersLimitReached)
                l_Lootable = true;

            return l_Lootable;
        }
    }
}
