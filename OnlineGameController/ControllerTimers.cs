﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Reflection;
using System.Windows.Forms;


namespace OnlineGameController
{
    public class ControllerTimers
    {

       
        private jdTimer m_RefreshTimer = new jdTimer();
       

        public delegate void updateRefreshTimerHandler(object sender, CustomArgs ca);
        public event updateRefreshTimerHandler refreshTimerUpdated;
        

        public ControllerTimers()
        {
            initEventHandlers();
            initTimers();
   
        }

        /// <summary>
        /// Inditsa a resfrest random szam alapjan
        /// </summary>
        public void startbot()
        {
            var setting = OnlineGameSettings.Settings.Instance;
            m_RefreshTimer.start(setting.RefreshTimeRandom);
        }

        private void initEventHandlers()
        {
          
            m_RefreshTimer.InternalTimer.Elapsed += new System.Timers.ElapsedEventHandler(RefreshTimer_InternalTimer_Elapsed);
           
        }

        public void initTimers()
        {
            m_RefreshTimer.Duration = 60;
        }


        private void RefreshTimer_InternalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (m_IsBotRunning)
            //{
                updateRefreshTimerEvent();
            //}
            //else
           // {
                //stop();
            //}
        }

        private void updateRefreshTimerEvent()
        {
            m_RefreshTimer.InternalTimer.Stop();

            CustomArgs l_CustomArgs = new CustomArgs(m_RefreshTimer.getTimeLeft());
            refreshTimerUpdated(this, l_CustomArgs);

            if (m_RefreshTimer.isTimerDone())
            {
                m_RefreshTimer.stop();
               // m_QueueTimer.stop();
               // if (m_QueueTimer.isTimerDone())
               //     m_TimeoutOnQueueTimer = true;
               // else
               //     m_TimeoutOnQueueTimer = false;
               // m_RequestingServerData = true;
                //locateFarmers();

            }
            else
                m_RefreshTimer.InternalTimer.Start();
        }

      
    }
}
