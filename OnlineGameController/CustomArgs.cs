﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineGameController
{
    public class CustomArgs : System.EventArgs
    {
        private string m_Message;

        public CustomArgs(string p_Message)
        {
            m_Message = p_Message;
        }

        public string getMessage()
        {
            return m_Message;
        }
    }
}
