﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using OnlineGameConfiguration;

namespace OnlineGameSettings
{
    /// <summary>
    /// It tarolodnak a jatekban levo valto adatok;
    /// </summary>
    public sealed class Settings
    {
        ///iten csinalom sajat magambo egy kopiat
        private static readonly Settings MInstance = new Settings();

        ///itt erhetem el masik prokrambol est a kopijat.
        public static Settings Instance
        {
            get { return MInstance; }
        }

        ///Contruktor
        private Settings()
        {
            ReadGeneralConfig();
            IsLogin = false;
            IsBotStart = false;
        }


        public bool IsLogin { get; set;}

        public bool IsBotStart { get; set; }

        public string Password { get; set; }

        public string UserName { get; set; }

        public string GameUrl { get; set; }

        public string AdvDebugFile { get; set; }

        public  bool AdvDebug { get; set; }

        public int RefreshTimeMin { get; set; }

        public int RefreshTimeMax { get; set; }


        public int RefreshTimeRandom
        {
            get
            {
                var r = new Random();
                return r.Next(RefreshTimeMin, RefreshTimeMax);
            }
        }




        /// <summary>
        /// Mennji fa all rendelkezesra
        /// </summary>

        public int FaMennjiseg { get; set; }


        /// <summary>
        /// Megdulpazom a rendelkelkezesre allo fa mennjiseget
        /// </summary>
        /// <returns>int </returns>
        public int Fadublazas()
        {
            FaMennjiseg = FaMennjiseg * 2;
            return FaMennjiseg;
        }




        private void ReadGeneralConfig()
        {

            UserName = Configuration.GetValue("UserName", "MyUserName");
            Password = Configuration.GetValue("Password", "MyPassword");
            GameUrl = Configuration.GetValue("GameURL", "grepolis.com");
            AdvDebugFile = Configuration.GetValue("AdvDebugFile", "debug.txt");
            AdvDebug = Configuration.GetValue("AdvDebug", "0").Equals("1") ? true : false;
            RefreshTimeMin = Configuration.GetValue("RefreshTimeMin", 2);
            RefreshTimeMax = Configuration.GetValue("RefreshTimeMax", 10);
        }
        
        public void SaveGeneralConfig()
        {
            Configuration.SetValue("UserName", UserName.ToString(CultureInfo.InvariantCulture));
            Configuration.SetValue("Password", Password.ToString(CultureInfo.InvariantCulture));
            Configuration.SetValue("GameURL", GameUrl.ToString(CultureInfo.InvariantCulture));
            Configuration.SetValue("AdvDebugFile", AdvDebugFile.ToString(CultureInfo.InvariantCulture));
           // Configuration.SetValue("AdvDebug", AdvDebug.ToString(CultureInfo.InvariantCulture));
            Configuration.SetValue("RefreshTimeMin", RefreshTimeMin);
            Configuration.SetValue("RefreshTimeMax", RefreshTimeMax);
        }
        
    }
}
