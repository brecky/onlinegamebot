﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;
using OnlineGameConfiguration;

namespace OnlineGameStart
{
    sealed class IOHandler
    {
        //Singleton
        private static readonly IOHandler m_Instance = new IOHandler();

//-->Constructor

        private IOHandler()
        {

        }

//-->Attributes

        public static IOHandler Instance
        {
            get
            {
                return m_Instance;
            }
        }

//--Methods

        public void debug(string p_Message)
        {
            TextWriter l_TwDebug = new StreamWriter(Configuration.GetValue("AdvDebugFile", "debug.txt"), true);
            l_TwDebug.WriteLine(DateTime.Now.ToLocalTime() + " " + p_Message);
            l_TwDebug.Close();
        }
    }
}
