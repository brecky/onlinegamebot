﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineGameConfiguration;
using System.Net;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Globalization;
//using OnlineGameController;
using System.Resources;
using Awesomium.Core;
using OnlineGameController;
using OnlineGameSettings;

namespace OnlineGameStart
{
    public partial class StartForm : Form
    {
        //Useragent
        //[DllImport("urlmon.dll", CharSet = CharSet.Ansi)]
        //private static extern int UrlMkSetSessionOption(int dwOption, string pBuffer, int dwBufferLength, int dwReserved);
        //const int URLMON_OPTION_USERAGENT = 0x10000001;
        private Settings setting = Settings.Instance;
        private ControllerTimers m_ControllerTimers = new OnlineGameController.ControllerTimers();
        ResourceManager LocRM = new ResourceManager("OnlineGameStart.StartForm", typeof(StartForm).Assembly);

        //Thread Handling
        public delegate void UpdateRefreshTimerCallBack(object sender);

        public StartForm()
        {
            //Inicialja a ezen a forman letezo osszes kontrolokat (label, txt, button, tabcontrol stb
            //ezek az adatok text box - nevek, labelek teksztje ey pedig a startform.designer.cs - ben van leirva
            //valamint a gombok (butonok) eventek nevei (click, text change) vannak megadva amik valojaban funkciok
            InitializeComponent();
            //a nem automatikus tortenetek amit mi szerkesztettunk pl. tabcontrol1.selectedindexchange
            //ezeket a tortenteket deklaraljuk (hivjuk meg)
            InitEventHandlers();
           
            

            RuntimeLocalizer.ChangeCulture(this, CultureInfo.CurrentCulture.IetfLanguageTag);

            var config = new WebConfig { LogLevel = LogLevel.Verbose };
            WebCore.Initialize(config);
            string sessionpath = System.AppDomain.CurrentDomain.BaseDirectory; // executable folder
            WebSession session = WebCore.CreateWebSession(System.AppDomain.CurrentDomain.BaseDirectory, WebPreferences.Default);

            ReadGeneralConfig();

            if (setting.IsLogin == false)
            {
                btnStartBot.Enabled = false; 
            }
            
            aWebControl.Source =new Uri("http://" + Configuration.GetValue("GameURL", "grepolis.com"));
        }
        private void InitEventHandlers()
        {
            m_ControllerTimers.refreshTimerUpdated += new ControllerTimers.updateRefreshTimerHandler(m_Controller_refreshTimerUpdated);

            aWebControl.DocumentReady += aWebControl_DocumentReady;
            aWebControl.LoadingFrameComplete += aWebControl_LoadingFrameComplete;
            tabControl1.SelectedIndexChanged += tabControl1_SelectedIndexChanged;
        }

        /*
         * Ez a funkcio hatarozza a tortenteket a tabulator kontrolakon
         */

        void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {            
            switch (tabControl1.SelectedIndex)
            {
                case 0: //tabulator index 0
                    break;
                case 1: //tabulator index 1
                   
                    break;
                /*tabulator index 2*/
                case 2: 
                //    aWebControl.Source = new Uri("http://" + Configuration.GetValue("GemURL", "grepolis.com"));
                    break;
                default:
                    break;


            }
        }

        private void m_Controller_refreshTimerUpdated(object sender, CustomArgs ca)
        {
            //Create new thread
            new Thread(new ParameterizedThreadStart(updateRefreshTimerCrossThread)).Start(ca.getMessage());
        }
                
        private void updateRefreshTimerCrossThread(object p_TimeLeft)
        {
            try
            {
               if (labelNextUpdateTimeLeft.InvokeRequired || statusStrip1.InvokeRequired)
               {
                   UpdateRefreshTimerCallBack l_Delegate = new UpdateRefreshTimerCallBack(updateRefreshTimer);
                   this.Invoke(l_Delegate, new object[] { (string)p_TimeLeft });
               }
               else
               {
                    updateRefreshTimer(p_TimeLeft);
               }
            }
            catch (Exception)
            {
                //No timer update this time :(
            }
        }

        private void updateRefreshTimer(object p_TimeLeft)
        {
            if (p_TimeLeft.ToString().Contains("-"))
            {
                labelNextUpdateTimeLeft.Text = "00:00:00";
                toolStripStatusLabel2.Text = LocRM.GetString("toolStripStatusLabel2.Text") + "00:00:00";
            }
            else
            {
                labelNextUpdateTimeLeft.Text = p_TimeLeft.ToString();
                toolStripStatusLabel2.Text = LocRM.GetString("toolStripStatusLabel2.Text") + p_TimeLeft;
            }
        }


    
#region awesionWebBrowser

        private void aWebControl_DocumentReady(object sender, UrlEventArgs e)
        {
            var pView = aWebControl.NativeView;
            if (pView == IntPtr.Zero)
            {
                throw new InvalidOperationException("WebView is not available");
            }
                      
           IWebView pview = (IWebView)aWebControl;

        }


       
        private void aWebControl_LoadingFrameComplete(object sender, FrameEventArgs e)
        {
            
            IWebView view = (IWebView)aWebControl;
           

            
        }

#endregion


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            // Dispose the WebControl.
            aWebControl.Dispose();

            // This was the only Window in our app.
            // Shutdown the WebCore while exiting.
            WebCore.Shutdown();
        }

        
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            RuntimeLocalizer.ChangeCulture(this, "en-US");
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            RuntimeLocalizer.ChangeCulture(this, "hu-HU");
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// General settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }
       
        

        private string GetJsSingleXpathString(string xpath)
        {
            return
                String.Format(
                    "document.evaluate(\"{0}\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue", xpath);
        }

        private string GetJsXpathString(string xpath)
        {
            return
               String.Format(
                   "document.evaluate(\"{0}\", document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null)", xpath);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ReadGeneralConfig();
        }
        
        private void ReadGeneralConfig()
        {
            textBoxUserName.Text = setting.UserName;
            textBoxPassword.Text = setting.Password;
            cmbUrl.SelectedItem = setting.GameUrl;
            txtDebug.Text = setting.AdvDebugFile;
            chkDebug.Checked = setting.AdvDebug;
            txtMin.Text = setting.RefreshTimeMin.ToString();
            txtMax.Text = setting.RefreshTimeMax.ToString();
           

        }
        /// <summary>
        /// Az usernevet, jelszot es a jatek url cimet mentem ell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {

            setting.UserName = textBoxUserName.Text;
            setting.Password = textBoxPassword.Text;
            setting.GameUrl = cmbUrl.SelectedItem.ToString();
            
            setting.SaveGeneralConfig();

            tabControl1.SelectedIndex = 0;
            aWebControl.Source = new Uri("http://" + setting.GameUrl);
        }

        private void btnStartBot_Click(object sender, EventArgs e)
        {
            //elinditom a bot funkciokat
            //a web oldal refresh ideje visszaszamlalasa 
            m_ControllerTimers.startbot();
            setting.IsBotStart = true;
            btnStartBot.Enabled = false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //aWebControlk atalakitom IwebView
                IWebView view = (IWebView)aWebControl;
                // kell a texboxot kitolteni
                view.FillTextboxById("name", setting.UserName);
                view.FillTextboxById("password", setting.Password);
                view.JsFireEvent("document.getElementsByClassName('button')[0]", "click");
                //varakozik
                System.Threading.Thread.Sleep(1000);
                //kivalsztja az elso terkepet
                string test = GetJsSingleXpathString("//*[@id='worlds']/div/ul/li[1]/a");
                view.ImprovedJsClick(test);
                btnLogin.Enabled = false;
                setting.IsLogin = true;
                btnStartBot.Enabled = true;
            }
            catch (Exception)
            {
                
                throw;
            }
            

        }
        /// <summary>
        /// Debug bealitasok felvetele
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSettingsSave_Click(object sender, EventArgs e)
        {
           
            setting.AdvDebug = chkDebug.Checked;
            setting.AdvDebugFile = txtDebug.Text.ToString();
            setting.RefreshTimeMin = Convert.ToInt32(txtMin.Text);
            setting.RefreshTimeMax = Convert.ToInt32(txtMax.Text);
            setting.SaveGeneralConfig();
           
         }

        private void chkDebug_CheckedChanged(object sender, EventArgs e)
        {

            if (chkDebug.Checked == true)
            {
               //Configuration.SetValue("AdvDebug", "1");
            }
            else
            {
                //Configuration.SetValue("AdvDebug", "0");
            }
        }

        private void btnSettingsCancel_Click(object sender, EventArgs e)
        {
           

        }

       
                         
    }


    public static class RuntimeLocalizer
    {
        public static void ChangeCulture(Form frm, string cultureCode)
        {
            CultureInfo culture = CultureInfo.GetCultureInfo(cultureCode);

            Thread.CurrentThread.CurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
            ComponentResourceManager resources = new ComponentResourceManager(frm.GetType());

            ApplyResourceToControl(resources, frm, culture);
            resources.ApplyResources(frm, "$this", culture);
        }

        private static void ApplyResourceToControl(ComponentResourceManager res, Control control, CultureInfo lang)
        {
            if (control.GetType() == typeof(MenuStrip))  // See if this is a menuStrip
            {
                MenuStrip strip = (MenuStrip)control;

                ApplyResourceToToolStripItemCollection(strip.Items, res, lang);
            }

            foreach (Control c in control.Controls) // Apply to all sub-controls
            {
                ApplyResourceToControl(res, c, lang);
                res.ApplyResources(c, c.Name, lang);
            }

            // Apply to self
            res.ApplyResources(control, control.Name, lang);
        }

        private static void ApplyResourceToToolStripItemCollection(ToolStripItemCollection col, ComponentResourceManager res, CultureInfo lang)
        {
            for (int i = 0; i < col.Count; i++)     // Apply to all sub items
            {
                ToolStripItem item = (ToolStripMenuItem)col[i];

                if (item.GetType() == typeof(ToolStripMenuItem))
                {
                    ToolStripMenuItem menuitem = (ToolStripMenuItem)item;
                    ApplyResourceToToolStripItemCollection(menuitem.DropDownItems, res, lang);
                }

                res.ApplyResources(item, item.Name, lang);
            }
        }
    }
}
