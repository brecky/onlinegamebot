using System;

namespace OnlineGameStart
{
    public static class WebViewHelper
    {
        /// <summary>
        ///  Returns JavaScript XPath query string for getting a single element 
        /// </summary>
        /// <param name="xpath">Any valid XPath string, for example "//input[@id='myid']"</param>
        /// <returns>JS code to perform the XPath query (document.evaluate(...)</returns>
        public static string GetJsSingleXpathString(string xpath)
        {
            return
                String.Format(
                    "document.evaluate(\"{0}\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue", xpath);
        }

        /// <summary>
        ///  Returns JavaScript XPath query string for getting a collection of elements
        /// </summary>
        /// <param name="xpath">Any valid XPath string, for example "//input[@id='myid']"</param>
        /// <returns>JS code to perform the XPath query (document.evaluate(...)</returns>
        public static string GetJsXpathString(string xpath)
        {
            return
                String.Format(
                    "document.evaluate(\"{0}\", document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null)", xpath);
        }

        /// <summary>
        /// Returns coordinates of top-left corner of the element (probably relative to the page)
        /// </summary>
        public static Point GetElementPosition(dynamic element)
        {
            dynamic rect = element.getBoundingClientRect();
            using (rect)
            {
                return new Point(rect.left, rect.top);
            }
        }
    }
}
